//
//  MenuVC.swift
//  Lobe
//
//  Created by Professional on 2016-01-20.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit
import Bond
import LGSideMenuController

//protocol MenuDelegate {
//    func menuSearch()
//    func menuGoToCurrentSong()
//    func menuLeaveGroup()
//    func menuRecalculateSync()
//    func menuLogout()
//    func menuViewProfile()
//}
class MenuVC: UITableViewController {

    @IBOutlet var userNameLabel: UILabel!
    @IBOutlet var userCityLabel: UILabel!
    @IBOutlet var userImageButton: UIButton!
    var delegate:MenuDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        LobeStateHelper.currentUser.userName.observe{ username in
//            self.userNameLabel.text = username
//        }
//        
//        LobeStateHelper.currentUser.userPic.observe{ image in
//            self.userImageButton.setImage(image, forState: UIControlState.Normal)
//        }
        
        LobeStateHelper.sharedInstance.addUserStateObserver(self)
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "menu_background")!)
        self.tableView.backgroundColor = UIColor(patternImage: UIImage(named: "menu_background")!)
        
        //Listen to these values
//        LobeStateHelper.currentUser.userPic.observe{ image in
//            self.userImageButton.setImage(image, forState: UIControlState.Normal)
//        }
//        
//        LobeStateHelper.currentUser.userName.observe{ username in
//            self.userNameLabel.text = username
//        }
    }
    
    func userStateChanged(){
        if let image = LobeStateHelper.currentUser.userImage {
            self.userImageButton.setImage(image, forState: UIControlState.Normal)
        }
        if let username = LobeStateHelper.currentUser.userName.value {
            self.userNameLabel.text = username
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
//        self.tableView.frame = CGRectMake(0,0,200,CGRectGetHeight(self.view.frame))
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        blurImage()
    }
    
    func blurImage() {
        
        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        let blurView = UIVisualEffectView(effect: darkBlur)
        blurView.frame = self.view.bounds
        self.view.addSubview(blurView)
        self.view.sendSubviewToBack(blurView)
        
    }
    // MARK: - Table view data source
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        let selectionView = UIView()
        selectionView.backgroundColor = SELECT_FOCUS_COLOR
        cell.selectedBackgroundView = selectionView
    }
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
         let cell =  self.tableView.cellForRowAtIndexPath(indexPath)
        cell?.selected = false
        if indexPath.section == 0 {
            
            switch indexPath.row {
                case 0: print("User Image")
                    self.sideMenuController()?.menuDelegate?.menuViewProfile()
//                    self.dismissViewControllerAnimated(true, completion: { () -> Void in
//                        self.delegate?.menuViewProfile()
//                    })
                case 1:
                    self.sideMenuController()?.menuDelegate?.menuViewProfile()
                case 2:
                    print("Search")
                    self.sideMenuController()?.menuDelegate?.menuSearch()
//                    self.dismissViewControllerAnimated(true, completion: { () -> Void in
//                        self.delegate?.menuSearch()
//                    })
                
                case 3:
                    self.sideMenuController()?.menuDelegate?.menuViewGroup()
//                    self.sideMenuController()?.toggleSidePanel()
//                    self.dismissViewControllerAnimated(true, completion: nil)
//                    self.delegate?.menuGoToCurrentSong()
                
                case 4:
                    self.sideMenuController()?.menuDelegate?.menuViewLobeLive()
                case 5: print("Notifications")
                    self.sideMenuController()?.menuDelegate?.menuViewLocalLibrary()
                case 6: print("Settings")
                
                default: break
            }
        }else{
            switch indexPath.row {
                case 0:
                    print("")
                case 1:
                    print("Leave Group")
//                    self.sideMenuController()?.toggleSidePanel()
//                    self.dismissViewControllerAnimated(true, completion: nil)
                    self.sideMenuController()?.menuDelegate?.menuLeaveGroup()
                
                case 2:
                    print("Recalculate Sync")
                    self.sideMenuController()?.menuDelegate?.menuRecalculateSync()
//                    self.sideMenuController()?.toggleSidePanel()
//                    self.dismissViewControllerAnimated(true, completion: { () -> Void in
//                        self.delegate?.menuRecalculateSync()
//                    })
    
                case 3:
                    print("Logout")
//                    self.sideMenuController()?.menuDelegate?.menuLogout()
//                    self.sideMenuController()?.toggleSidePanel()
//                    self.dismissViewControllerAnimated(true, completion: { () -> Void in
//                        self.delegate?.menuLogout()
//                    })
                
                default: break
            }
        }
    }
}
