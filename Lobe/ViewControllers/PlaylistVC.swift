//
//  PlaylistVC.swift
//  Lobe
//
//  Created by Professional on 2015-11-17.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import Foundation
import MediaPlayer
import SwiftyJSON
import Alamofire
import RealmSwift
import BLKFlexibleHeightBar
import JLToast
import Bond
import PureLayout
import ZFDragableModalTransition
import MGSwipeTableCell

class PlaylistVC: UIViewController {
    
    var tapCloseButtonActionHandler : (Void -> Void)?
    
    @IBOutlet var musicImgLeadingConstraint: NSLayoutConstraint!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var popoverView: UIViewController!
    
    var mediaItems = [MPMediaItem]()
    var musicTracks = [TrackInfo]()
    
    var songItemList:NSMutableArray = NSMutableArray()//[SongItem]()
    var collection: MPMediaItemCollection?
    
    var sectionTitle: String = ""
    var cellEditable = false
    var isPlayingExist: Bool = false
    var realm: Realm!
    //    var bar:BLKFlexibleHeightBar?
    var delegateSplitter: BLKDelegateSplitter?
    var delegateLobeList: LobeListDelegate?
    
    var userHasSelectPlaylist: Bool = false
    var persistendIDOfSelectedUserPlaylist: String = ""
    
    var nowPlayingSongRow = 0
    var indexOfCurrentPlayingMusic: NSIndexPath = NSIndexPath(forItem: 0, inSection: 0)
    
    let resumeButton = UIButton()
    override func viewDidLoad() {
        super.viewDidLoad()
        
//            self.tableView.separatorStyle = .None
//        self.tableView.setEditing(true, animated: false)
        self.tableView.allowsSelectionDuringEditing = true

        sectionTitle = "New Lobe List"
        realm = try! Realm()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "alertToSave", name:"SaveNotification", object: nil)
        //deletePlist   CellMovedNotification
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateAfterDelete", name:"deletePlist", object: nil)
        
        //mediaItemPlayingNotification
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updatePlaylist", name:"UpdateNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "selectedPlaylist:", name:"UserHasSelectPlaylistNotification", object: nil)
        
        LobeStateHelper.sharedInstance.addLobeStateObserver(self)
        LobeStateHelper.sharedInstance.addUserStateObserver(self)
        AUDIO_OUT_MANAGER.observeMusicChange(self)
//        AUDIO_OUT_MANAGER.observeLobeListChange(self)
        
        createResumeButton()
        
        dispatch_async(dispatch_get_main_queue()) {
            self.observeCurrentSongChange()
        }
        
    }
    
    func createResumeButton(){
        
        resumeButton.backgroundColor = UIColor.orangeColor()
        resumeButton.setTitle("Resume", forState: UIControlState.Normal)
        resumeButton.titleLabel!.frame = CGRectMake(0, 0, 200, 40)
        resumeButton.addTarget(self, action: "resumeButtonTapped:", forControlEvents: UIControlEvents.TouchUpInside)
        resumeButton.tag = 22;
        resumeButton.titleLabel!.font = UIFont.systemFontOfSize(14)
        resumeButton.reversesTitleShadowWhenHighlighted = true
        resumeButton.hidden = true
        resumeButton.layer.cornerRadius = 20
        self.view.addSubview(resumeButton)
        
        resumeButton.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsets(top: 0, left: 90.0, bottom: 20.0, right: 90.0), excludingEdge: .Top)
        resumeButton.autoSetDimension(.Height, toSize: 40)
    }
    
    func resumeButtonTapped(sender: UIButton!){
        //TODO: - Change this . We dont want to use the nowPlayingSongRow anymore
        if AUDIO_OUT_MANAGER.lobeListSongItems.count > 0  && LobeStateHelper.sharedInstance.state == .Local {
            AUDIO_OUT_MANAGER.musicPlayerState = .LobeList
            AUDIO_OUT_MANAGER.currentSong = AUDIO_OUT_MANAGER.lobeListSongItems[AUDIO_OUT_MANAGER.currentLobeListSongIndex] as! SongItem
            AUDIO_OUT_MANAGER.playMusic()
            scrollToCurrentPlayingItem()

        }

    }
    
    func scrollToCurrentPlayingItem() {
        
        if AUDIO_OUT_MANAGER.lobeListSongItems.count > 0 {
            let indexOfCurrentPlayingMusic = AUDIO_OUT_MANAGER.getIndexOfCurrentPlayingLobeListTrack()
             let indexPath = NSIndexPath(forRow: indexOfCurrentPlayingMusic, inSection: 0)
            tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
        }
    }
    
    func selectItem(atRow row:Int, andSection section:Int){
        let indexPath = NSIndexPath(forRow: row, inSection: section);
        self.tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: UITableViewScrollPosition.None)
        self.tableView(self.tableView, didSelectRowAtIndexPath: indexPath)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        updateResumeButton()

    }
    func lobeStateChanged(){
    }
    
    func actOnMusicChange(notification:NSNotification){
    }
    
    func updateResumeButton(){
        
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            
            if (AUDIO_OUT_MANAGER.musicPlayerState == .Library || !AUDIO_OUT_MANAGER.isPlaying) && AUDIO_OUT_MANAGER.lobeListSongItems.count > 0{
                UIView.animateWithDuration(0.2, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                    self.resumeButton.alpha = 1
                    }, completion: { finished in
                        self.resumeButton.hidden = false
                })
                
            }else{
                UIView.animateWithDuration(0.4, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                    self.resumeButton.alpha = 0
                    }, completion: { finished in
                        self.resumeButton.hidden = true
                })
            }
            
            if AUDIO_OUT_MANAGER.isGroupMusicPlaying {
                self.resumeButton.hidden = true
            }
        })

    }
    func updateState(){
       
    }
    
    func userStateChanged(){
        print("UTILISATEUR CHANGER")
        for songItem in ( AUDIO_OUT_MANAGER.lobeListSongItems as NSArray as! [SongItem]) {
            
            if songItem.trackInfo.mUniqueDeviceId == unique_device_id {
                
                songItem.trackInfo.mUser = LobeStateHelper.currentUser
                songItem.trackInfo.mIsYoutubeVideo = false
                songItem.trackInfo.mUserOwnerName = LobeStateHelper.currentUser.userName.value!
                songItem.trackInfo.mUserOwnerUid = LobeStateHelper.currentUser.parseID
            }
        }
        tableView.reloadData()
        
    }
    
    
    // Mark - Custom Menu Function
    func showBLKMenuView(){
        
        let bar = BLKFlexibleHeightBar(frame: CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 60))
        
        bar.minimumBarHeight = 0.0
        bar.backgroundColor = UIColor.blackColor()
        bar.alpha = 0.9
        
        bar.behaviorDefiner = FacebookStyleBarBehaviorDefiner()
        self.tableView.delegate = bar.behaviorDefiner as? UITableViewDelegate
        
        bar.behaviorDefiner.addSnappingPositionProgress(0.0, forProgressRangeStart:0.0, end:0.5)
        bar.behaviorDefiner.addSnappingPositionProgress(1.0, forProgressRangeStart:0.5, end:1.0)
        
        bar.behaviorDefiner.snappingEnabled = false
        //            bar.behaviorDefiner.elasticMaximumHeightAtTop = true
        
        let button = UIButton(frame: CGRectMake(10, 10, 80, 40))
        button.setTitle("Save", forState: UIControlState.Normal)
        button.titleLabel?.textColor = UIColor.whiteColor()
        
        // Configure a separate UITableViewDelegate and UIScrollViewDelegate (optional)
        self.delegateSplitter = BLKDelegateSplitter(firstDelegate: bar.behaviorDefiner, secondDelegate: self)
//        self.tableView.delegate = self.delegateSplitter
        
        self.view.addSubview(bar)
        
    }
    
    func tapped(sender: UIButton) {
        print("save")
    }
    
}

//==========================================================================================
// MARK: - Observers
//==========================================================================================
extension PlaylistVC {
    
    func observeCurrentSongChange(){
        
        AUDIO_OUT_MANAGER.currentTrackObservable.observe{ currentTrack in
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }
        }
        
        //Observe LobeList
        AUDIO_OUT_MANAGER.lobeListObservableList.observe{ newList in
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }
        }
        
        //Observe audio playing state
        AUDIO_OUT_MANAGER.isAudioPlaying.observe{ currentTrack in
            dispatch_async(dispatch_get_main_queue()) {
                self.updateResumeButton()
            }
        }
        
        //Observe voting map list 
        AUDIO_OUT_MANAGER.socialVotingMapListChangeObserver.observe{ currentTrack in
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }
        }
        
    }
}


//==========================================================================================
// MARK: - UITableViewDataSource
//==========================================================================================
extension PlaylistVC: UITableViewDataSource {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCellWithIdentifier("PlaylistCell", forIndexPath: indexPath) as! PlaylistCell
        let cell = tableView.dequeueReusableCellWithIdentifier("PlaylistCell") as! PlaylistCell
        let songItem = AUDIO_OUT_MANAGER.lobeListSongItems[indexPath.section] as! SongItem
        
        cell.songItem = songItem
        //Hide reoder button
        self.tableView.setEditing(false, animated: false)
        //Used for change the view
        var isCurrentPlayingSong = false
        switch LobeStateHelper.sharedInstance.state {
        case .Local:
            isCurrentPlayingSong = AUDIO_OUT_MANAGER.currentLobeListSongIndex == indexPath.section ? true : false
        case .DJ,.Participant:
            isCurrentPlayingSong = AUDIO_OUT_MANAGER.currentSong.trackInfo.mID == songItem.trackInfo.mID ? true : false
        }
        
        if isCurrentPlayingSong {
            cell.cellImgLeadingConstraint.constant = 90
            cell.titleLabel.textColor = CURRENT_PLAYING_COLOR
        }else{
            cell.cellImgLeadingConstraint.constant = 1
            cell.titleLabel.textColor = UIColor.whiteColor()
        }
        
        
        cell.onRemoveTrackToLobeList = { [unowned self] (MusicCell) -> Void in
            self.handleRemoveTrack(songItem,index: indexPath.section)
        }
        
        return cell
    }
    
    func handleRemoveTrack(songItem:SongItem,index:Int){
        
        let lockQueue = dispatch_queue_create("lobeList.LockQueue", nil)
        dispatch_sync(lockQueue) {
            switch LobeStateHelper.sharedInstance.state {
                
            case .Local:
                if (AUDIO_OUT_MANAGER.lobeListContains(songItem)){
                    AUDIO_OUT_MANAGER.lobeListSongItems.removeObjectAtIndex(index)
                    AUDIO_OUT_MANAGER.localObservableTrackList.removeAtIndex(index)
                }
            case .DJ:
                NativeToWebHelper.sharedInstance.deleteTrackFromLobeList(songItem.trackInfo.mID)
            case .Participant:
                NativeToWebHelper.sharedInstance.dislikeTrackInLobeList(-1, trackUid: songItem.trackInfo.mID)
                
            }
            dispatch_async(dispatch_get_main_queue()) {
                AUDIO_OUT_MANAGER.notifyLobeListChange()
            }
        }
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  1
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return  AUDIO_OUT_MANAGER.lobeListSongItems.count ?? 0
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.clearColor()
        return view
    }
}

//==========================================================================================
// MARK: - UITableViewDelegate
//==========================================================================================
extension PlaylistVC: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if AUDIO_OUT_MANAGER.musicPlayerState != .LobeList{
            AUDIO_OUT_MANAGER.musicPlayerState = .LobeList
        }
        print("section index: \(indexPath.section)")
        print("section row: \(indexPath.row)")
         print("indexPath : \(indexPath)")
        let songItem =  AUDIO_OUT_MANAGER.lobeListSongItems[indexPath.section] as! SongItem
//        let cell =  self.tableView.cellForRowAtIndexPath(indexPath) as! PlaylistCell
        switch LobeStateHelper.sharedInstance.state {
            
            case .Local:
                AUDIO_OUT_MANAGER.currentSong = songItem
                AUDIO_OUT_MANAGER.currentLocalSongIndex = indexPath.section
            case .DJ:
                AUDIO_OUT_MANAGER.currentSong = songItem
            case .Participant:
                let message = "Can't play the song from library in Social Mode. You need to use lobelist"
                print("\(message)")
                JLToast.makeText(message).show()
        }

        AUDIO_OUT_MANAGER.directPlayMusic()
        tableView.reloadData()
        
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    //MARK: - Drag & drop native methode
    func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.None
    }
    func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    func tableView(tableView: UITableView, moveRowAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath) {
        print("source: \(sourceIndexPath)")
         print("destination: \(destinationIndexPath)")
        //songItemList.exchangeObjectAtIndex(sourceIndexPath.section, withObjectAtIndex: destinationIndexPath.section)
        AUDIO_OUT_MANAGER.lobeListSongItems.exchangeObjectAtIndex(sourceIndexPath.section, withObjectAtIndex: destinationIndexPath.section)
        self.tableView.reloadData()
    }
}

extension PlaylistVC {
    
    //MARK: - Alert View with ActionSheet
    func showOptionList(sender: UIButton){
        let songsName = self.mediaItems[sender.tag].valueForProperty(MPMediaItemPropertyTitle) as! String
        let songsArtist = self.mediaItems[sender.tag].valueForProperty(MPMediaItemPropertyArtist) as? String
        let itemArtwork = self.mediaItems[sender.tag].valueForProperty(MPMediaItemPropertyArtwork) as? MPMediaItemArtwork
        var imageView: UIImageView
        let titleStr = "\n\n\n\n"
        
        if itemArtwork != nil{
            let img = itemArtwork!.imageWithSize(CGSize(width: 30.0, height: 30.0))
            imageView  = UIImageView(image: img!)
            imageView.layer.cornerRadius = 5
            
            //            print("\(itemArtwork?.bounds.size) ====== \(itemArtwork?.bounds.width)")
            //            titleStr = String(count: Int((itemArtwork?.bounds.height)!/50),repeatedValue: Character("\n"))
            
        }else{
            imageView = UIImageView(image: UIImage(named: "logo_lobe_square_no_gradient_Faded_64.png")!)
        }
        
        let alert = UIAlertController(title: titleStr, message: "", preferredStyle: .ActionSheet)
        
        
        let margin:CGFloat = 8.0
        let rect = CGRectMake(margin, margin, 330.0, 70.0)
        let customView = UIView(frame: rect)
        customView.backgroundColor = UIColor.clearColor()
        alert.view.addSubview(customView)
        
        let labelSongsName = UILabel(frame: CGRectMake(100.0, 10.0, 200.0, 20.0))
        labelSongsName.text = "\(songsName)".capitalizedString
        customView.addSubview(labelSongsName)
        
        if songsArtist != nil {
            let labelSongsArtist = UILabel(frame: CGRectMake(100.0, 32.0, 200.0, 20.0))
            labelSongsArtist.text = "\(songsArtist!)".capitalizedString
            customView.addSubview(labelSongsArtist)
        }
        
        //        itemArtwork!.frame = CGRect(x: 5.0, y: 20.0, width: 6, height: 6)
        customView.addSubview(imageView)
        
        
        let firstAction = UIAlertAction(title: "Remove", style: .Default) { (alert: UIAlertAction!) -> Void in
//            let pageDict: Dictionary<String,String>! = [
//                "presistentID": "\(self.mediaItems[sender.tag])",
//            ]
//            self.toastAlert("Song remove form LobeList")
            NSNotificationCenter.defaultCenter().postNotificationName("trackToRemoveNotification", object: nil, userInfo: nil)
            self.toastAlert("Song remove form LobeList")
            print("Delete")
        }
        
        alert.addAction(firstAction)
        //        alert.view.tintColor = UIColor(fromHexCode: "D91809")
        //        alert.view.tintColor = UIColor(fromHexCode: "191b25")
        let secondAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: {(alert: UIAlertAction!) in
            print("cancel")
        })
        alert.addAction(secondAction)
        presentViewController(alert, animated: true, completion:nil)
        
    }
    
    func selectedPlaylist(notification: NSNotification){
        
        let playlistID = notification.userInfo!["playlistID"] as! String
        let savedPlaylists = realm.objects(UserPlaylist).filter("idPlaylist_='\(playlistID)'")
        
        mediaItems.removeAll()
        if savedPlaylists.count > 0 {
            
            for elem in savedPlaylists{
                sectionTitle = elem.playlistName_
                
                for track  in elem.tracks_  {
                    
                    mediaItems.append(self.findSongWithPersistentIdString(track.idTrack_)!)
                    collection = MPMediaItemCollection(items: mediaItems )
                    self.tableView.reloadData()
                }
            }
        }else{
            //            print(playlistID)
            //            mediaItems.removeAll()
            
            
            let playlistPredicate = MPMediaPropertyPredicate(value: playlistID, forProperty: MPMediaPlaylistPropertyPersistentID)
            //MPMediaPlaylistPropertyName
            let playlist = MPMediaQuery()
            playlist.addFilterPredicate(playlistPredicate)
            //playlistName
            sectionTitle = notification.userInfo!["playlistName"] as! String
            for elem  in playlist.items!  {
                
                mediaItems.append(elem)
                collection = MPMediaItemCollection(items: mediaItems )
                self.tableView.reloadData()
            }
        }
    }
    
    func updatePlaylist(){
        
        let savedPlaylists = realm.objects(UserPlaylist).filter("playlistName_='\(sectionTitle)'")
        
        if savedPlaylists.count > 0 {
            for elem in savedPlaylists{
                try! realm.write {
                    realm.delete(elem)
                }
                
            }
            if self.mediaItems.count > 0 {
                let newPlaylist = UserPlaylist()
                
                newPlaylist.idPlaylist_ = NSUUID().UUIDString
                newPlaylist.playlistName_ = sectionTitle
                
                for elem in self.mediaItems{
                    let newTrack = UserTrack()
                    newTrack.idTrack_ = ((elem.valueForProperty(MPMediaItemPropertyPersistentID))?.stringValue)!
                    newTrack.trackName_ = (elem.valueForProperty(MPMediaItemPropertyTitle) as? String
                        )!
                    newPlaylist.tracks_.append(newTrack)
                    try! self.realm.write({ () -> Void in
                        self.realm.add(newTrack)
                    })
                    
                }
                
                try! self.realm.write({ () -> Void in
                    self.realm.add(newPlaylist)
                })
                
                self.tableView.reloadData()
                self.toastAlert("Playlist updated...")
            }
        }
    }
    
    func toastAlert(message: String){
        JLToastView.setDefaultValue(
            UIColor.redColor(),
            forAttributeName: JLToastViewBackgroundColorAttributeName,
            userInterfaceIdiom: .Phone
        )
        JLToastView.setDefaultValue(
            NSNumber(double: 100.0),
            forAttributeName: JLToastViewPortraitOffsetYAttributeName,
            userInterfaceIdiom: .Phone
        )
        JLToastView.setDefaultValue(
            UIFont.systemFontOfSize(25),
            forAttributeName: JLToastViewFontAttributeName,
            userInterfaceIdiom: .Phone
        )
        JLToast.makeText(message, duration: JLToastDelay.ShortDelay).show()
    }
    
    func findSongWithPersistentIdString(persistentIDString: String) -> MPMediaItem? {
        let predicate = MPMediaPropertyPredicate(value: persistentIDString, forProperty: MPMediaItemPropertyPersistentID)
        let songQuery = MPMediaQuery()
        songQuery.addFilterPredicate(predicate)
        
        var song: MPMediaItem?
        if let items = songQuery.items where items.count > 0 {
            song = items[0]
        }
        return song
    }
    
    func updateAfterDelete(){
        sectionTitle = "New Lobe List"
        self.tableView.reloadData()
    }
    func alertToSave(){
        
        let alertController = UIAlertController(title: "Save Playlist", message:
            "", preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "New Playlist Name"
        }
        alertController.addAction(UIAlertAction(title: "Save", style: UIAlertActionStyle.Default,handler: { (action) -> Void in
            let textField = alertController.textFields![0] as UITextField
            if (textField.text != "")  && (self.mediaItems.count > 0) {
                
                //Override playlist if has same name
                let savedPlaylists = self.realm.objects(UserPlaylist).filter("playlistName_='\(textField.text!)'")
                if savedPlaylists.count > 0 {
                    for elem in savedPlaylists{
                        try! self.realm.write {
                            self.realm.delete(elem)
                        }
                        
                    }
                }
                let newPlaylist = UserPlaylist()
                newPlaylist.idPlaylist_ = NSUUID().UUIDString
                newPlaylist.playlistName_ = textField.text!
                
                for elem in self.mediaItems{
                    let newTrack = UserTrack()
                    newTrack.idTrack_ = ((elem.valueForProperty(MPMediaItemPropertyPersistentID))?.stringValue)!
                    newTrack.trackName_ = (elem.valueForProperty(MPMediaItemPropertyTitle) as? String
                        )!
                    newPlaylist.tracks_.append(newTrack)
                    try! self.realm.write({ () -> Void in
                        self.realm.add(newTrack)
                    })
                    
                }
                
                try! self.realm.write({ () -> Void in
                    self.realm.add(newPlaylist)
                })
                self.sectionTitle = textField.text!
                self.tableView.reloadData()
                self.toastAlert("\(textField.text!) is saved")
                let pageDict: Dictionary<String,String>! = [
                    "playlistName": "\(textField.text!)",
                ]
                NSNotificationCenter.defaultCenter().postNotificationName("NewNameNotification", object: nil, userInfo: pageDict)
            }
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default,handler: { (action) -> Void in
            self.dismissViewControllerAnimated(true, completion: nil)
        }))
        self.presentViewController(alertController, animated: true){
            
        }
        //        print("save case")
    }
    
}


extension PlaylistVC{
    
    @IBAction func createGroup(sender: UIButton) {
        let lower : UInt32 = 1000
        let upper : UInt32 = 9999
        let randomNumber = arc4random_uniform(upper - lower) + lower
        NativeToWebHelper.sharedInstance.createGroupSession("'\(randomNumber)'")
        print("group Created: \(randomNumber)")
//        groupNameLabel.text = "Group number: \(randomNumber)"
//        suggestionDelegate!.openWebView()
    }
    
    @IBAction func tapCloseButton() {
        self.tapCloseButtonActionHandler?()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
