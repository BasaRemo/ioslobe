//
//  ContainerVC.swift
//  Lobe
//
//  Created by Professional on 2015-12-28.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import MXSegmentedPager

class ContainerVC: MXSegmentedPagerController {

    @IBOutlet var backgroundImage:UIImageView!
    @IBOutlet var countryFlag: UIImageView!
    @IBOutlet var businessName: UILabel!
    @IBOutlet var busineDescription: UILabel!
    @IBOutlet var countryName: UILabel!
    @IBOutlet var closeNowLabel: UILabel!
    
    var titles = ["Library", "LobeList","Suggest"]
    var headerView:ProfileView!
    var musicVC: MusicVC?
    var playlistVC: PlaylistVC?
    var suggestionsVC: SuggestionsVC?
    var suggestVC:SuggestionsVC?
    var webViewVC: LobeWebVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webViewVC = self.storyboard?.instantiateViewControllerWithIdentifier("LobeWebVC") as? LobeWebVC
        setupHeaderView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    func userStateChanged(){
    }
    
    // Calls update function for all the views and viewControllers
    func updateState(){
        headerView.updateState()
        musicVC!.updateState()
        playlistVC!.updateState()
//        suggestVC!.updateState()
//        webViewVC!.updateState()
        makeChanges()
    }
    
    func makeChanges(){
        
        func updateState(){
            print("ProfilView update")
            
            switch LobeStateHelper.sharedInstance.state {
                
            case .Local:
                playlistVC!.view.backgroundColor = UIColor.clearColor()
                musicVC!.view.backgroundColor = UIColor.clearColor()
                
            case .DJ:
                playlistVC!.view.backgroundColor = DJ_COLOR
                musicVC!.view.backgroundColor = DJ_COLOR
                
            case .Participant:
                playlistVC!.view.backgroundColor = PARTICIPANT_COLOR
                musicVC!.view.backgroundColor = PARTICIPANT_COLOR
                
            }
            // TODO: Get currrent song meta data
        }
    }
    func setupHeaderView(){
        // Parallax Header
        let uiview = NSBundle.mainBundle().loadNibNamed(ProfileViewCellIdentifier, owner: self, options: nil)[0] as? ProfileView
        headerView = uiview
        headerView.delegate = self
        setupSegmentPager()
    }
    
    func setupSegmentPager(){
        
        self.segmentedPager.backgroundColor = UIColor.clearColor()
        self.segmentedPager.parallaxHeader.view = headerView
        self.segmentedPager.parallaxHeader.mode = MXParallaxHeaderMode.Fill
        self.segmentedPager.parallaxHeader.height = 500;
        self.segmentedPager.parallaxHeader.minimumHeight = 75;
        
        // Segmented Control customization
        self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
        self.segmentedPager.segmentedControl.backgroundColor = SEGMENT_MENU_COLOR
  
        self.segmentedPager.segmentedControl.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.hexColor("969696"), NSBackgroundColorAttributeName: UIColor.clearColor(), NSFontAttributeName:  UIFont(name: "HelveticaNeue-Light", size: 15)!]
        self.segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSForegroundColorAttributeName : UIColor.hexColor("969696")]
        self.segmentedPager.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe
        self.segmentedPager.segmentedControl.selectionIndicatorColor = UIColor.hexColor("D2D2D2")
    }
    
}


extension ContainerVC:ProfileViewDelegate{
    
    func openWebView(){
        self.webViewVC?.modalPresentationStyle = .Custom
        self.presentViewController(self.webViewVC!, animated: true, completion: nil)
        
    }
    
    func expandMusicList(){
//        print("Expand!")
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            
//            self.segmentedPager.contentView.setContentOffset(CGPointMake(0, 100), animated: true)
//            self.segmentedPager.contentView.scrollEnabled = true
            //            self.segmentedPager.contentView.contentInset = UIEdgeInsetsMake(300, 0, 0, 0);
            
            //            var vc = self.segmentedPager(self.segmentedPager, viewControllerForPageAtIndex: 1) as? PlaylistVC
            //            self.setPageViewController(vc!)
        })
    }
    
    func createGroup() {
        print("Group Creation")
        NativeToWebHelper.sharedInstance.createGroupSession("999") //3 digit
    }
}


extension ContainerVC{
    
    override func segmentedPager(segmentedPager: MXSegmentedPager, titleForSectionAtIndex index: Int) -> String {
        return ["Library", "LobeList"][index];
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
//        if let vc = segue.destinationViewController as? MusicVC
//            where segue.identifier == "mx_page_0" {
////                print("SEGUE mx_page_0")
//                if self.musicVC == nil {
//                    self.musicVC = vc
//                }
//        }
//        
//        if let vc = segue.destinationViewController as? PlaylistVC
//            where segue.identifier == "mx_page_1" {
////                print("SEGUE mx_page_1")
//                if self.playlistVC == nil {
//                    self.playlistVC = vc
//                    if let _ = self.musicVC {
//                        self.musicVC?.lobeListDelegate = self.playlistVC
//                    }
//                }
//        }
//        
//        if let vc = segue.destinationViewController as? SuggestionsVC
//            where segue.identifier == "mx_page_2" {
////                print("SEGUE mx_page_2")
//                if self.suggestVC == nil {
//                    self.suggestVC = vc
//                }
//        }
    }

}

