//
//  LobeWebVC.swift
//  Lobe
//
//  Created by Professional on 2015-12-08.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import WebKit
import SwiftyJSON
import ZFDragableModalTransition
import Cordova

class LobeWebVC: CDVViewController {

    @IBOutlet var magicButton: UIButton!
    @IBOutlet var menuButton:UIButton!
    var mWebView: WKWebView?
    var menuAnimator:ZFModalTransitionAnimator!
    override func prefersStatusBarHidden() -> Bool {
        return true;
    }
    
    func updateState(){
        print("WebView Update")
    }    
    
    func userStateChanged(){
    }
    override func loadView() {
        super.loadView()

    }
//    override func loadView() {
//        super.loadView()
//        let contentController = WKUserContentController();
////        let userScript = WKUserScript(
////            source: "redHeader()",
////            injectionTime: WKUserScriptInjectionTime.AtDocumentEnd,
////            forMainFrameOnly: true
////        )
////        contentController.addUserScript(userScript)
//        
//        // Javascript that disables pinch-to-zoom by inserting the HTML viewport meta tag into <head>
////        let source: NSString = "var meta = document.createElement('meta');" +
////            "meta.name = 'viewport';" +
////            "meta.content = 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no';" +
////            "var head = document.getElementsByTagName('head')[0];" +
////        "head.appendChild(meta);";
////        let script: WKUserScript = WKUserScript(source: source as String, injectionTime: .AtDocumentEnd, forMainFrameOnly: true)
////        
////        // Create the user content controller and add the script to it
////        let userContentController: WKUserContentController = WKUserContentController()
////        userContentController.addUserScript(script)
//        
//     
//        contentController.addScriptMessageHandler(
//            self,
//            name: "initiatePlayMusicFromUrl"
//        )
//        
//        contentController.addScriptMessageHandler(
//            self,
//            name: "notifyJavaForTimeDiffWithFirebase"
//        )
//        
//        contentController.addScriptMessageHandler(
//            self,
//            name: "testIOSNative"
//        )
//        
//        for webNativeCall in WebToNativeCalls.callList {
//            contentController.addScriptMessageHandler(
//                self,
//                name: webNativeCall
//            )
//        }
//
//        let config = WKWebViewConfiguration()
//        config.userContentController = contentController
//        config.allowsInlineMediaPlayback = true
//        config.preferences.javaScriptCanOpenWindowsAutomatically = true
//        config.preferences.javaScriptEnabled = true
//        
//        var tempFrame = self.view.bounds
//        tempFrame.origin.y = 22
//        tempFrame.size.height = self.view.frame.size.height - 75
//        
//        self.mWebView = WKWebView(
//            frame: tempFrame,
//            configuration: config
//        )
//        self.mWebView?.translatesAutoresizingMaskIntoConstraints = true
//        self.mWebView?.tag = 100
//        self.mWebView?.scrollView.delegate = self
//        self.view.addSubview(self.mWebView!)
////        self.view.sendSubviewToBack(self.view.viewWithTag(100)!)
////        self.view.insertSubview(self.webView!, belowSubview: self.view.viewWithTag(15)!)
//    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.mWebView!.navigationDelegate = self
        
        var tempFrame = self.view.bounds
        tempFrame.origin.y = 22
        tempFrame.size.height = self.view.frame.size.height - 55
        self.webView.frame = tempFrame
        self.webView.allowsInlineMediaPlayback = true
        NativeToWebHelper.sharedInstance.delegate = self
        HybridBridge.sharedInstance = HybridBridge(webView: self.webView)
        HybridBridge.sharedInstance.genericJavascriptCall("receivedNativeMessage", params: "")
        
        LobeStateHelper.sharedInstance.addMagicButtonStateObserver(self)
        
//        let url = NSURL(string:"http://lobemusic.com/ionicIndex_ios.html")
//        let req = NSURLRequest(URL:url!)
//        self.mWebView!.loadRequest(req)
        
//        var pluginResult:CDVPluginResult  = CDVPluginResult(status: CDVCommandStatus_OK);

        
 
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
//        self.webView.stringByEvaluatingJavaScriptFromString("receivedNativeMessage()")
//        if let _ = LobeStateHelper.sharedInstance.pushNotif {
//            print("sending push notif to web")
//            NativeToWebHelper.sharedInstance.JavaCallPushNotifActionReceived()
//            LobeStateHelper.sharedInstance.pushNotif = nil
//        }
        
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)

//        HybridBridge.sharedInstance.genericJavascriptCall("receivedNativeMessage", params: "")
    }

    @IBAction func menuButtonPressed(sender: UIButton) {
        print("toggle menu")
        self.sideMenuController()?.menuDelegate = self.parentViewController as! MainVC
        self.sideMenuController()?.toggleSidePanel()
    }
    
    @IBAction func closeBtnPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}

//extension LobeWebVC{
//    
////    override func webView(webView: UIWebView, didFailLoadWithError error: NSError!) {
////        print("Webview fail with error \(error)");
////    }
////    
////    override func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
////        return true;
////    }
////    
////    override func webViewDidStartLoad(webView: UIWebView) {
////        print("Webview started Loading")
////    }
//    
////    override func webViewDidFinishLoad(webView: UIWebView){
////        print("Webview did finish load")
////        WebToNativePlugin.sharedInstance = WebToNativePlugin(webView: self.webView)
////        WebToNativePlugin.sharedInstance.genericJavascriptCall("receivedNativeMessage", params: "")
////    }
//    
//}

extension LobeWebVC:UIScrollViewDelegate{
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return nil
    }
}
extension LobeWebVC:NativeToWebHelperDelegate{
    
    func genericFunctionCall(functionName:String, params:String){
//        print("Native to Web Call bridge : \(functionName) params : \(params)")
//        self.webView.stringByEvaluatingJavaScriptFromString("\(functionName)(\(params))")
        HybridBridge.sharedInstance.genericJavascriptCall(functionName,params: params)
        
//        self.mWebView!.evaluateJavaScript("\(functionName)(\(params))",completionHandler: { ( result, error) -> Void in
//            if let result = result{
//                print("result: \(result)")
//            }
//        })
//        self.webView!.evaluateJavaScript("fromNativeTest('test')",completionHandler: { ( result, error) -> Void in
//            if let result = result{
//                print("result: \(result)")
//            }
//        })
    }
}
extension LobeWebVC:WKNavigationDelegate{
    
    func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
        //Call JAVASCRIPT
        NativeToWebHelper.sharedInstance.resumeEvent()
    }
    override func webViewDidStartLoad(webView: UIWebView) {
//        LocationHelper.sharedInstance.manager.requestWhenInUseAuthorization()
//        LocationHelper.sharedInstance.manager.requestAlwaysAuthorization()
    }
    override func webViewDidFinishLoad(webView: UIWebView) {
        
//        if CLLocationManager.locationServicesEnabled() {
//            switch(CLLocationManager.authorizationStatus()) {
//                case .NotDetermined, .Restricted, .Denied:
//                    print("No location access")
//                case .AuthorizedAlways, .AuthorizedWhenInUse:
//                    print("Got location permission")
//                    LobeStateHelper.sharedInstance.user_location_latitude = (LocationHelper.sharedInstance.manager.location?.coordinate.latitude)!
//                    LobeStateHelper.sharedInstance.user_location_longitude = (LocationHelper.sharedInstance.manager.location?.coordinate.longitude)!
//            }
//        } else {
//            print("Location services are not enabled")
//        }
        
    }
    override func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        
    }
}



extension LobeWebVC:WKScriptMessageHandler{
    
    func userContentController(userContentController: WKUserContentController, didReceiveScriptMessage message: WKScriptMessage) {
//        var selector = "\(message.name)"
//        WebToNativeHelper.sharedInstance.performSelector(selector, withObject: message.body)
        print("call \(message.name): \(message.body)")
        let json = JSON(message.body)

        switch message.name {
            
        case WebToNativeCalls.testCall:
            print("JSON testCall: \(json)")
            WebToNativeHelper.sharedInstance.testCall(json[0].string!)
            
        case WebToNativeCalls.showToastMessage:
            print("JSON testCall: \(json)")
            WebToNativeHelper.sharedInstance.showToastMessage(json[0].string!)
            
        case WebToNativeCalls.toJavaCallSetAudioSourceMode:
            WebToNativeHelper.sharedInstance.toJavaCallSetAudioSourceMode(json[0].string!)
            
        case WebToNativeCalls.playNotifSound:
            WebToNativeHelper.sharedInstance.playNotifSound(json[0].string!)
            
        case WebToNativeCalls.setIsSignedInState:
            WebToNativeHelper.sharedInstance.setIsSignedInState(json[0].bool!)
            
        case WebToNativeCalls.setMyPersonalInfo:
            WebToNativeHelper.sharedInstance.setMyPersonalInfo(json[0].string!, name: json[1].string!, picUrl: json[2].string!)
            
        case WebToNativeCalls.setLobeList:
            WebToNativeHelper.sharedInstance.setLobeList(json)
            
        case WebToNativeCalls.setSuggestionList:
            WebToNativeHelper.sharedInstance.setSuggestionList(json)
            
        case WebToNativeCalls.backToNormal:
            WebToNativeHelper.sharedInstance.backToNormal()
            
        case WebToNativeCalls.doSynchoFutureEventTest:
            print("JSON Synchro: \(json)")
        
        case WebToNativeCalls.resumeMusic: break
//            WebToNativeHelper.sharedInstance.resumeMusic()
            
        case WebToNativeCalls.pauseMusic:
            WebToNativeHelper.sharedInstance.pauseMusic()
            
        case WebToNativeCalls.setTimeDiffWithFirebase:
            print("JSON time: \(json)")
            //            let jsCurrentUnixTime_String:String = json[0].string!
            //            let jsCurrentUnixTime:CLong = CLong(jsCurrentUnixTime_String)!
            //
            //            let deltaF_JS_String:String = json[1].string!
            //            let deltaF_JS:CLong = CLong(deltaF_JS_String)!
            //            WebToNativeHelper.sharedInstance.setTimeDiffWithFirebase(jsCurrentUnixTime, deltaF_JS: deltaF_JS)
            
        case WebToNativeCalls.startStreamingFromUrl:
            print("JSON stream: \(json)")
            let time_stamp_String:Int64 = json[1].int64Value
            let music_start_time_delay_String:Int64 = json[2].int64Value
//
            print("time_stamp_String: \(time_stamp_String)")
            print("music_start_time_delay_String: \(music_start_time_delay_String)")
            
             WebToNativeHelper.sharedInstance.startStreamingFromUrl(json[0], server_time_stamp: time_stamp_String, music_start_time_delay: music_start_time_delay_String)
            break
            
        case WebToNativeCalls.toggleDrawerMenuClick:
            print("Draw Menu")
//            WebToNativeHelper.sharedInstance.toggleDrawerMenuClick()
//            showMenu()
            
        case WebToNativeCalls.groupUploadRequestReceived:
            WebToNativeHelper.sharedInstance.groupUploadRequestReceived(json[0])
            
        case WebToNativeCalls.addTrackUniqueToLobeList:
            WebToNativeHelper.sharedInstance.addTrackUniqueToLobeList(json[0])
            
        case WebToNativeCalls.createAllLocalFileChunks:
            WebToNativeHelper.sharedInstance.createAllLocalFileChunks(json[0], nbSecondsPerChunk: json[1].int!)
            
        case WebToNativeCalls.uploadFileToServer:
            
            let mCorrelationID:String = json[2].string!
            let mChunkStartTimeServer:Int64 = json[3].int64Value
            let mChunkIndex:Int64 = json[0].int64Value
            
            print("mCorrelationID: \(mCorrelationID)")
            print("mChunkStartTimeServerUInt64: \(mChunkStartTimeServer)")
            
            WebToNativeHelper.sharedInstance.uploadFileToServer(UInt32(mChunkIndex), localFilePath: json[1].string!, correlationID: mCorrelationID, chunkStartTimeServer: mChunkStartTimeServer)
            break
        case WebToNativeCalls.logFromSocial:
            print("")
        default:
            print("no message body")
        }
    }
    
}

extension LobeWebVC{
    
    func showMenu(){
        
        let menuVC = self.storyboard?.instantiateViewControllerWithIdentifier("MenuVC") as? MenuVC
        
//        menuVC?.delegate = self
        
        self.menuAnimator = ZFModalTransitionAnimator(modalViewController: menuVC!)
        self.menuAnimator.dragable = true
        self.menuAnimator.bounces = false
        self.menuAnimator.behindViewAlpha = 0.5
        self.menuAnimator.behindViewScale = 1.0
        self.menuAnimator.transitionDuration = 0.7
        self.menuAnimator.direction = ZFModalTransitonDirection.Left
        menuVC!.transitioningDelegate = self.menuAnimator
        menuVC!.modalPresentationStyle = UIModalPresentationStyle.Custom
        
        presentViewController(menuVC!, animated: true, completion: { () -> Void in
            //            self.menuAnimator.setContentScrollView(self.menuVC!.tableView)
        })
    }
}

//extension LobeWebVC:MenuDelegate{
//    
//    func menuSearch(){
////        self.goToSearchView()
//    }
//    func menuGoToCurrentSong(){
////        self.moveToCurrentPlayingItem(self)
//    }
//    func menuLeaveGroup(){
//        NativeToWebHelper.sharedInstance.leaveGroupSession()
//        
//    }
//    func menuRecalculateSync(){
//    }
//    func menuLogout(){
//    }
//    func menuViewProfile(){
//        NativeToWebHelper.sharedInstance.viewMyProfile()
//    }
//        
//}

extension LobeWebVC{
    
    func magicButtonStateChanged(){
//        print("Magic Changed")
//        switch LobeStateHelper.sharedInstance.magicButtonState {
//            case .localMusicView:
//                self.dismissViewControllerAnimated(false) { () -> Void in
//                     self.magicButton.hidden = false
//                    self.menuButton.hidden = false
//                    
//                }
//                break
//            case .lobeLiveView:
//                self.magicButton.hidden = false
//                self.menuButton.hidden = false
//                break
//            case .groupView:
//                self.magicButton.hidden = false
//                self.menuButton.hidden = false
//                break
//            case .myProfileView:
//                self.magicButton.hidden = true
//                self.menuButton.hidden = true
//                break
//            case .findLobersView:
//                self.magicButton.hidden = true
//                self.menuButton.hidden = true
//                break
//        }
    }
    
    //Unused: delete it
    @IBAction func magicButtonTapped(sender: AnyObject) {
        
//        let transition = CircularRevealTransition(layer: self.view.layer, center: self.magicButton.center)
//        transition.start()
//        WebToNativeHelper.sharedInstance.playNotifSound("tock")
////        LobeStateHelper.sharedInstance.magicButtonState.goToNextState()
//        self.dismissViewControllerAnimated(true) { () -> Void in
//
//        }

//        presentViewController(webViewVC!, animated: true, completion: { () -> Void in
//            //            self.animator2.setContentScrollView(self.webViewVC?.webView?.scrollView)
//            NativeToWebHelper.sharedInstance.viewMyProfile()
//        })
    }
}

