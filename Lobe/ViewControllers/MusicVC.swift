//
//  MusicVC.swift
//  Lobe
//
//  Created by Professional on 2015-11-17.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import Foundation
import MediaPlayer
import SwiftyJSON
import Alamofire
import SABlurImageView
import BLKFlexibleHeightBar
import StreamingKit
import MGSwipeTableCell
import JLToast
import Bond
//import CustonUploadAudioPlayerVC


//Create the delegate protocol to add a mediaItem to a lobeList
protocol LobeListDelegate {
    func addMusicItemToLobeList(musicItem: SongItem)
    func addMusicTrackToLobeList(musicItem: SongItem)
    func removeMusicItemFromLobeList(musicItem: SongItem)
    func lobeListContains(musicItem: SongItem) -> Bool
    func removeMusicItemFromBoth(musicItem: MPMediaItem)
}

class MusicVC: UIViewController {
    
    @IBOutlet var headerView: SABlurImageView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var songArtWorkImageView:RounderCornerImageView!
    @IBOutlet var blkMenuBar:BLKFlexibleHeightBar!
    var mediaItems = [MPMediaItem]()
    var filteredMediaItems = [MPMediaItem]()
    
    var filteredSongItems = [SongItem]()
    var collection: MPMediaItemCollection?
    
    var indexOfCurrentPlayingMusic: NSIndexPath = NSIndexPath(forItem: 0, inSection: 0)
    var nowPlayingSongRow = 0
    
    var lobeListDelegate: LobeListDelegate?
    var delegateSplitter: BLKDelegateSplitter?
    
    var inSearchMode = false
    var bar:BLKFlexibleHeightBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.tableView.separatorStyle = .None
        mediaItems = MPMediaQuery.songsQuery().items!
        collection = MPMediaItemCollection(items: mediaItems )
        AUDIO_OUT_MANAGER.observeLobeListChange(self)
        dataSourceObservable()
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
//            }
        for item in self.mediaItems {
            let songItem = SongItem(mMediaItem: item)
//                if !AUDIO_OUT_MANAGER.localListContains(songItem){
                AUDIO_OUT_MANAGER.localSongItemList.addObject(songItem)
                AUDIO_OUT_MANAGER.localObservableTrackList.append(songItem)
//                }
        }
        guard AUDIO_OUT_MANAGER.localObservableTrackList.count > 0 else {
            return
        }
        AUDIO_OUT_MANAGER.currentSong = AUDIO_OUT_MANAGER.localObservableTrackList[0]
        self.applyBlurEffect()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "trackToRemove:", name:"trackToRemoveNotification", object: nil)
        //MARK: - Placeholder Image
        let width = Int(CGRectGetWidth(self.view.bounds))
        let heigth = Int(CGRectGetHeight(self.view.bounds))
        let croppedImage = DownloadHelper.cropImage( UIImage(named: placeholderImageName)!, withWidth: width , andHeigth:heigth)
        self.headerView.backgroundColor = UIColor(patternImage: croppedImage)
        self.songArtWorkImageView.image = croppedImage
        
        //        showBLKMenuView()
        self.observeCurrentSongChange()
        
        
        guard !inSearchMode && AUDIO_OUT_MANAGER.initialLoading  else {
            return
        }
        
        print("Init the Library")
        AUDIO_OUT_MANAGER.player.setQueueWithItemCollection(collection!)
        AUDIO_OUT_MANAGER.currentSong = AUDIO_OUT_MANAGER.localSongItemList[0] as! SongItem
        AUDIO_OUT_MANAGER.player.nowPlayingItem = AUDIO_OUT_MANAGER.currentSong.music!
        AUDIO_OUT_MANAGER.initialLoading = false

    }
    
    
    func selectItem(atRow row:Int, andSection section:Int){
        
        let indexPath = NSIndexPath(forRow: row, inSection: section)
        self.indexOfCurrentPlayingMusic = indexPath
        self.nowPlayingSongRow = indexPath.row
//        self.tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: UITableViewScrollPosition.None)
//        self.tableView(self.tableView, didSelectRowAtIndexPath: indexPath)
        
    }
    
    func actOnLobeListChange(){
        tableView.reloadData()
    }
    func updateState(){
        print("Music Update")
    }
    func trackToRemove(notification: NSNotification){
        let trackID = notification.userInfo!["presistentID"] as! String
        
        for elem in mediaItems{
            if String(elem) == trackID{
                self.lobeListDelegate?.removeMusicItemFromBoth(elem)
                self.tableView.reloadData()
                print("\(mediaItems.indexOf(elem)!)")
            }
        }
    }
    

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        JLToast.makeText("QUICK TOAST", delay: 0, duration: JLToastDelay.LongDelay)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    func applyBlurEffect(){
        //        headerView.configrationForBlurAnimation()
        //        headerView.addBlurEffect(30, times: 1)
        
        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        let blurView = UIVisualEffectView(effect: darkBlur)
        blurView.frame = headerView.bounds
        headerView.addSubview(blurView)
        
    }
    
    func userStateChanged(){
    }
    
}

//==========================================================================================
// MARK: - Observers
//==========================================================================================
extension MusicVC {
    
    func observeCurrentSongChange(){
        
        //Observe current song
        AUDIO_OUT_MANAGER.currentTrackObservable.observe{ currentTrack in
            
            currentTrack.trackInfo.songAlbumArt.observe{ image in
                if let albumImage = image {
                
                    let croppedImage = DownloadHelper.cropImage(albumImage, withWidth: Int(CGRectGetWidth(self.view.bounds)) , andHeigth:Int(CGRectGetHeight(self.view.bounds)))
                    dispatch_async(dispatch_get_main_queue()) {
                        self.songArtWorkImageView.image = croppedImage
                    }
                }
            }
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }
        }
        
        //Observe LobeList
        AUDIO_OUT_MANAGER.lobeListObservableList.observe{ newList in
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }
        }
    }
}


//==========================================================================================
// MARK: - UITableViewDataSource Observable
//==========================================================================================
extension MusicVC {
    

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MusicCell", forIndexPath: indexPath) as! MusicCell
        
        let songItem:SongItem!
        
        if inSearchMode{
            songItem = filteredSongItems[indexPath.row]
        }else{
            songItem = AUDIO_OUT_MANAGER.localSongItemList[indexPath.row] as! SongItem
        }
        
        cell.songItem = songItem
        //Hide reoder button
        switch LobeStateHelper.sharedInstance.state {
        case .Local,.DJ,.Participant:
            self.tableView.setEditing(false, animated: false)
        }
        
        let lockQueue = dispatch_queue_create("lobeList.LockQueue", nil)
        dispatch_sync(lockQueue) {
            // code
            cell.onPushTrackToLobeList = { [unowned self] (MusicCell) -> Void in
                self.handlePushTrack(songItem)
            }
        }

        
        return cell
    }
    
    func dataSourceObservable(){
        
        AUDIO_OUT_MANAGER.localObservableTrackList.observe{ newList in
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }
        }
    }
        
 
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if inSearchMode{
            return filteredSongItems.count ?? 0
        }else{
            return AUDIO_OUT_MANAGER.localSongItemList.count ?? 0
        }
        
    }
    
    func currentTimeMillis() -> Int64{
        let nowDouble = NSDate().timeIntervalSince1970
        return Int64(nowDouble*1000)
    }
    
    func handlePushTrack(songItem:SongItem){
        
        //Set the trackInfo with the right infos
        songItem.trackInfo.mUser = LobeStateHelper.currentUser
        songItem.trackInfo.mIsYoutubeVideo = false
        songItem.trackInfo.mUserOwnerName = LobeStateHelper.currentUser.userName.value!
        songItem.trackInfo.mUserOwnerUid = LobeStateHelper.currentUser.parseID
        songItem.trackInfo.mUniqueDeviceId = unique_device_id
        songItem.trackInfo.mUnixTimeStampAtUserPush = "\(currentTimeMillis())"
        
        //TODO: - Get the format desc in trackInfo
        let url = songItem.music!.valueForProperty(MPMediaItemPropertyAssetURL) as! NSURL
        let songAsset = AVURLAsset(URL: url)
        let trackAsset = songAsset.tracks[0] as AVAssetTrack
        let descriptions = trackAsset.formatDescriptions
        let audioFormstDesc = descriptions[0] as! CMAudioFormatDescription

//        let descJSON = JSON(audioFormstDesc)
        let descString = "streamBasicDescription: \(audioFormstDesc)"
        let descStringArray = descString.componentsSeparatedByString("]> ")
        let descJSON = descStringArray[1]
//        print("descJSON: \(descJSON)")
        
        let mFormatIDArrayLong = descJSON.componentsSeparatedByString("mFormatID: ")
        let mFormatIDArray = mFormatIDArrayLong[1].componentsSeparatedByString("mFormatFlags")
        let mFormatID = mFormatIDArray[0]
//        print("mFormatID: \(mFormatID)")
        
        let mSampleRateArrayLong = descJSON.componentsSeparatedByString("mSampleRate: ")
        let mSampleRateArray = mSampleRateArrayLong[1].componentsSeparatedByString("mFormatID")
        let mSampleRate = mSampleRateArray[0]
        let integerSampleRateString = mSampleRate.componentsSeparatedByString(".")[0]
//        print("mSampleRate: \(mSampleRate)")
        print("integerSampleRateString: \(Int(integerSampleRateString)!)")
        
        let mChannelsPerFrameArrayLong = descJSON.componentsSeparatedByString("mChannelsPerFrame: ")
        let mChannelsPerFrameArray = mChannelsPerFrameArrayLong[1].componentsSeparatedByString("mBitsPerChannel")
        let mChannelsPerFrame = mChannelsPerFrameArray[0]
        let integerMChannelsPerFrame = mChannelsPerFrame.componentsSeparatedByString(" \n")[0]
//        print("mChannelsPerFrame: \(mChannelsPerFrame)")
        print("integerMChannelsPerFrame: \(Int(integerMChannelsPerFrame)!)")
        
        songItem.trackInfo.mAudioNbChannels = Int(integerMChannelsPerFrame)!
        songItem.trackInfo.mAudioSampleRate = Int(integerSampleRateString)!
        songItem.trackInfo.mAudioBitRate = 16
        
//        let message = "streamBasicDescription: \(streamBasicDescription)"
//        JLToast.makeText(message).show()
        
        switch LobeStateHelper.sharedInstance.state {
            
        case .Local:
            if isValidFileType(songItem.music!){
            if (!AUDIO_OUT_MANAGER.lobeListContains(songItem)){
                 AUDIO_OUT_MANAGER.lobeListSongItems.addObject(songItem)
                AUDIO_OUT_MANAGER.lobeListObservableList.replaceRange(0...0, with: [SongItem()])
                }
            }
           
        case .Participant,.DJ:
            
            if isValidFileType(songItem.music!){
            
                if (!AUDIO_OUT_MANAGER.lobeListContains(songItem)){
                    NativeToWebHelper.sharedInstance.pushSingleSuggestionItem(songItem.trackInfo)
                }
            }
        }
        
    }

}


func isValidFileType(mediaItem:MPMediaItem) -> Bool{
    
    if let localPath = mediaItem.valueForProperty(MPMediaItemPropertyAssetURL) as? NSURL{
        
        let fileType:String = localPath.pathExtension!
        print("file type: \(fileType)")
        if fileType == "mp3" {
            return true
        }
        let message = "This file type: \(fileType) is not supported at the moment"
        JLToast.makeText(message).show()
    }

    return false

}

//==========================================================================================
// MARK: - UITableViewDelegate
//==========================================================================================
extension MusicVC: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if AUDIO_OUT_MANAGER.musicPlayerState != .Library{
            AUDIO_OUT_MANAGER.musicPlayerState = .Library
        }
//        let cell =  self.tableView.cellForRowAtIndexPath(indexPath) as! MusicCell
        let songItem =  AUDIO_OUT_MANAGER.localSongItemList[indexPath.row] as! SongItem
        
        switch LobeStateHelper.sharedInstance.state {
            
            case .Local,.DJ:
                AUDIO_OUT_MANAGER.currentSong = songItem
                AUDIO_OUT_MANAGER.currentLocalSongIndex = indexPath.row
            case .Participant:
                let message = "Can't play the song from library in Social Mode. You need to use lobelist"
                print("\(message)")
                JLToast.makeText(message).show()
        }
    
        AUDIO_OUT_MANAGER.directPlayMusic()
        tableView.reloadData()
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
}

//==========================================================================================
//MARK: - Search Mode
//==========================================================================================
extension MusicVC:SearchVCDelegate{
    
    func searchSongWithText(text:String){
        print("SEARCHING!!")
        inSearchMode = true
        let searchPredicate = NSPredicate(format: "title CONTAINS[c] %@ OR content CONTAINS[c] %@", text, text)
        
        let filteredSongs = AUDIO_OUT_MANAGER.localSongItemList.filter {
            let songName = $0.trackInfo.mName
//            let artistName = $0.trackInfo.mArtist
            let options = NSStringCompareOptions.CaseInsensitiveSearch
            return songName.rangeOfString(text, options: options) != nil
        }
        self.filteredSongItems = filteredSongs as! [SongItem]
        self.tableView.reloadData()
    }
    func returnToDefault(){
        inSearchMode = false
        self.tableView.reloadData()
    }
    func searchActivated(isActivated: Bool) {
        inSearchMode = isActivated
        self.tableView.reloadData()
    }
    
    //MARK: - Library Menu: BLKFlexibleHeightBar
    func showBLKMenuView(){
        
        bar = BLKFlexibleHeightBar(frame: CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 60))
        
        bar.minimumBarHeight = 0.0
        bar.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
        bar.alpha = 1
        
        //            bar.behaviorDefiner.elasticMaximumHeightAtTop = true
        
        //        let button = UIButton(frame: CGRectMake(10, 10, 80, 40))
        //        button.setTitle("Search", forState: UIControlState.Normal)
        //        button.titleLabel?.textColor = UIColor.whiteColor()
        //        button.layer.cornerRadius = 10
        //        button.layer.borderColor = UIColor.whiteColor().CGColor
        //        button.layer.borderWidth = 1
        //        button.addTarget(self, action: Selector("searchButtonTapped"), forControlEvents: .TouchUpInside)
        //        bar.addSubview(button)
        //        self.tableView.contentInset = UIEdgeInsetsMake(bar.maximumBarHeight, 0.0, 0.0, 0.0)
        
        let searchBar = UISearchBar(frame: CGRectMake(20, 10, 280, 40))
        searchBar.backgroundColor = UIColor.blackColor()
        searchBar.barTintColor = UIColor.blackColor()
        searchBar.delegate = self
        searchBar.tintColor = UIColor.orangeColor()
        searchBar.barStyle = .Black
        searchBar.searchBarStyle = .Prominent
        bar.addSubview(searchBar)
        
        let textFieldInsideSearchBar = searchBar.valueForKey("searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = UIColor.orangeColor()
        
        let textFieldInsideSearchBarLabel = textFieldInsideSearchBar!.valueForKey("placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.textColor = UIColor.lighterGrayColor()
        
        bar.behaviorDefiner = FacebookStyleBarBehaviorDefiner()
//        self.tableView.delegate = bar.behaviorDefiner as? UITableViewDelegate
        bar.behaviorDefiner.addSnappingPositionProgress(0.0, forProgressRangeStart:0.0, end:0.5)
        bar.behaviorDefiner.addSnappingPositionProgress(1.0, forProgressRangeStart:0.5, end:1.0)
        bar.behaviorDefiner.snappingEnabled = true
        
        // Configure a separate UITableViewDelegate and UIScrollViewDelegate (optional)
        self.delegateSplitter = BLKDelegateSplitter(firstDelegate: bar.behaviorDefiner, secondDelegate: self)
        self.tableView.delegate = self.delegateSplitter
        
        self.view.addSubview(bar)
        
    }
    
    func searchButtonTapped(){
        searchActivated(!inSearchMode)
    }
}

// MARK: - Searchbar Delegate
extension MusicVC: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
        searchActivated(true)
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = ""
        searchBar.setShowsCancelButton(false, animated: true)
        searchActivated(false)
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        searchSongWithText(searchText)
    }
    
}

public struct MyString {
    public static func contains(text: String, substring: String,
        ignoreCase: Bool = false,
        ignoreDiacritic: Bool = false) -> Bool {
            
            if substring == "" { return true }
            var options = NSStringCompareOptions.CaseInsensitiveSearch
            
            
            return text.rangeOfString(substring, options: options) != nil
    }
}
