
import Foundation
//import CordovaPlugin_console
import Cordova
import SwiftyJSON
import CoreLocation
import SVProgressHUD

 @objc(HybridBridge) class HybridBridge : CDVPlugin {
    
    static var sharedInstance = HybridBridge()
    
    var mGenericJvascriptCallbackId:String?
    
    //** From Native to Javascript **//
    func genericJavascriptCall(functionName:String, params:String){
//        print("Native Calling JS: \(functionName)")
        let jsFunction = "\(functionName)(\(params))"
        
        dispatch_async(dispatch_get_main_queue()) {
            self.webView?.stringByEvaluatingJavaScriptFromString(jsFunction)
        }
    }
    
    //** From Javascript to Native **//
    func getDeviceLocation(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.runInBackground({ () -> Void in
            let json = JSON(command.arguments)
            print("webcall getDeviceLocation: \(json)")
            
//            let latitude: CLLocationDegrees = (LocationHelper.sharedInstance.manager.location?.coordinate.latitude)!
//            let longitude: CLLocationDegrees = (LocationHelper.sharedInstance.manager.location?.coordinate.longitude)!

            let functionName:String = "fromJavaDeviceLocationCallback";
            self.genericJavascriptCall(functionName, params: "'\(LobeStateHelper.sharedInstance.user_location_latitude)','\(LobeStateHelper.sharedInstance.user_location_longitude)'")
            
            let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAsString: "OK")
            self.commandDelegate!.sendPluginResult(pluginResult, callbackId:command.callbackId)
        })
    }
    
    func testWebToNativeCall(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.runInBackground({ () -> Void in
//            let message = command.arguments[0] as! Bool
            let json = JSON(command.arguments)
            print("webcall params: \(json)")
//            WebToNativeHelper.sharedInstance.playNotifSound(json[0].string!)
            
            let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAsString: "Hello ")
            self.commandDelegate!.sendPluginResult(pluginResult, callbackId:command.callbackId)
        })
    }
    
    func sendSwiftMessage(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.runInBackground({ () -> Void in
            let message = command.arguments[0] as! String
            let json = JSON(command.arguments)
            print("webcall: \(json)")
            
            let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAsString: "Hello \(message)")
            self.commandDelegate!.sendPluginResult(pluginResult, callbackId:command.callbackId)
        })

    }
    
    func goToViewFromBackAction(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.runInBackground({ () -> Void in
            let json = JSON(command.arguments)
            print("webcall goToViewFromBackAction: \(json)")
            WebToNativeHelper.sharedInstance.goToViewFromBackAction(json[0].string!)
            WebToNativeHelper.sharedInstance.playNotifSound("")
        })
    }
    
    func testCall(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.runInBackground({ () -> Void in
            let json = JSON(command.arguments)
            print("webcall: \(json)")
            WebToNativeHelper.sharedInstance.testCall(json[0].string!)
        })
    }
    
    func showToastMessage(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.runInBackground({ () -> Void in
            let json = JSON(command.arguments)
            WebToNativeHelper.sharedInstance.showToastMessage(json[0].string!)
        })
    }
    
    func toJavaCallSetAudioSourceMode(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.runInBackground({ () -> Void in
            let json = JSON(command.arguments)
            self.mGenericJvascriptCallbackId = command.callbackId
            WebToNativeHelper.sharedInstance.toJavaCallSetAudioSourceMode(json[0].string!)
        })
    }
    
    func playNotifSound(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.runInBackground({ () -> Void in
            let json = JSON(command.arguments)
            WebToNativeHelper.sharedInstance.playNotifSound(json[0].string!)
        })
    }
    
    func setIsSignedInState(command: CDVInvokedUrlCommand) {
        let json = JSON(command.arguments)
        print("webcall: \(json)")
        WebToNativeHelper.sharedInstance.setIsSignedInState(json[0].bool!)
    }
    
    func setMyPersonalInfo(command: CDVInvokedUrlCommand) {
        let json = JSON(command.arguments)
        print("webcall setMyPersonalInfo: \(json)")
        WebToNativeHelper.sharedInstance.setMyPersonalInfo(json[0].string!, name: json[1].string!, picUrl: json[2].string!)
    }
    
    func setLobeList(command: CDVInvokedUrlCommand) {
        let json = JSON(command.arguments)
//        print("webcall: \(json)")
        WebToNativeHelper.sharedInstance.setLobeList(json)
    }
    
    func setSuggestionList(command: CDVInvokedUrlCommand) {
        let json = JSON(command.arguments)
//        print("webcall: \(json)")
        WebToNativeHelper.sharedInstance.setSuggestionList(json)
    }
    
    func backToNormal(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.runInBackground({ () -> Void in
            WebToNativeHelper.sharedInstance.backToNormal()
        })
    }
    
    func doSynchoFutureEventTest(command: CDVInvokedUrlCommand) {
        
    }
    
    func resumeMusic(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.runInBackground({ () -> Void in
            WebToNativeHelper.sharedInstance.resumeMusic()
        })
    }
    
    func pauseMusic(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.runInBackground({ () -> Void in
            WebToNativeHelper.sharedInstance.pauseMusic()
        })
    }
    
    func setTimeDiffWithFirebase(command: CDVInvokedUrlCommand) {

        let json = JSON(command.arguments)
        let jsCurrUnixTime:Int64 = json[0].int64Value
        let deltaF_JS:Int64 = json[1].int64Value
        WebToNativeHelper.sharedInstance.setTimeDiffWithFirebase(jsCurrUnixTime, deltaF_JS: deltaF_JS)
//        self.commandDelegate?.runInBackground({ () -> Void in
//            
//        })
    }
    
    func startStreamingFromUrl(command: CDVInvokedUrlCommand) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {

            let json = JSON(command.arguments)
//            print("webcall startStreamingFromUrl: \(json)")
            let time_stamp_String:Int64 = json[1].int64Value
            let music_start_time_delay_String:Int64 = json[2].int64Value
            print("time_stamp_String: \(time_stamp_String)")
            print("music_start_time_delay_String: \(music_start_time_delay_String)")
            
            WebToNativeHelper.sharedInstance.startStreamingFromUrl(json[0], server_time_stamp: time_stamp_String, music_start_time_delay: music_start_time_delay_String)
        }
    
    }
    
    func toggleDrawerMenuClick(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.runInBackground({ () -> Void in
            WebToNativeHelper.sharedInstance.toggleDrawerMenuClick()
        })
        
    }
    
    func groupUploadRequestReceived(command: CDVInvokedUrlCommand) {
        let json = JSON(command.arguments)
        WebToNativeHelper.sharedInstance.groupUploadRequestReceived(json[0])
    }
    
    func addTrackUniqueToLobeList(command: CDVInvokedUrlCommand) {
        let json = JSON(command.arguments)
        WebToNativeHelper.sharedInstance.addTrackUniqueToLobeList(json[0])
    }
    
    func createAllLocalFileChunks(command: CDVInvokedUrlCommand) {
        let json = JSON(command.arguments)
        WebToNativeHelper.sharedInstance.createAllLocalFileChunks(json[0], nbSecondsPerChunk: json[1].int!)
    }
    
    func uploadFileToServer(command: CDVInvokedUrlCommand) {
        let json = JSON(command.arguments)
        let mCorrelationID:String = json[2].string!
        let mChunkStartTimeServer:Int64 = json[3].int64Value
        let mChunkIndex:Int64 = json[0].int64Value
        
        print("mCorrelationID: \(mCorrelationID)")
        print("mChunkStartTimeServerUInt64: \(mChunkStartTimeServer)")
        
        WebToNativeHelper.sharedInstance.uploadFileToServer(UInt32(mChunkIndex), localFilePath: json[1].string!, correlationID: mCorrelationID, chunkStartTimeServer: mChunkStartTimeServer)
    }
    
    //===============================
    // MARK:- Social playback
    //===============================
    
    func updateCurrTrackProgressRatio(command: CDVInvokedUrlCommand) {
        
        dispatch_async(dispatch_get_main_queue()) {
            //TODO: - Update the slider circle
            let json = JSON(command.arguments)
            let ratioFloat = json[0].double
            social_slider_value.value = ratioFloat!
            
//            AUDIO_OUT_MANAGER.notifyMusicChange(false)
            
        }
    }
    
    //===============================
    // MARK:- Social Voting System
    //===============================
    func setLobeListDynamicMap(command: CDVInvokedUrlCommand){
        
        let json = JSON(command.arguments)
        print("webcall setLobeListDynamicMap: \(json)")
        AUDIO_OUT_MANAGER.socialVotingMapList = json
    }
    
    func showLoadingSpinner(command: CDVInvokedUrlCommand){
        dispatch_async(dispatch_get_main_queue()) {
            SVProgressHUD.show()
        }
    }
    
    func hideLoadingSpinner(command: CDVInvokedUrlCommand){
        dispatch_async(dispatch_get_main_queue()) {
            SVProgressHUD.dismiss()
        }
    }
}