//
//  UserPlaylistsVC.swift
//  Lobe
//
//  Created by Professional on 2015-11-17.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import MediaPlayer
import RealmSwift

class UserPlaylistsVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var businessArray = MPMediaQuery.playlistsQuery().collections
    var playlistAdded: Int = 0
    var plist: NSMutableArray = []
    //    var businessArrayPlist: NSMutableArray = []
    var numbOfPlistSongs: NSMutableArray = []
    var realm: Realm!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        realm = try! Realm()
        let savedPlaylists = realm.objects(UserPlaylist)
        playlistAdded = savedPlaylists.count
        for pls in savedPlaylists{
            plist.addObject(pls.playlistName_)
            numbOfPlistSongs.addObject(pls.tracks_.count)
        }
        
        for pls in businessArray!{
            let plss = pls as! MPMediaPlaylist
            let plsName = plss.valueForProperty(MPMediaPlaylistPropertyName) as! String
            //            businessArrayPlist.addObject(plsName)
            plist.addObject(plsName)
            numbOfPlistSongs.addObject(plss.items.count)
        }
    }
    
    @IBAction func closeView(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}

extension UserPlaylistsVC: UITableViewDataSource {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("UserPlaylistsCell", forIndexPath: indexPath) as! UserPlaylistsCell
        
        
        cell.playlistNameLabel?.text = String(plist.objectAtIndex(indexPath.row))
        cell.songsNumberLabel.text = String(numbOfPlistSongs.objectAtIndex(indexPath.row))
        cell.deletePlistButton.tag = indexPath.row
        cell.deletePlistButton.addTarget(self, action: "deletePlist:", forControlEvents: .TouchUpInside)
        
        //                if !businessArrayPlist.containsObject(String(plist.objectAtIndex(indexPath.row))){
        //                    
        //                }else{
        //                    cell.deletePlistButton.setBackgroundImage(UIImage(named: "eye_open_dark.png"), forState: UIControlState.Disabled)
        //                    //            cell.deletePlistButton.addTarget(self, action: "deletePlist:", forControlEvents: .TouchUpInside)
        //                }
        
        return cell
    }
    @IBAction func deletePlist(sender: UIButton){
        
        let savedPlaylists = realm.objects(UserPlaylist).filter("playlistName_='\(plist[sender.tag] as! String)'")
        
        if savedPlaylists.count > 0 {
            let alert = UIAlertController(title: "Alert", message: "This Playlist will be lost!!", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction!) in
                for elem in savedPlaylists{
                    try! self.realm.write {
                        self.realm.delete(elem)
                    }
                }
                print("Playlist Deleted")
                self.dismissViewControllerAnimated(false, completion: nil)
                NSNotificationCenter.defaultCenter().postNotificationName("deletePlist", object: nil)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            
            
        }else{
            let alert = UIAlertController(title: "Alert", message: "This Playlist must be deleted from iTunes", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numbOfPlistSongs.count ?? 0 //businessArray!.count ?? 0
    }
    
}

extension UserPlaylistsVC: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var pageDict: Dictionary<String,String>! = ["playlistID": "", "playlistName": "", ]
        
        let savedPlaylists = self.realm.objects(UserPlaylist).filter("playlistName_='\(plist[indexPath.row])'")
        
        if savedPlaylists.count > 0 {
            for elem in savedPlaylists {
                pageDict["playlistID"] = "\(elem.idPlaylist_)"
            }
        }else{
            //            MPMediaItemPropertyPersistentID
            pageDict["playlistID"] = "\(businessArray![Int(indexPath.row - playlistAdded)].valueForProperty(MPMediaItemPropertyPersistentID)!)"
            pageDict["playlistName"] = String(plist.objectAtIndex(indexPath.row))
        }
        
        NSNotificationCenter.defaultCenter().postNotificationName("UserHasSelectPlaylistNotification", object: nil, userInfo: pageDict)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
    
    
}