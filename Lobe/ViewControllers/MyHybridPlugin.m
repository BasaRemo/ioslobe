//
//  MyHybridPlugin.m
//  HybridIOSApp
//
//  Created by Holly Schinsky on 6/25/15.
//
//
#import "MyHybridPlugin.h"

@implementation MyHybridPlugin
-(void)addBookmark:(CDVInvokedUrlCommand*) command {
    NSString* bookmark = [command.arguments objectAtIndex:0];
    
    if(bookmark) {
        NSLog(@"Native addBookmark %@", bookmark);
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    } else {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

-(void)sendMessage:(CDVInvokedUrlCommand *)command{
    NSString* message = [command.arguments objectAtIndex:0];
    NSLog(@"Received Args: %@", command.arguments);
    if(message) {
        NSLog(@"Native Received Message %@", message);
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    } else {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

@end
