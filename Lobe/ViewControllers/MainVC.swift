//
//  MainVC.swift
//  Lobe
//
//  Created by Professional on 2015-11-17.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import VYPlayIndicator
import ZFDragableModalTransition
import AVFoundation
import Foundation
import MediaPlayer
import SwiftyJSON
import Alamofire
import SABlurImageView
import ARNTransitionAnimator
import BLKFlexibleHeightBar
import Sheriff
import CarbonKit
import LGSideMenuController
import BubbleTransition
import CoreLocation
import Parse
import QuartzCore
import DOFavoriteButton

public enum MenuItemMode {
    case None
    case Underline(height: CGFloat, color: UIColor, horizontalPadding: CGFloat, verticalPadding: CGFloat)
    case RoundRect(radius: CGFloat, horizontalPadding: CGFloat, verticalPadding: CGFloat, selectedColor: UIColor)
}


let presentPopUpTransition = PresentPopUpTransition()
let dismissPopUpTransition = DismissPopUpTransition()
let transition = BubbleTransition()
var search_animator:ZFModalTransitionAnimator!
//Create the delegate protocol to add a mediaItem to a lobeList
protocol MainViewDelegate {
}

var sideMenuControllerInstance:SideMenuController?
class MainVC: LGSideMenuController,SuggestionDelegate {
    
    // MARK: Attributes
    
    @IBOutlet var magicButton: UIButton!
    
    @IBOutlet var playButton:UIButton!
    @IBOutlet var playIndicatorView: UIView!
    @IBOutlet var playerViewBackground: SABlurImageView!
    @IBOutlet var playerView:UIView!
    @IBOutlet var socialButton:UIButton!
    @IBOutlet var topView:UIView!
    @IBOutlet var currentSongLabel:UILabel!
    @IBOutlet var currentArtistLabel:UILabel!
    
    @IBOutlet var containerView: UIView!
    @IBOutlet var webViewContainerView:UIView!
    
    @IBOutlet var likeButton:DOFavoriteButton!
    @IBOutlet var nextButton:UIButton!
    
    @IBOutlet var searchBar:UISearchBar!
    
    var artWorkImageView:UIImageView?
    var timer:NSTimer!
    var viewControllers: [UIViewController]=[]
    var pagingMenuController: PagingMenuController?
    var playlistName_ :String = "New Lobe List"
    
    var playlistVC: PlaylistVC?
    var musicVC: MusicVC?
    var webViewVC:LobeWebVC?
    var menuVC:MenuVC?
    var menuContainerVC:MenuContainerVC?
    var searchVC:SearchVC?
    
    
    var nowPlayingIndexInLobeList: Int = 0
    var nowPlayingIndexInMusicList: Int = 0
    var isPlayingExistInLobeList: Bool = false
    
    var delegateSplitter: BLKDelegateSplitter?
    var delegate:MainViewDelegate?
    
    var animator : ARNTransitionAnimator!
    var animator2:ZFModalTransitionAnimator!
    
    var animatorModal:ZFModalTransitionAnimator!
    var menuAnimator:ZFModalTransitionAnimator!
    var modalVC : InitVC!

    
    @IBOutlet var lobeListCountLabel: UILabel!
    
    var carbonTabSwipeNavigation:CarbonTabSwipeNavigation!
    
    // MARK: Start of function declaration
    override func viewDidLoad() {
        super.viewDidLoad()
        
        LobeStateHelper.sharedInstance.addLobeStateObserver(self)
        LobeStateHelper.sharedInstance.addUserStateObserver(self)
        LobeStateHelper.sharedInstance.addMagicButtonStateObserver(self)
        LobeStateHelper.sharedInstance.addPushNotificatonObserver(self)
        AUDIO_OUT_MANAGER.observeMusicChange(self)
        AUDIO_OUT_MANAGER.observeLobeListChange(self)
        AUDIO_OUT_MANAGER.addMainViewInstance(self)
        LobeStateHelper.sharedInstance.addLobeStateObserver(AUDIO_OUT_MANAGER)

        likeButton.addTarget(self, action: Selector("likeButtonSelected:"), forControlEvents: .TouchUpInside)
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: "goToSearchView")
        self.searchBar.addGestureRecognizer(gestureRecognizer)
        self.searchBar.delegate = self
        
        
        setupUserInterface()
        
//        self.magicButton.hidden = true
        observeCurrentSongChange()
        
        AUDIO_OUT_MANAGER.createUploadDirectory()
//        AUDIO_OUT_MANAGER.deleteUploadDirectory()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    //Show my lobelive view
    @IBAction func socialBtnPressed(sender: AnyObject) {
        NativeToWebHelper.sharedInstance.showView(MagicButtonViewState.lobeLiveView.rawValue, backToViewString: "")
        self.webViewContainerView.hidden = false
    }
}

//==========================================================================================
// MARK: - Observers
//==========================================================================================
extension MainVC {
    
    func observeCurrentSongChange(){
        //Observe current song
        AUDIO_OUT_MANAGER.currentTrackObservable.observe{ currentTrack in
            
            dispatch_async(dispatch_get_main_queue()) {
                self.currentSongLabel.text = currentTrack.trackInfo.mName
                self.currentArtistLabel.text = currentTrack.trackInfo.mArtist
            }
            
            currentTrack.trackInfo.songAlbumArt.observe{ image in
                if let albumImage = image {
                    
                    let croppedImage = DownloadHelper.cropImage(albumImage, withWidth: Int(CGRectGetWidth(self.view.bounds)) , andHeigth:Int(CGRectGetHeight(self.view.bounds)))
                    
                    dispatch_async(dispatch_get_main_queue()) {
                        self.playerViewBackground.image = croppedImage
                        self.topView.backgroundColor = UIColor(patternImage: croppedImage)
                    }
                }
            }
        }
        
        //Observe lobelist
        AUDIO_OUT_MANAGER.lobeListObservableList.observe{ newList in
            dispatch_async(dispatch_get_main_queue()) {
                self.lobeListCountLabel.text = "\(AUDIO_OUT_MANAGER.lobeListSongItems.count)"
            }
        }
        
        //Observe audio playing state
        AUDIO_OUT_MANAGER.isAudioPlaying.observe{ currentTrack in
            dispatch_async(dispatch_get_main_queue()) {
                self.playButton.selected = AUDIO_OUT_MANAGER.isMusicPlaying()
            }
        }
        
    }
}

//==========================================================================================
// MARK: - UI Change Management
//==========================================================================================

extension MainVC {
    
    func setupUserInterface(){
        
        self.webViewContainerView.hidden = false
        setupMusicInitVC()
        blurImage()
//        setupCarbonKitTab()
//        setupPagingMenuController()
        
        // Change navbar appearance
        UINavigationBar.appearance().translucent = true
        UINavigationBar.appearance().barTintColor = UIColor.clearColor()
        UINavigationBar.appearance().tintColor = UIColor.clearColor()
        UINavigationBar.appearance().backgroundColor = UIColor.clearColor()
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        self.navigationItem.title = ""
        
        self.containerView.backgroundColor = UIColor.clearColor()
        self.view.backgroundColor = UIColor.clearColor()
        definesPresentationContext = true
        
        let px = 2 / UIScreen.mainScreen().scale
        let frame = CGRectMake(0, 0, self.playerView.frame.size.width, px)
        let line: UIView = UIView(frame: frame)
        self.playerView.addSubview(line)
        line.backgroundColor = UIColor.blackColor()

        lobeListCountLabel.layer.cornerRadius = CGRectGetWidth(lobeListCountLabel.bounds)/2
        lobeListCountLabel.layer.masksToBounds = true
        lobeListCountLabel.clipsToBounds = true
        lobeListCountLabel.text = "\(AUDIO_OUT_MANAGER.lobeListSongItems.count)"
        
        //MARK: - Placeholder Image
        let width = Int(CGRectGetWidth(self.view.bounds))
        let heigth = Int(CGRectGetHeight(self.view.bounds))
        let croppedImage = DownloadHelper.cropImage( UIImage(named: placeholderImageName)!, withWidth: width , andHeigth:heigth)
        self.playerViewBackground.image = croppedImage
        
        setupSearchVC()
        
    }
    
    func blurImage() {
        
        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        
        //Player view blur
        let blurView = UIVisualEffectView(effect: darkBlur)
        blurView.frame = playerViewBackground.bounds
        playerViewBackground.addSubview(blurView)
        playerViewBackground.sendSubviewToBack(blurView)
        
        //Top view blur
        let blurView2 = UIVisualEffectView(effect: darkBlur)
        blurView2.frame = topView.bounds
        topView.addSubview(blurView2)
        topView.sendSubviewToBack(blurView2)
        topView.backgroundColor = UIColor(patternImage: UIImage(named: placeholderImageName)!)
        
        //Top view bottom black line
        let px = 2 / UIScreen.mainScreen().scale
        let frame = CGRectMake(0, CGRectGetHeight(self.topView.frame), self.topView.frame.size.width, px)
        let line: UIView = UIView(frame: frame)
        self.topView.addSubview(line)
        line.backgroundColor = UIColor.blackColor()
        
    }
    
    func setupMusicInitVC(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        self.modalVC = storyboard.instantiateViewControllerWithIdentifier("InitVC") as? InitVC
        self.modalVC.delegate = self
        
        //Drag to Present
        self.modalVC.modalPresentationStyle = .FullScreen
        self.modalVC.tapCloseButtonActionHandler = { [weak self] in
            self!.animator.interactiveType = .None
        }
        self.setupAnimator()
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: "expandPlayerView")
        self.playerView.addGestureRecognizer(gestureRecognizer)
    }
    
    func likeButtonSelected(sender: DOFavoriteButton) {
        let trackInfo = AUDIO_OUT_MANAGER.currentSong.trackInfo
        if sender.selected {
            // deselect
            NativeToWebHelper.sharedInstance.dislikeTrackInLobeList(-1, trackUid: (trackInfo.mID))
            sender.deselect()
        } else {
            // select with animation
            
            NativeToWebHelper.sharedInstance.promoteTrackInLobeList(-1, trackUid: (trackInfo.mID))
            sender.select()
        }
    }
    
}

//==========================================================================================
// MARK: lobeWebVC & musicVC SEGUE
//==========================================================================================
extension MainVC {
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let vc = segue.destinationViewController as? LobeWebVC
            where segue.identifier == "webViewSegue" {
                self.webViewVC = vc
        }
        
        if let viewController = segue.destinationViewController as? MusicVC
            where segue.identifier == "musicVCSegue" {
                self.musicVC = viewController
        }
    }
}

//==========================================================================================
// MARK: Events Notification Management
//==========================================================================================
extension MainVC {
    
    func lobeStateChanged(){
        switch LobeStateHelper.sharedInstance.state {
            case .Local:
                likeButton.hidden = true
                nextButton.hidden = false
            case .DJ:
                likeButton.hidden = true
                nextButton.hidden = false
            case .Participant:
                likeButton.hidden = false
                nextButton.hidden = true
        }
    }
    
    func userStateChanged(){
//        if let image = LobeStateHelper.currentUser.userImage {
//            socialButton.setImage(image, forState: UIControlState.Normal)
//        }
    }
    
    func actOnMusicChange(notification:NSNotification) {
        dispatch_async(dispatch_get_main_queue()) {
            let userInfo:Dictionary<String,Bool!> = notification.userInfo as! Dictionary<String,Bool!>
            let songChanged:Bool = userInfo["songChanged"]!
            if songChanged {
                self.updateViewOnMusicChange(songChanged)
            }
        }
    }
    func actOnLobeListChange(){
        self.updateViewOnMusicChange(false)
    }
    
    func updateViewOnMusicChange(songChanged:Bool){
        
    }
}

//==========================================================================================
// MARK: - PagingMenuControllerDelegate
//==========================================================================================
extension MainVC:PagingMenuControllerDelegate{
    
    func setupPagingMenuController(){
        
        viewControllers = [musicVC!,playlistVC!]
        
        let options = PagingMenuOptions()
        options.menuHeight = 44
        options.font = UIFont.systemFontOfSize(12)
        options.selectedFont = UIFont.systemFontOfSize(12)
        options.menuDisplayMode = .SegmentedControl
        options.backgroundColor = SEGMENT_MENU_COLOR
        options.selectedBackgroundColor = SEGMENT_MENU_COLOR
        options.selectedTextColor = UIColor.hexColor("D2D2D2")
        options.animationDuration = 0.01
        //        options.deceleratingRate = 0
        
        pagingMenuController = self.childViewControllers.first as? PagingMenuController
        pagingMenuController!.delegate = self
        pagingMenuController!.setup(viewControllers: viewControllers, options: options)
        
        if let imageView = pagingMenuController!.view.viewWithTag(15) as? RounderCornerImageView {
            imageView.layer.cornerRadius = imageView.frame.width/2
        }
        pagingMenuController?.view.backgroundColor = UIColor.clearColor()
        
    }
    
    
    func willMoveToMenuPage(page: Int) {
        print("will move to \(page)")
        //        playlistVC!.tableView.reloadData()
        //        suggestionsVC!.tableView.reloadData()
    }
    
    func didMoveToMenuPage(page: Int) {
        print("did move to \(page)")
        playlistVC!.tableView.reloadData()
    }
    
}

//==========================================================================================
// MARK: - CarbonTabSwipeNavigation Delegate
//==========================================================================================
extension MainVC:CarbonTabSwipeNavigationDelegate{
    
    func setupCarbonKitTab(){
        
        let tabMenuItems = ["Library","Lobelist"]
        
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: tabMenuItems, delegate: self)
        carbonTabSwipeNavigation.insertIntoRootViewController(self.childViewControllers.first!)
        
        carbonTabSwipeNavigation.toolbar.translucent = true
        carbonTabSwipeNavigation.toolbar.backgroundColor = UIColor.clearColor()
        carbonTabSwipeNavigation.setIndicatorColor(UIColor.orangeColor())
        carbonTabSwipeNavigation.toolbarHeight.constant = 0
        
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        let width = screenSize.width/2
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(width, forSegmentAtIndex: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(width+2, forSegmentAtIndex: 1)
        
        // Custimize segmented control
        carbonTabSwipeNavigation.setNormalColor(UIColor.lightGrayColor(), font: UIFont.systemFontOfSize(12))
        carbonTabSwipeNavigation.setSelectedColor(UIColor.hexColor("D2D2D2"), font: UIFont.systemFontOfSize(12))
        carbonTabSwipeNavigation.view.backgroundColor = UIColor.clearColor()
//        carbonTabSwipeNavigation.carbonSegmentedControl?.backgroundColor = UIColor.ligthBlackColor()
        
        //Loads all the vc
        for (var i:UInt = 0; i < 1; i++){
            carbonTabSwipeNavigation.currentTabIndex = i
            carbonTabSwipeNavigation.currentTabIndex = 0
        }
        
    }
    
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAtIndex index: UInt) -> UIViewController {
        switch index {
        case 0:
            return musicVC!
        case 1:
            return playlistVC!
        default:return musicVC!
        }
    }
    
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAtIndex index: UInt) {
        
    }
    
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAtIndex index: UInt) {
        
    }
    
    func barPositionForCarbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation) -> UIBarPosition {
        return UIBarPosition.Top
    }
}

//==========================================================================================
// TODO: - OLD playlist Change Management functions need to move them to InitVC
//==========================================================================================

extension MainVC{
    
    func mainUpdateAfterDelete(){
        self.showUserPlaylist()
    }
    func showUserPlaylist(){
        if let UserPlaylistsVC = storyboard!.instantiateViewControllerWithIdentifier("UserPlaylistsVC") as? UserPlaylistsVC {
            presentViewController(UserPlaylistsVC, animated: true, completion: nil)
        }
    }
}


//==========================================================================================
// MARK: - Custom modal Transitions animation
//==========================================================================================

//extension MainVC:UIViewControllerTransitioningDelegate {
//    
//    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        return presentPopUpTransition
//    }
//    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        return dismissPopUpTransition
//    }
//}

extension MainVC:UIPopoverPresentationControllerDelegate{
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.None
    }
}

// MARK: - Option List Delegate
extension MainVC:OptionListDelegate{
    func selectedCell(index: String) {
        //self.selectedIndex = index
        //        print("\(index) =========== \(playlistVC?.mediaItems.count)")
        switch index{
        case "Open existing playlist...":
            self.presentedViewController?.dismissViewControllerAnimated(false, completion: nil)
            self.showUserPlaylist()
            break
        case "Save as ...":
            //            PlaylistVC().alertToSave()
            self.presentedViewController?.dismissViewControllerAnimated(false, completion: nil)
            NSNotificationCenter.defaultCenter().postNotificationName("SaveNotification", object: nil)
            
            break
        case "Save":
            self.presentedViewController?.dismissViewControllerAnimated(false, completion: nil)
            NSNotificationCenter.defaultCenter().postNotificationName("UpdateNotification", object: nil)
            
            break
        case "Clean / new LobeList":
            playlistVC?.mediaItems.removeAll()
            playlistVC?.sectionTitle = "New Lobadbe List"
            playlistVC?.tableView.reloadData()
            musicVC?.tableView.reloadData()
            break
        default:
            break
        }
        
        
    }
}

//==========================================================================================
// MARK: InitVCDelegate
//==========================================================================================
extension MainVC:InitVCDelegate {
    func openWebView() {
        NativeToWebHelper.sharedInstance.showView(MagicButtonViewState.myProfileView.rawValue, backToViewString: "")
        self.webViewContainerView.hidden = false
        //        menuViewProfile()
    }
    
    func nextButtonTapped(){
        self.handleNextButtonTapped()
    }
    func previousButtonTapped(){
        self.handlePreviousButtonTapped()
    }
    func playPauseButtonTapped(){
        self.handlePlayOrPauseMusic()
    }
    func setupTargetView(){
        self.animator.gestureTargetView = self.modalVC!.targetImageView
    }
    
    func setupSearchVC(){
        
        searchVC  = self.storyboard?.instantiateViewControllerWithIdentifier("SearchVC") as? SearchVC
        menuVC = self.storyboard?.instantiateViewControllerWithIdentifier("MenuVC") as? MenuVC
        //        searchVC?.delegate = musicVC!
        
        search_animator = ZFModalTransitionAnimator(modalViewController: searchVC!)
        search_animator.dragable = true
        search_animator.bounces = false
        search_animator.behindViewAlpha = 0.5
        search_animator.behindViewScale = 1.0
        search_animator.transitionDuration = 0.7
        search_animator.direction = ZFModalTransitonDirection.Left
        searchVC!.transitioningDelegate = search_animator
        searchVC!.modalPresentationStyle = UIModalPresentationStyle.Custom
        
    }
    
    func goToSearchView(){
        
//        presentViewController(searchVC!, animated: true, completion: { () -> Void in
////            self.searchAnimator.setContentScrollView(self.musicVC?.tableView)
//        })
        
//        let searchVC  = self.storyboard?.instantiateViewControllerWithIdentifier("SearchVC") as! SearchVC
        presentViewController(searchVC!, animated: true, completion: { () -> Void in
            
        })
    }
    
    func goToChatView(){
        
        NativeToWebHelper.sharedInstance.showView("chatting", backToViewString: "")
        self.webViewContainerView.hidden = false
        LobeStateHelper.sharedInstance.magicButtonState = .lobeLiveView
    }
    func goToYoutubeView(){
        NativeToWebHelper.sharedInstance.showView("youtubeSearch", backToViewString: "")
        self.webViewContainerView.hidden = false
        LobeStateHelper.sharedInstance.magicButtonState = .lobeLiveView
    }
    func goToLocalLibraryView(){
//        NativeToWebHelper.sharedInstance.showView("youtubeSearch", backToViewString: "")
        self.webViewContainerView.hidden = true
        LobeStateHelper.sharedInstance.magicButtonState = .localMusicView
    }
}

//==========================================================================================
// MARK: - Music Playback Management
//==========================================================================================
extension MainVC {
    
    @IBAction func playOrPauseMusic(sender: UIButton) {
        
        self.handlePlayOrPauseMusic()
        
    }
    
    @IBAction func backButton(sender: AnyObject) {
        handlePreviousButtonTapped()
    }
    @IBAction func forwardButton(sender: UIButton) {
        
        handleNextButtonTapped()
    }
    
    func handlePlayOrPauseMusic() {
        
        if AUDIO_OUT_MANAGER.isMusicPlaying() {
            AUDIO_OUT_MANAGER.pauseMusic()
        }else{
            AUDIO_OUT_MANAGER.playMusic()
        }
        dispatch_async(dispatch_get_main_queue()) {
            AUDIO_OUT_MANAGER.notifyLobeListChange()
            AUDIO_OUT_MANAGER.notifyMusicChange(false)
        }
        
    }
    
    func handleNextButtonTapped(){
        
        AUDIO_OUT_MANAGER.skipToNextMusic()
    }
    func handlePreviousButtonTapped(){

        AUDIO_OUT_MANAGER.skipToPreviousMusic()
    }
}

//==========================================================================================
// MARK: - present InitVC transition Animation
//==========================================================================================
extension MainVC {
    
    func playerViewBecomeTarget(){
        
        self.animator.gestureTargetView = self.playerView
    }
    //TODO:- Wait for the webview to be loaded before allowing interactive animation. Otherwise, the trying to present a modal that is already presented crash will appear
    func setupAnimator() {
        self.animator = ARNTransitionAnimator(operationType: .Present, fromVC: self, toVC: modalVC)
        self.animator.usingSpringWithDamping = 0.8
        self.animator.gestureTargetView = self.playerView
        self.animator.interactiveType = .Present
        
        // Present
        
        self.animator.presentationBeforeHandler = { [weak self] (containerView: UIView, transitionContext:
            UIViewControllerContextTransitioning) in
            self!.animator.direction = .Top
            
            self!.modalVC.view.frame.origin.y = self!.playerView.frame.origin.y + self!.playerView.frame.size.height
            containerView.addSubview(self!.view)
            self!.view.addSubview(self!.modalVC.view)
            //                insertSubview(self!.modalVC.view, belowSubview: self!.tabBar)
            
            self!.view.layoutIfNeeded()
            self!.modalVC.view.layoutIfNeeded()
            
            // miniPlayerView
            let startOriginY = self!.playerView.frame.origin.y
            let endOriginY = -self!.playerView.frame.size.height
            let diff = -endOriginY + startOriginY
            
            self!.animator.presentationCancelAnimationHandler = { (containerView: UIView) in
                self!.playerView.frame.origin.y = startOriginY
                self!.modalVC.view.frame.origin.y = self!.playerView.frame.origin.y + self!.playerView.frame.size.height
                //                self!.tabBar.frame.origin.y = tabStartOriginY
                self!.containerView.alpha = 1.0
                for subview in self!.playerView.subviews {
                    subview.alpha = 1.0
                }
            }
            
            self!.animator.presentationAnimationHandler = { [weak self] (containerView: UIView, percentComplete: CGFloat) in
                let _percentComplete = percentComplete >= 0 ? percentComplete : 0
                self!.playerView.frame.origin.y = startOriginY - (diff * _percentComplete)
                if self!.playerView.frame.origin.y < endOriginY {
                    self!.playerView.frame.origin.y = endOriginY
                }
                self!.modalVC.view.frame.origin.y = self!.playerView.frame.origin.y + self!.playerView.frame.size.height
                
                self!.containerView.alpha = 1.0 - (1.0 * _percentComplete) + 0.5
                for subview in self!.playerView.subviews {
                    subview.alpha = 1.0 - (1.0 * percentComplete)
                }
            }
            
            self!.animator.presentationCompletionHandler = {(containerView: UIView, completeTransition: Bool) in
                if completeTransition {
                    self!.modalVC.view.removeFromSuperview()
                    containerView.addSubview(self!.modalVC.view)
                    self!.animator.interactiveType = .Dismiss
                    self!.animator.gestureTargetView = self!.modalVC.targetImageView
                    self!.animator.direction = .Bottom
                } else {
                    self!.view.removeFromSuperview()
                    UIApplication.sharedApplication().keyWindow!.addSubview(self!.view)
                }
            }
        }
        
        // Dismiss
        self.animator.dismissalBeforeHandler = { [weak self] (containerView: UIView, transitionContext: UIViewControllerContextTransitioning) in
            containerView.addSubview(self!.view)
            self!.view.addSubview(self!.modalVC.view)
            //            self!.view.insertSubview(self!.modalVC.view, belowSubview: self!.tabBar)
            
            self!.view.layoutIfNeeded()
            self!.modalVC.view.layoutIfNeeded()
            
            // miniPlayerView
            let startOriginY = 0 - self!.playerView.bounds.size.height
            let endOriginY = self!.containerView.bounds.size.height - self!.playerView.frame.size.height +  self!.topView.frame.size.height
            let diff = -startOriginY + endOriginY
            
            self!.animator.dismissalCancelAnimationHandler = { (containerView: UIView) in
                self!.playerView.frame.origin.y = startOriginY
                self!.modalVC.view.frame.origin.y = self!.playerView.frame.origin.y + self!.playerView.frame.size.height
                //                self!.tabBar.frame.origin.y = tabStartOriginY
                self!.containerView.alpha = 0.0
                for subview in self!.playerView.subviews {
                    subview.alpha = 0.0
                }
            }
            
            self!.animator.dismissalAnimationHandler = {(containerView: UIView, percentComplete: CGFloat) in
                let _percentComplete = percentComplete >= -0.05 ? percentComplete : -0.05
                self!.playerView.frame.origin.y = startOriginY + (diff * _percentComplete)
                self!.modalVC.view.frame.origin.y = self!.playerView.frame.origin.y + self!.playerView.frame.size.height
                
                self!.containerView.alpha = (1.0 * _percentComplete)
                for subview in self!.playerView.subviews {
                    subview.alpha = 1.0 * _percentComplete
                }
            }
            
            self!.animator.dismissalCompletionHandler = { (containerView: UIView, completeTransition: Bool) in
                if completeTransition {
                    self!.modalVC.view.removeFromSuperview()
                    self!.animator.gestureTargetView = self!.playerView
                    self!.animator.interactiveType = .Present
                    
                    self!.view.removeFromSuperview()
                    UIApplication.sharedApplication().keyWindow!.addSubview(self!.view)
                }
            }
        }
        
//        self.modalVC.transitioningDelegate = self.animator
    }
    
    
    @IBAction func expandPlayerView() {
        
//        self.presentViewController(modalVC, animated: true) { () -> Void in
//            self.menuAnimator.setContentScrollView(self.modalVC.playlistVC?.tableView)
//        }
        
        //Drag to present
        self.animator.interactiveType = .None
        self.presentViewController(modalVC, animated: true, completion: nil)
        
        
    }
}

//==========================================================================================
// MARK: - Menu Delegate
//==========================================================================================
extension MainVC:MenuDelegate{
    
    func menuSearch(){
        self.sideMenuController()?.toggleSidePanel()
        self.goToSearchView()
    }
    func menuGoToCurrentSong(){
    }
    func menuLeaveGroup(){
        self.sideMenuController()?.toggleSidePanel()
        NativeToWebHelper.sharedInstance.leaveGroupSession()
        AUDIO_OUT_MANAGER.userDidLeaveGroup()
//        AUDIO_OUT_MANAGER.stopSocialAudio()
        
    }
    func menuRecalculateSync(){
        NativeToWebHelper.sharedInstance.recalculateSyncAlgo()
    }
    func menuLogout(){
        print("Menu Logout")
    }
    
    func menuViewProfile() {
        print("User Profile")
        self.sideMenuController()?.toggleSidePanel()
        NativeToWebHelper.sharedInstance.showView(MagicButtonViewState.myProfileView.rawValue, backToViewString: "")
         self.webViewContainerView.hidden = false
        LobeStateHelper.sharedInstance.magicButtonState = .lobeLiveView
    }
    
    func menuViewLobeLive(){
        self.sideMenuController()?.toggleSidePanel()
        NativeToWebHelper.sharedInstance.showView(MagicButtonViewState.lobeLiveView.rawValue, backToViewString: "")
        self.webViewContainerView.hidden = false
        LobeStateHelper.sharedInstance.magicButtonState = .lobeLiveView
    }
    func menuViewGroup(){
        self.sideMenuController()?.toggleSidePanel()
        NativeToWebHelper.sharedInstance.showView(MagicButtonViewState.groupView.rawValue, backToViewString: "")
        self.webViewContainerView.hidden = false
        LobeStateHelper.sharedInstance.magicButtonState = .groupView
    }
    
    func menuViewLocalLibrary(){
        self.sideMenuController()?.toggleSidePanel()
        self.webViewContainerView.hidden = true
        LobeStateHelper.sharedInstance.magicButtonState = .localMusicView
    }
    
    @IBAction func menuButtonPressed(sender: AnyObject) {
        
        self.sideMenuController()?.menuDelegate = self
        self.sideMenuController()?.toggleSidePanel()
    }
}

//==========================================================================================
// MARK: - PUSH NOTIF & MAGIC BUTTON
//==========================================================================================
extension MainVC {
    
    func magicButtonStateChanged(){
//        print("Magic Changed")
            switch LobeStateHelper.sharedInstance.magicButtonState {
                case .localMusicView:
                    self.magicButton.hidden = false
                    break
                case .lobeLiveView:
                    self.magicButton.hidden = false
                    break
                case .groupView:
                    self.magicButton.hidden = true
                    break
                case .myProfileView:
                    self.magicButton.hidden = false
                    break
                case .findLobersView:
                    self.magicButton.hidden = false
                    break
            }
        
    }
    
    func actOnPushNotifReceived(){
        print("push received")
        dispatch_async(dispatch_get_main_queue()) {
            NativeToWebHelper.sharedInstance.JavaCallPushNotifActionReceived()
            self.webViewContainerView.hidden = false
        }
    }
    
    @IBAction func magicButtonTapped(sender: AnyObject) {
        
//        let transition = CircularRevealTransition(layer: self.view.layer, center: self.magicButton.center)
//        transition.start()
        WebToNativeHelper.sharedInstance.playNotifSound("tock")
        NativeToWebHelper.sharedInstance.showView("groupView", backToViewString: "")
        self.webViewContainerView.hidden = false
        LobeStateHelper.sharedInstance.magicButtonState = .groupView
        
////        LobeStateHelper.sharedInstance.magicButtonState.goToNextState()
//        self.webViewContainerView.hidden = !self.webViewContainerView.hidden
    }
}

// MARK: - Searchbar Delegate
extension MainVC: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {

    }
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
        self.resignFirstResponder()
        self.goToSearchView()
        return false
    }
    
}


