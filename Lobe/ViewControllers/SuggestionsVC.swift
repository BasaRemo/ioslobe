//
//  SuggestionsVC.swift
//  Lobe
//
//  Created by Professional on 2016-01-08.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit
import MGSwipeTableCell

protocol SuggestionDelegate {
    func openWebView()
}
class SuggestionsVC: UIViewController {

    @IBOutlet var tableView:UITableView!
    @IBOutlet var createGroupButton:UIButton!
    @IBOutlet var joinGroupButton:UIButton!
    @IBOutlet var groupNameLabel:UILabel!

    var delegate:LobeListDelegate?
    var suggestionDelegate:SuggestionDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.dataSource = self
        tableView.delegate = self
        
        LobeStateHelper.sharedInstance.addLobeStateObserver(self)
        AUDIO_OUT_MANAGER.observeMusicChange(self)
        
        createGroupButton.layer.borderWidth = 1
        createGroupButton.layer.cornerRadius = 18
        createGroupButton.layer.borderColor = UIColor.orangeColor().CGColor
        joinGroupButton.layer.borderWidth = 1
        joinGroupButton.layer.cornerRadius = 18
        joinGroupButton.layer.borderColor = UIColor.orangeColor().CGColor
        
        self.tableView.backgroundColor = UIColor.blackColor()
        
    }

    @IBAction func createGroup(sender: UIButton) {
        let lower : UInt32 = 1000
        let upper : UInt32 = 9999
        let randomNumber = arc4random_uniform(upper - lower) + lower
        NativeToWebHelper.sharedInstance.createGroupSession("'\(randomNumber)'")
        print("group Created: \(randomNumber)")
        groupNameLabel.text = "Group number: \(randomNumber)"
        suggestionDelegate!.openWebView()
    }
    
    @IBAction func joinGroup(sender: AnyObject) {
        
        let alert = UIAlertController(title: "Enter group Number", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addTextFieldWithConfigurationHandler { (textField) -> Void in
            textField.placeholder = "group number"
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler:nil))
        alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.Default, handler:{ (UIAlertAction)in
            
            let groupNumber = alert.textFields![0].text!
            print("group Joined: \(groupNumber)")
            NativeToWebHelper.sharedInstance.joinGroupSession("'\(groupNumber)'")
        }))
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    func lobeStateChanged(){
        print("HERE BABY")
        switch LobeStateHelper.sharedInstance.state {
            case .Local:
                self.tableView.hidden = true
            case .DJ:
                self.tableView.hidden = false
            case .Participant:
                self.tableView.hidden = false
            
        }
        
        self.tableView.reloadData()
    }
    
    func actOnMusicChange(notification:NSNotification){
        self.tableView.reloadData()
    }
    
    func updateState(){
        print("SuggestionsVC Update")
    }

    func userStateChanged(){
    }
    
}


func currentTimeMillis() -> Int64{
    let nowDouble = NSDate().timeIntervalSince1970
    return Int64(nowDouble*1000)
}
extension SuggestionsVC: UITableViewDataSource {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("SuggestionCell", forIndexPath: indexPath) as! SuggestionCell
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        print("NUMBER OF ROW: \(AUDIO_OUT_MANAGER.suggestionList.count)")
        return 0
        
    }
    
}

extension SuggestionsVC: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell =  self.tableView.cellForRowAtIndexPath(indexPath) as! SuggestionCell
        cell.selected = false
//        self.tableView.reloadData()
        
    }
}
