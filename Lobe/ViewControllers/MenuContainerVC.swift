//
//  MenuContainerVC.swift
//  Lobe
//
//  Created by Professional on 2016-01-20.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit

class MenuContainerVC: UIViewController {

    @IBOutlet var menuView:UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
       
        let menuVC = self.storyboard?.instantiateViewControllerWithIdentifier("MenuVC") as! MenuVC
        menuView.addSubview(menuVC.tableView)
//        self.menuView.frame = CGRectMake(0,0,200,CGRectGetHeight(self.menuView.frame))
    }

}
