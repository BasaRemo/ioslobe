//
//  InitVC.swift
//  Lobe
//
//  Created by Professional on 2016-01-17.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit
import VYPlayIndicator
import ZFDragableModalTransition
import AVFoundation
import Foundation
import MediaPlayer
import SwiftyJSON
import Alamofire
import Haneke
import Bond
import SVProgressHUD
import JLToast
import RealmSwift
import CircleSlider

protocol InitVCDelegate {
    func openWebView()
    func nextButtonTapped()
    func previousButtonTapped()
    func playPauseButtonTapped()
    func setupTargetView()
    func goToSearchView()
    func goToChatView()
    func goToYoutubeView()
    func goToLocalLibraryView()
}

var social_slider_value : Observable<Double> = Observable<Double>(0.0)
class InitVC: UIViewController {

    @IBOutlet var containerView:UIView!
    @IBOutlet var playbackView:UIView!
    @IBOutlet weak var artWorkImageView: UIImageView!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet var filterView:UIView!
    @IBOutlet var sliderAreaView:UIView!
//    @IBOutlet var currentTrackTimeLabel:UILabel!
//    @IBOutlet var trackLengthLabel:UILabel!
    @IBOutlet var socialButton:UIButton!
    @IBOutlet var playButton:UIButton!
    @IBOutlet var nextButton:UIButton!
    @IBOutlet var previousButton:UIButton!
    @IBOutlet var addButton:UIButton!
    @IBOutlet var settingsButton:UIButton!
    
    @IBOutlet var circularSlider:BWCircularSliderView!

    var realm: Realm!
    
    var trackLength = 0
    var albumArtImage:UIImage?
    var webViewVC: LobeWebVC?
    var mainVC:MainVC?
    var animator:ZFModalTransitionAnimator!
    var animator2:ZFModalTransitionAnimator!
    var currentPlayingSongTimer = NSTimer()
    var useSliderValue = false
    var delegate:InitVCDelegate?
    var playlistVC: PlaylistVC?
    
    private var circleSlider: CircleSlider! {
        didSet {
            self.circleSlider.tag = 0
        }
    }
    
    private var sliderOptions: [CircleSliderOption] {
        return [
            .BarColor(UIColor.clearColor()),
            .ThumbColor(UIColor.orangePositiveColor()),
            .TrackingColor(UIColor.orangePositiveColor()),
            .BarWidth(5),
            .StartAngle(-90),
            .MaxValue(CFloat(trackLength)),
            .MinValue(0),
            .ThumbWidth(25)
        ]
    }
    
    @IBOutlet weak var targetImageView : UIImageView!
    
    //==========================================================================================
    // MARK: - vc Life cycle
    //==========================================================================================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        realm = try! Realm()
        self.playlistVC = self.childViewControllers.first as? PlaylistVC
        
        LobeStateHelper.sharedInstance.addLobeStateObserver(self)
        self.observeCurrentSongChange()
        
        resetSlider()
        setupUI()
        buildCircleSlider()
    }
    
    private func buildCircleSlider() {
        self.circleSlider = CircleSlider(frame: self.sliderAreaView.bounds, options: self.sliderOptions)
        self.circleSlider?.addTarget(self, action: Selector("valueChange:"), forControlEvents: .ValueChanged)
        self.circleSlider?.addTarget(self, action: "circleSliderTouchDown:", forControlEvents: UIControlEvents.TouchDown)
        self.circleSlider?.addTarget(self, action: "circleSliderTouchCanceled:", forControlEvents: UIControlEvents.TouchUpInside)
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: "playOrPauseMusic:")
        self.sliderAreaView.addGestureRecognizer(gestureRecognizer)
        self.sliderAreaView.addSubview(self.circleSlider!)
    }
    
    func valueChange(sender: CircleSlider) {
//        print("\(Int(sender.value))")
    }
    
    func circleSliderTouchDown(slider:CircleSlider){
//        changeThumbWidth(width: 35)
        useSliderValue = true
    }
    func circleSliderTouchCanceled(slider:CircleSlider){
        useSliderValue = false
//        changeThumbWidth(width: 20)
        seekTo(slider.value)
    }
    
    func changeThumbWidth(width width:Float) {
        self.circleSlider.changeOptions([.ThumbWidth(CGFloat(width))])
    }
    func setSliderState(isEnable isEnable:Bool) {
        self.circleSlider.changeOptions([.SliderEnabled(isEnable)])
    }
    func changeSliderMaxValue(value value:Float) {
        self.circleSlider.changeOptions([.MaxValue(value)])
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func  viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        self.delegate?.setupTargetView()
//        disableSlider()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        lobeStateChanged()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }

    var tapCloseButtonActionHandler : (Void -> Void)?
    
    @IBAction func tapCloseButton() {
        self.tapCloseButtonActionHandler?()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
   
//==========================================================================================
// MARK: - UI Setup
//==========================================================================================
    func setupUI(){
        
        //Rotate the slider to start from the top
        self.circularSlider.transform = CGAffineTransformMakeRotation((-90.0 * 3.14) / 180.0)
        // Attach an Action and a Target to the slider
//        let gestureRecognizer = UITapGestureRecognizer(target: self, action: "playOrPauseMusic:")
//        self.circularSlider.addGestureRecognizer(gestureRecognizer)
//        self.circularSlider.slider.addTarget(self, action: "valueChanged:", forControlEvents: UIControlEvents.ValueChanged)
//        self.circularSlider.slider.addTarget(self, action: "circSliderTouchDown:", forControlEvents: UIControlEvents.TouchDown)
//        self.circularSlider.slider.addTarget(self, action: "circSliderTouchCanceled:", forControlEvents: UIControlEvents.TouchUpInside)
        
        blurImage()
        //Playback view separator
        //        let px = 2 / UIScreen.mainScreen().scale
        //        let frame = CGRectMake(0, CGRectGetHeight(self.playbackView.frame), self.playbackView.frame.size.width, px)
        //        let line: UIView = UIView(frame: frame)
        //        self.playbackView.addSubview(line)
        //        line.backgroundColor = UIColor.blackColor()
        //        self.playbackView.backgroundColor = UIColor.playbackBackgroundColor()
        
        
        let width = Int(CGRectGetWidth(self.view.bounds))
        let heigth = Int(CGRectGetHeight(self.view.bounds))
        let croppedImage = DownloadHelper.cropImage( UIImage(named: placeholderImageName)!, withWidth: width , andHeigth:heigth)
        
        self.view.backgroundColor = UIColor(patternImage:croppedImage)
        self.artWorkImageView.image = croppedImage
        
        let gestureRecognizer = UILongPressGestureRecognizer(target: self, action: "openAddSongContextMenu:")
        gestureRecognizer.minimumPressDuration = 0.1
        self.addButton.addGestureRecognizer(gestureRecognizer)
        
        let gestureRecognizer2 = UILongPressGestureRecognizer(target: self, action: "openOptionsContextMenu:")
        gestureRecognizer2.minimumPressDuration = 0.1
        self.settingsButton.addGestureRecognizer(gestureRecognizer2)
        
    }
    
    func blurImage() {
        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        let blurView = UIVisualEffectView(effect: darkBlur)
        blurView.frame = self.view.bounds
        self.view.addSubview(blurView)
        self.view.sendSubviewToBack(blurView)
        //        self.view.insertSubview(blurView, belowSubview: self.imageView)
    }
    
//==========================================================================================
// MARK: - playbackview functions
//==========================================================================================

    @IBAction func playOrPauseMusic(sender: UIButton) {
        self.delegate!.playPauseButtonTapped()
    }
    
    @IBAction func previousButtonPressed(sender: UIButton) {
        if LobeStateHelper.sharedInstance.state == .Participant {
            sender.selected = !sender.selected
        }
        self.delegate!.previousButtonTapped()
    }
    @IBAction func nextButtonPressed(sender: UIButton) {
        if LobeStateHelper.sharedInstance.state == .Participant {
            sender.selected = !sender.selected
        }
        self.delegate!.nextButtonTapped()
    }
    
    //Scrolls to the current playing song
    @IBAction func seekBtnPressed(sender: UIButton) {

        scrollToCurrentPlayingItem()
    }
    func scrollToCurrentPlayingItem() {
        
        guard AUDIO_OUT_MANAGER.lobeListSongItems.count > 0 else {
            print("No song in lobelist")
//            JLToast.makeText("No song in lobelist").show()
            return
        }
        //TODO: - Crashes here because cant find the current song
        let indexOfCurrentPlayingMusic = AUDIO_OUT_MANAGER.getIndexOfCurrentPlayingLobeListTrack()
        let indexPath = NSIndexPath(forRow: 0, inSection: indexOfCurrentPlayingMusic)
         playlistVC!.tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
    }
}

//==========================================================================================
// MARK: - Slider
//==========================================================================================
extension InitVC {
    
    func valueChanged(slider:BWCircularSlider){
        
        //        print("Value changed \(slider.angle)")
    }
    func circSliderTouchDown(slider:BWCircularSlider){
        
        useSliderValue = true
    }
    func circSliderTouchCanceled(slider:BWCircularSlider){
        useSliderValue = false
        
        let revereSeekTime:Int = Int(trackLength*slider.angle/360)
        let seekTime = Float(trackLength - revereSeekTime)
        
        seekTo(seekTime)
    }
    
    func resetSlider(){
        setupSlider()
        startCurrentPlayingSongTimer()
        updateTrackLength()
    }
    func setupSlider(){
//        trackLengthLabel.text = "00:00"
//        currentTrackTimeLabel.text = "00:00"
        circularSlider.slider.angle = 360
        circularSlider.slider.setNeedsDisplay()
    
    }
    func startCurrentPlayingSongTimer(){
        
        currentPlayingSongTimer.invalidate()
        // start the timer
        currentPlayingSongTimer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "timerAction", userInfo: nil, repeats: true)
    }
    
    func stopLocalSongTimer(){
        currentPlayingSongTimer.invalidate()
    }
    func timerAction() {
        if  AUDIO_OUT_MANAGER.isMusicPlaying() && !useSliderValue{
                updateTime()
        }
    }

    func updateTime() {
        
        let currentTime = Int(AUDIO_OUT_MANAGER.player.currentPlaybackTime)
        let minutes = currentTime/60
        let seconds = currentTime - minutes * 60

        let sliderAngle = (trackLength - currentTime)*360/trackLength
        circularSlider.slider.angle = sliderAngle
        circularSlider.slider.setNeedsDisplay()
        circleSlider.value = Float(currentTime)
//        currentTrackTimeLabel.text = NSString(format: "%02d:%02d", minutes,seconds) as String
        
    }
    //FIXME:- IMPORTANT - This function crashed when called BEFORE THE nowPlayingItem in the local player is set
    func updateTrackLength() {
        guard let nowPlayingItem = AUDIO_OUT_MANAGER.player.nowPlayingItem else {
            return
        }
         trackLength = Int(nowPlayingItem.playbackDuration)
        let minutes = trackLength/60
        let seconds = trackLength - minutes * 60
//        trackLengthLabel.text = NSString(format: "%02d:%02d", minutes,seconds) as String
//        print("trackLength:\(trackLength)")
    }
    
    func seekTo(currentTime:Float){
        AUDIO_OUT_MANAGER.player.currentPlaybackTime = Double(currentTime)
    }
}


//==========================================================================================
// MARK: - Observers
//==========================================================================================
extension InitVC {
    
    func observeCurrentSongChange(){
        
        AUDIO_OUT_MANAGER.currentTrackObservable.observe{ currentTrack in
            
            dispatch_async(dispatch_get_main_queue()) {
                self.songNameLabel.text = currentTrack.trackInfo.mName
                self.artistLabel.text = currentTrack.trackInfo.mArtist
            }
            
            currentTrack.trackInfo.songAlbumArt.observe{ image in
                if let albumImage = image {
                    
                    let croppedImage = DownloadHelper.cropImage(albumImage, withWidth: Int(CGRectGetWidth(self.view.bounds)) , andHeigth:Int(CGRectGetHeight(self.view.bounds)))
                    dispatch_async(dispatch_get_main_queue()) {
                        self.artWorkImageView.image = croppedImage
                        self.view.backgroundColor = UIColor(patternImage: croppedImage)
                    }
                }
            }
        }
        
        //Observe audio playing state
        AUDIO_OUT_MANAGER.isAudioPlaying.observe{ currentTrack in
            dispatch_async(dispatch_get_main_queue()) {
                self.playButton.selected = AUDIO_OUT_MANAGER.isMusicPlaying()
            }
        }
        
    }
}

//==========================================================================================
// MARK:- App State Change management
//==========================================================================================
extension InitVC {
    
    func lobeStateChanged(){
        self.updateState()
    }

    func updateState(){
        print("ProfilView update")
        
        nextButton.setImage(UIImage(named: "next_button_unfill"), forState: UIControlState.Normal)
        nextButton.setImage(UIImage(named: "next_button"), forState: UIControlState.Highlighted)
        nextButton.setImage(UIImage(named: "next_button"), forState: UIControlState.Selected)
        
        previousButton.setImage(UIImage(named: "previous_button_unfill"), forState: UIControlState.Normal)
        previousButton.setImage(UIImage(named: "previous_button"), forState: UIControlState.Highlighted)
        previousButton.setImage(UIImage(named: "previous_button"), forState: UIControlState.Selected)
        
        switch LobeStateHelper.sharedInstance.state {
            
        case .Local:
            self.circularSlider.slider.userInteractionEnabled = true
            dispatch_async(dispatch_get_main_queue()) {
                self.settingsButton.setImage(UIImage(named: "option_button_2"), forState: UIControlState.Normal)
                self.setSliderState(isEnable: true)
                self.changeSliderMaxValue(value: Float(self.trackLength))
            }
            
            startCurrentPlayingSongTimer()
            stateLabel.text = "Local Libray"
            
            
        case .DJ:
            self.circularSlider.slider.userInteractionEnabled = false
            stopLocalSongTimer()
            
            
            stateLabel.text = "DJ Broadcast"
            
            //Make sure im in the main queue
            dispatch_async(dispatch_get_main_queue()) {
                self.setSliderState(isEnable: false)
                self.changeSliderMaxValue(value: 1)
                 self.settingsButton.setImage(UIImage(named: "share_icon"), forState: UIControlState.Normal)
                
                social_slider_value.observe{ sliderValue in
                    let sliderAngle = (1 - sliderValue)*360
                    self.circularSlider.slider.angle = Int(sliderAngle)
                    self.circularSlider.slider.setNeedsDisplay()
                    self.circleSlider.value = Float(social_slider_value.value)
                }
            }

            
        case .Participant:
            self.circularSlider.slider.userInteractionEnabled = false
            stopLocalSongTimer()
            
            dispatch_async(dispatch_get_main_queue()) {
                self.setSliderState(isEnable: false)
                self.changeSliderMaxValue(value: 1)
                 self.settingsButton.setImage(UIImage(named: "share_icon"), forState: UIControlState.Normal)
                
                social_slider_value.observe{ sliderValue in
                    let sliderAngle = (1 - sliderValue)*360
                    self.circularSlider.slider.angle = Int(sliderAngle)
                    self.circularSlider.slider.setNeedsDisplay()
                    self.circleSlider.value = Float(social_slider_value.value)

                    
                }
            }
            stateLabel.text = "Streaming"
//            self.view.backgroundColor = UIColor.redColor()
            nextButton.setImage(UIImage(named: "like_btn_unselected"), forState: UIControlState.Normal)
            nextButton.setImage(UIImage(named: "like_btn_selected"), forState: UIControlState.Highlighted)
            nextButton.setImage(UIImage(named: "like_btn_selected"), forState: UIControlState.Selected)
            
            previousButton.setImage(UIImage(named: "dislike_btn_unselected"), forState: UIControlState.Normal)
            previousButton.setImage(UIImage(named: "dislike_btn_selected"), forState: UIControlState.Highlighted)
            previousButton.setImage(UIImage(named: "dislike_btn_selected"), forState: UIControlState.Selected)
            
        }
    }
}

//==========================================================================================
// MARK:- Add song management
//==========================================================================================
extension InitVC {
    
    @IBAction func addTrackButtonTapped(sender: UIButton) {
        showTrackOptionPopover(0)
    }
    
    func showTrackOptionPopover(index:Int){

        var alert: UIAlertController!
        var firstAction: UIAlertAction!
        var secondAction: UIAlertAction!
        var alertTitle = ""
        var actionTitle = ""
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: {(alert: UIAlertAction!) in
            print("cancel")
        })
        
        switch index
        {
        case 0:
            alertTitle = ""
            alert = UIAlertController(title: "Add Song", message: "", preferredStyle: .ActionSheet)
            firstAction = UIAlertAction(title: "From Local library", style: UIAlertActionStyle.Default) { (alert: UIAlertAction!) -> Void in
                self.delegate?.goToLocalLibraryView()
                self.dismissViewControllerAnimated(true, completion: nil)
            }
            secondAction = UIAlertAction(title: "From youtube", style: .Default) { (alert: UIAlertAction!) -> Void in
//                NativeToWebHelper.sharedInstance.showView("youtubeSearch", backToViewString: "")
                self.delegate?.goToYoutubeView()
                self.dismissViewControllerAnimated(true, completion: nil)
                
            }
            alert.addAction(secondAction)
            alert.addAction(firstAction)
            alert.addAction(cancelAction)
            presentViewController(alert, animated: true, completion:nil)
            
        case 1:
            alertTitle = ""
            alert = UIAlertController(title: "LobeList options", message: "", preferredStyle: .ActionSheet)
            firstAction = UIAlertAction(title: "Remove all items", style: UIAlertActionStyle.Destructive) { (alert: UIAlertAction!) -> Void in
                AUDIO_OUT_MANAGER.lobeListSongItems.removeAllObjects()
                AUDIO_OUT_MANAGER.lobeListObservableList.replaceRange(0...0, with: [SongItem()])
            }
            secondAction = UIAlertAction(title: "Save", style: UIAlertActionStyle.Default) { (alert: UIAlertAction!) -> Void in
                self.savePlaylist()
                
            }
            alert.addAction(secondAction)
            alert.addAction(firstAction)
            alert.addAction(cancelAction)
            presentViewController(alert, animated: true, completion:nil)
            
        case 2:
            break
            
        case 3:
            alertTitle = "Visit"
            actionTitle = ""
            alert = UIAlertController(title: "\(alertTitle) our website", message: "", preferredStyle: .ActionSheet)
            firstAction = UIAlertAction(title: actionTitle, style: .Default) { (alert: UIAlertAction!) -> Void in
                self.navigationController?.pushViewController(self.webViewVC!, animated: true)
            }
            alert.addAction(firstAction)
            alert.addAction(cancelAction)
            presentViewController(alert, animated: true, completion:nil)

        default:
            break
        }
        
    }
}



//==========================================================================================
// MARK:- Chat management
//==========================================================================================
extension InitVC {
    
    @IBAction func chatButtonTapped(sender: UIButton) {
        
        self.delegate?.goToChatView()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
        
//==========================================================================================
// MARK:- Playlist management
//==========================================================================================
extension InitVC {
    
    @IBAction func optionButtonTapped(sender: UIButton) {
        
        showTrackOptionPopover(1)
    }
    
    func savePlaylist(){
        
        let alertController = UIAlertController(title: "Save Playlist", message:
            "", preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "New Playlist Name"
        }
        alertController.addAction(UIAlertAction(title: "Save", style: UIAlertActionStyle.Default,handler: { (action) -> Void in
            let textField = alertController.textFields![0] as UITextField
            if (textField.text != "")  && (AUDIO_OUT_MANAGER.lobeListSongItems.count > 0) {
                
                //Override playlist if has same name
                let savedPlaylists = self.realm.objects(UserPlaylist).filter("playlistName_='\(textField.text!)'")
                if savedPlaylists.count > 0 {
                    for elem in savedPlaylists{
                        try! self.realm.write {
                            self.realm.delete(elem)
                        }
                        
                    }
                }
                let newPlaylist = UserPlaylist()
                newPlaylist.idPlaylist_ = NSUUID().UUIDString
                newPlaylist.playlistName_ = textField.text!
                
                for elem in AUDIO_OUT_MANAGER.lobeListSongItems{
                    let newTrack = UserTrack()
                    let songItem  = elem as! SongItem
                    newTrack.idTrack_ = songItem.trackInfo.mID
                    newTrack.trackName_ = songItem.trackInfo.mName
                    
                    newPlaylist.tracks_.append(newTrack)
                    try! self.realm.write({ () -> Void in
                        self.realm.add(newTrack)
                    })
                }
                
                try! self.realm.write({ () -> Void in
                    self.realm.add(newPlaylist)
                })
                
                self.playlistVC!.tableView.reloadData()
                JLToast.makeText("\(textField.text!) is saved").show()
                
                let pageDict: Dictionary<String,String>! = [
                    "playlistName": "\(textField.text!)",
                ]
                NSNotificationCenter.defaultCenter().postNotificationName("NewNameNotification", object: nil, userInfo: pageDict)
            }
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default,handler: { (action) -> Void in
            self.dismissViewControllerAnimated(true, completion: nil)
        }))
        self.presentViewController(alertController, animated: true){
            
        }
    }
}
//==========================================================================================
// MARK:- Segue
//==========================================================================================
extension InitVC {
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if let vc = segue.destinationViewController as? PlaylistVC
            where segue.identifier == "PlaylistVC_segue" {
                if self.playlistVC == nil {
                    self.playlistVC = vc
                }
        }
        
        if segue.identifier == "playlistOption"{
            let optionListView:OptionsTableViewVC = segue.destinationViewController as! OptionsTableViewVC

            if AUDIO_OUT_MANAGER.lobeListSongItems.count != 0{

                if playlistVC?.sectionTitle != "New Lobe List"{
                    optionListView.dataArray.append("Save")
                    optionListView.preferredContentSize = CGSize(width: 200, height: 186)
//                    print("\(self.playlistName_)")
                }
                optionListView.dataArray.append("Clean / new LobeList")
                optionListView.tableView.sizeToFit()
            }else{
                optionListView.preferredContentSize = CGSize(width: 200, height: 100)
            }
            optionListView.popoverPresentationController?.delegate=self
        }

    }
}

extension InitVC:UIPopoverPresentationControllerDelegate{
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.None
    }
}

//==========================================================================================
// MARK:- Dragging alert
//==========================================================================================
extension InitVC {
    
    func openAddSongContextMenu(gesture:UILongPressGestureRecognizer){
        
        PCRapidSelectionView.viewForParentView(self.view ,currentGuestureRecognizer:gesture,interactive:true, options:["From Youtube","From Library"], title:"Quick Song Select", completionHandler: { (index:Int) -> Void in
                print("Index selected: \(index)")
            
            switch index {
                case 0:
                    self.delegate?.goToYoutubeView()
                    self.dismissViewControllerAnimated(true, completion: nil)
                case 1:
                    self.delegate?.goToLocalLibraryView()
                    self.dismissViewControllerAnimated(true, completion: nil)
                default:break
            }
            
            })
    }
    
    func openOptionsContextMenu(gesture:UILongPressGestureRecognizer){
        
        switch LobeStateHelper.sharedInstance.state {
            case .Local:
                PCRapidSelectionView.viewForParentView(self.view ,currentGuestureRecognizer:gesture,interactive:true, options:["Save","Remove All"], title:"Lobe list Options", completionHandler: { (index:Int) -> Void in
                    print("Index selected: \(index)")
                    
                    switch index {
                    case 0:
                        self.savePlaylist()
                    case 1:
                        AUDIO_OUT_MANAGER.lobeListSongItems.removeAllObjects()
                        AUDIO_OUT_MANAGER.lobeListObservableList.replaceRange(0...0, with: [SongItem()])
                    default:break
                    }
                })
            case .DJ,.Participant:
                PCRapidSelectionView.viewForParentView(self.view ,currentGuestureRecognizer:gesture,interactive:true, options:["Share Lobelist on Facebook"], title:"Lobelist Options", completionHandler: { (index:Int) -> Void in
                    print("Index selected: \(index)")
                    
                    switch index {
                    case 0:
                         JLToast.makeText("This functionality is not yet implemented").show()
                    default:break
                    }
                })
        }

    }
}
