//
//  LibraryContainerVC.swift
//  Lobe
//
//  Created by Professional on 2016-01-21.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit
import CarbonKit

enum CurrentView :Int {
    case AllMusic = 0
    case Album = 1
    case Artist = 2

    private init () {
        self = .AllMusic
    }
}

class LibraryContainerVC: UIViewController {
    
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var libraryContainer:UIView!
    @IBOutlet var detailsViewContainer:UIView!
    
    var musicVC:MusicVC!
    var userPlaylistsVC:UserPlaylistsVC!
    var albumVC:AlbumVC!
    var artistVC:AlbumVC!
    
    var carbonTabSwipeNavigation:CarbonTabSwipeNavigation!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViewControllers()
//        libraryContainer.hidden = true
        
    }
    
    func setupViewControllers(){
        
        musicVC = storyboard!.instantiateViewControllerWithIdentifier("MusicVC") as? MusicVC
        userPlaylistsVC = storyboard!.instantiateViewControllerWithIdentifier("UserPlaylistsVC") as? UserPlaylistsVC
        albumVC = storyboard!.instantiateViewControllerWithIdentifier("AlbumVC") as? AlbumVC
        artistVC = storyboard!.instantiateViewControllerWithIdentifier("AlbumVC") as? AlbumVC
    
        setupCarbonKitTab()
    }
    
}


// MARK: - CarbonTabSwipeNavigation Delegate
extension LibraryContainerVC:CarbonTabSwipeNavigationDelegate{
    
    func setupCarbonKitTab(){
        
        let tabMenuItems = ["Music","Playlists","Albums","Artists"]
        
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: tabMenuItems, delegate: self)
        carbonTabSwipeNavigation.insertIntoRootViewController(self.childViewControllers[1])
        
        carbonTabSwipeNavigation.toolbar.setBackgroundImage(UIImage(), forToolbarPosition: .Top, barMetrics: .Default)
        carbonTabSwipeNavigation.toolbar.setShadowImage(UIImage(), forToolbarPosition: .Top)
        carbonTabSwipeNavigation.toolbar.translucent = true
        
        carbonTabSwipeNavigation.setIndicatorColor(UIColor.clearColor())
        carbonTabSwipeNavigation.toolbarHeight.constant = 40
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        let width = screenSize.width/4
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(width, forSegmentAtIndex: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(width+2, forSegmentAtIndex: 1)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(width+2, forSegmentAtIndex: 2)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(width+2, forSegmentAtIndex: 3)
        
        // Custimize segmented control
        carbonTabSwipeNavigation.setNormalColor(UIColor.hexColor("D2D2D2"), font: UIFont (name: "HelveticaNeue", size: 11)!)
        carbonTabSwipeNavigation.setSelectedColor(UIColor.orangeColor(), font: UIFont (name: "HelveticaNeue-Bold", size: 11)!)
        for (var i:UInt = 0; i < 3; i++){
            carbonTabSwipeNavigation.currentTabIndex = i
            carbonTabSwipeNavigation.currentTabIndex = 0
        }
        //
    }
    
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAtIndex index: UInt) -> UIViewController {
                switch index {
                    case 0:
                        return musicVC
                    case 1:
                        return userPlaylistsVC
                    case 2:
                        return albumVC
                    case 3:
                        return artistVC
                    
                    default:return musicVC
        
                }
    }
    
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAtIndex index: UInt) {
        
    }
    
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAtIndex index: UInt) {
        
    }
    
    func barPositionForCarbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation) -> UIBarPosition {
        return UIBarPosition.Top
    }
}

// MARK: Searchbar Delegate
//extension LibraryContainerVC: UISearchBarDelegate {
//    
//    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
//        searchBar.setShowsCancelButton(true, animated: true)
//        state = .SearchMode
//    }
//    
//    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
//        searchBar.resignFirstResponder()
//        searchBar.text = ""
//        searchBar.setShowsCancelButton(false, animated: true)
//        state = .DefaultMode
//        self.dismissViewControllerAnimated(true, completion: nil)
//    }
//    
//    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
//        //        delegate?.searchSongWithText(searchText)
//    }
//}
