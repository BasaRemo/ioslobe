//
//  ParticipantState.swift
//  Lobe
//
//  Created by Professional on 2016-02-20.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit

class ParticipantState: SocialState {

    static let sharedInstance = ParticipantState()
    private override init() {
        super.init()
    }
    static func getSharedInstance() -> ParticipantState{
        return sharedInstance
    }
    
    override func directPlayTrack(){
        //Direct play not allowed at the moment for participant
    }
    
    //In participant Mode: the next button is for upVoting the current playing track
    override func skipToNextMusic(){
        let currentSong = AUDIO_OUT_MANAGER.currentSong
            
            let resultArr = AUDIO_OUT_MANAGER.lobeListSongItems.filter({ $0.trackInfo.mID == currentSong.trackInfo.mID })
            if resultArr.count > 0 {
                // any matching items are in results
                let songItem = resultArr[0]
                if songItem.trackInfo.isMePromotingTrack() == false {
                    songItem.trackInfo.mPromotingUserIdsJsonArray.append(songItem.trackInfo.mID)
                    NativeToWebHelper.sharedInstance.promoteTrackInLobeList(-1, trackUid: songItem.trackInfo.mID)
                }
            }

    }
     //In participant Mode: the previous button is for downVoting the current playing track
    override func skipToPreviousMusic(){
        
        let currentSong = AUDIO_OUT_MANAGER.currentSong
            
            let resultArr = AUDIO_OUT_MANAGER.lobeListSongItems.filter({ $0.trackInfo.mID == currentSong.trackInfo.mID })
            if resultArr.count > 0 {
                // any matching items are in results
                let songItem = resultArr[0]
                if songItem.trackInfo.isMePromotingTrack() == false {
                    songItem.trackInfo.mDislikingUserIdsJsonArray.append(songItem.trackInfo.mID)
                    NativeToWebHelper.sharedInstance.dislikeTrackInLobeList(-1, trackUid: songItem.trackInfo.mID)
                }
            }
    }
    override func skipToBeginning(){
        preconditionFailure("SocialState directPlayTrack() : This method must be overridden")
    }
}
