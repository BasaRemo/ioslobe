//
//  SocialState.swift
//  Lobe
//
//  Created by Professional on 2016-02-20.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit

class SocialState: LobeAppState {

//    static let sharedInstance = SocialState()
//    static func getSharedInstance() -> SocialState{
//        return sharedInstance
//    }
//    private override init() {
//        super.init()
//    }
    
    override func isTrackPlaying() -> Bool {
        return AUDIO_OUT_MANAGER.isGroupMusicPlaying
    }
    
    override func playTrack(){
        
        guard AUDIO_OUT_MANAGER.lobeListSongItems.count > 0 else {
            print("No song in the playlist")
            return
        }
        
        guard !AUDIO_OUT_MANAGER.isGroupMusicPlaying else {
            print("A song is already playing in social mode")
            return
        }
        
        startPlayAction()
        
//            if let _ = AUDIO_OUT_MANAGER.currentSong {
//                startPlayAction()
//            }else if AUDIO_OUT_MANAGER.lobeListSongItems.count > 0 {
//                AUDIO_OUT_MANAGER.currentSong = AUDIO_OUT_MANAGER.lobeListSongItems[0] as! SongItem
//                directPlayTrack()
//            }
    }
    
    override func resumeTrack(){
        guard AUDIO_OUT_MANAGER.lobeListSongItems.count > 0 else {
            print("No song in the playlist")
            return
        }
        
        guard !AUDIO_OUT_MANAGER.isGroupMusicPlaying else {
            print("A song is already playing in social mode")
            return
        }
        startPlayAction()
    }
    
    func startPlayAction(){
        
        print("Start playing action")
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            
            if AUDIO_OUT_MANAGER.currentSong.trackInfo.mIsYoutubeVideo == false {
                streamingManager_sharedInstance.resumeAudioQueue()
            }
            AUDIO_OUT_MANAGER.isGroupMusicPlaying = true
            NativeToWebHelper.sharedInstance.setIsPlayingMediaContentAllowed(true)
            
            dispatch_async(dispatch_get_main_queue()) {
                AUDIO_OUT_MANAGER.notifyMusicChange(false)
            }
        }
    }
    
    override func directPlayTrack(){
        preconditionFailure("SocialState directPlayTrack() : This method must be overridden")
    }
    override func skipToNextMusic(){
        preconditionFailure("SocialState directPlayTrack() : This method must be overridden")
    }
    override func skipToPreviousMusic(){
        preconditionFailure("SocialState directPlayTrack() : This method must be overridden")
    }
    override func skipToBeginning(){
        preconditionFailure("SocialState directPlayTrack() : This method must be overridden")
    }
    

}
