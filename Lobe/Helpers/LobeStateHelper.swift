//
//  LobeStateHelper.swift
//  Lobe
//
//  Created by Professional on 2016-01-10.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation

enum LobeState :String {
    case DJ = "Broadcast"
    case Participant = "Listening"
    case Local = "Local"
    
    private init () {
        self = .Local
    }
}

enum MagicButtonViewState :String {
    
    case localMusicView = "localMusicView"
    case groupView = "groupView"
    case lobeLiveView = "lobeLiveView"
    case myProfileView = "myProfileView"
    case findLobersView = "findLobersView"
    
    private init () {
        self = .localMusicView
    }

    mutating func goToState(nextState nextState:MagicButtonViewState){
        //NativeToWebHelper.sharedInstance.showView(nextState.rawValue, backToViewString: self.rawValue)
        self = nextState
    }
    
    mutating func goToNextState(){
        
        switch self {
            
            // In localView
            case .localMusicView:
                switch LobeStateHelper.sharedInstance.state {
                    case .Local:
                        self = .groupView
                    case .DJ:
                        self = .groupView
                    case .Participant:
                        self = .groupView
                }
            
            // In groupView
            case .groupView:
                switch LobeStateHelper.sharedInstance.state {
                    case .Local:
                        self = .localMusicView //Should never happen
                    case .DJ:
                        self = .localMusicView
                    case .Participant:
                        self = .localMusicView
                }
            
            //In lobeLiveView
            case .lobeLiveView:
                switch LobeStateHelper.sharedInstance.state {
                    case .Local:
                        self = .localMusicView
                    case .DJ:
                        self = .groupView
                    case .Participant:
                        self = .groupView
                }
            
            //In myProfileView
            case .myProfileView:
                switch LobeStateHelper.sharedInstance.state {
                case .Local:
                    self = .localMusicView
                case .DJ:
                    self = .groupView
                case .Participant:
                    self = .groupView
                }
            
            //In findLobersView
            case .findLobersView:
                switch LobeStateHelper.sharedInstance.state {
                case .Local:
                    self = .localMusicView
                case .DJ:
                    self = .groupView
                case .Participant:
                    self = .groupView
                }
        }
    }
}

let musicNotificationKey = "musicChangeNotification:"
let lobeStateNotificationKey = "lobeStateChangeNotification"
let magicButtonNotificationKey = "magicButtonChangeNotification"
let userStateNotificationKey = "userStateChangeNotification"
let pushNotificationKey = "pushNotificationKey"
let lobeListNotificationKey = "lobeListNotificationKey"
class LobeStateHelper: NSObject {

    var user_location_latitude: CLLocationDegrees?
    var user_location_longitude: CLLocationDegrees?
    
    var pushNotifList = [PushNotif]()
    var pushNotif:JSON?
    static let sharedInstance = LobeStateHelper()
    static var currentUser = User(){
        didSet{
            print("USER STATE CHANGED")
            currentUser.downloadCurrentUserImage()
            notifyUserStateChange()
        }
    }
    
    private override init() {
        super.init()
    }
    
    var state = LobeState() {
        didSet{
            print("State moved from \(oldValue) to \(state)")
            
            
            switch state {
                case .Local:
                    AUDIO_OUT_MANAGER.lobeAppState = LocalState.getSharedInstance()
                    
                    if oldValue == .Participant || oldValue == .DJ {
                        AUDIO_OUT_MANAGER.resetUploadDirectory()
                    }
                
                case .DJ:
                    AUDIO_OUT_MANAGER.lobeAppState = HostState.getSharedInstance()
//                    AUDIO_OUT_MANAGER.saveLocalMusicState()
                case .Participant:
                     AUDIO_OUT_MANAGER.lobeAppState = ParticipantState.getSharedInstance()
//                    AUDIO_OUT_MANAGER.saveLocalMusicState()
            }
            notifyLobeStateChange()
        }
    }
    
    var magicButtonState = MagicButtonViewState() {
        
        willSet {
//            print("magicButtonState is moving from \(magicButtonState), to \(newValue)")
        }
        
        didSet{
            print("magicButtonState moved from \(oldValue) to \(magicButtonState)")
            if oldValue == .localMusicView {
                NativeToWebHelper.sharedInstance.showView(magicButtonState.rawValue, backToViewString: oldValue.rawValue)
            }
            
            notifyMagicButtonStateChange()
        }
    }
    
    // Lobe states
    func addLobeStateObserver(viewController: AnyObject){
        NSNotificationCenter.defaultCenter().addObserver(viewController, selector: "lobeStateChanged", name: lobeStateNotificationKey, object: nil)
    }
    func notifyLobeStateChange() {
        NSNotificationCenter.defaultCenter().postNotificationName(lobeStateNotificationKey, object: nil)
    }
    
    //User/Social State
    func addUserStateObserver(viewController: AnyObject){
        NSNotificationCenter.defaultCenter().addObserver(viewController, selector: "userStateChanged", name: userStateNotificationKey, object: nil)
    }
    static func notifyUserStateChange() {
        NSNotificationCenter.defaultCenter().postNotificationName(userStateNotificationKey, object: nil)
    }
    
    //Magic button state
    func addMagicButtonStateObserver(viewController: AnyObject){
        NSNotificationCenter.defaultCenter().addObserver(viewController, selector: "magicButtonStateChanged", name: magicButtonNotificationKey, object: nil)
    }
    func notifyMagicButtonStateChange() {
        NSNotificationCenter.defaultCenter().postNotificationName(magicButtonNotificationKey, object: nil)
    }
    
    //Push notif received
    func addPushNotificatonObserver(viewController: AnyObject){
        NSNotificationCenter.defaultCenter().addObserver(viewController, selector: Selector("actOnPushNotifReceived"), name: pushNotificationKey, object: nil)
    }
    
    func notifyPushNotifReceived() {
        NSNotificationCenter.defaultCenter().postNotificationName(pushNotificationKey, object: nil)
    }
    
    

}
