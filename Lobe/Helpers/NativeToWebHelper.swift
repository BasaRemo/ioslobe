//
//  NativeToWebHelper.swift
//  Lobe
//
//  Created by Professional on 2016-01-05.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol NativeToWebHelperDelegate {
    func genericFunctionCall(functionName:String, params:String)
}

class NativeToWebHelper: NSObject {
    
    static let sharedInstance = NativeToWebHelper()
    var delegate:NativeToWebHelperDelegate?
    
    private override init() {
        super.init()
    }
    
    func JavaCallPushNotifActionReceived()
    {
        let pushNotif = LobeStateHelper.sharedInstance.pushNotif!.rawString()
        let functionName:String = "JavaCallPushNotifActionReceived";
        self.delegate?.genericFunctionCall(functionName, params: "'\(replaceBadChar(pushNotif!))'")
    }
    
    func fromJavaDeviceLocationCallback(longitude:Double,latitude:Double)
    {
        let functionName:String = "fromJavaDeviceLocationCallback";
        self.delegate?.genericFunctionCall(functionName, params: "'\(longitude)','\(latitude)'")
    }
    
    func showView(viewString:String,backToViewString:String)
    {
        let functionName:String = "JavaCallShowView";
        self.delegate?.genericFunctionCall(functionName, params: "'\(viewString)','\(backToViewString)'")
    }
    
    // MARK: -  Nabil : Fonction a utiliser
    func fileUploadResponseFromServer(status:Int,resultStr:String, correlationID:String, chunkStartServerTime:UInt64){
        let functionName:String  = "JavaCallFileUploadResponseFromServer"
        self.delegate?.genericFunctionCall(functionName, params: "\(status), '\(resultStr)', '\(correlationID)', \(chunkStartServerTime)")
    }
    
    // MARK: -  Nabil : Fonction a utiliser
    func chunksLocalCreationCallback( chunckInfoList:[ChunkInfo], status:Int){
        
        let functionName:String = "JavaCallChunksLocalCreationCallback"
        
        if chunckInfoList.count != 0 {
            
            let chunckInfoListJSON = AUDIO_OUT_MANAGER.chunkInfoListToJSONArrayData(chunckInfoList)

            for (_,subJson):(String, JSON) in chunckInfoListJSON {
                if let chunkInfoListString = subJson.rawString() {
                    self.delegate?.genericFunctionCall(functionName, params: "'\(replaceBadChar(chunkInfoListString))',\(status)")
                }else{
                    print("chunksLocalCreationCallback: FAILURE Sending ChunkInfoList TO Social")
                }
            }
            
        }else {
            self.delegate?.genericFunctionCall(functionName, params: "null ,'\(status)'")
        }
        
    }
    
    func pushSingleSuggestionItem(track:TrackInfo){
        
        let letTrackJson = AUDIO_OUT_MANAGER.trackInfoToJSON(track)
        if let string1 = letTrackJson.rawString() {
            
            let functionName:String  = "JavaCallPushSingleSuggestionItem";
            self.delegate?.genericFunctionCall(functionName, params: "'\( replaceBadChar(string1))'")
        }else{
            print("FAILURE PUSHING SUGGESTION TO WEB")
        }
    }
    
    func removeSingleSuggestionItem(track:TrackInfo)
    {
        let functionName:String  = "JavaCallRemoveSingleSuggestionItem";
        self.delegate?.genericFunctionCall(functionName, params: "\(track)")
    }
    
    func leaveGroupSession(){
        let functionName:String = "JavaCallLeaveGroupSession"
        self.delegate?.genericFunctionCall(functionName, params: "null")
    }
    
    func createGroupSession(name:String){
        let functionName:String  = "JavaCallCreateGroupSession";
        self.delegate?.genericFunctionCall(functionName, params: "\(name)")
    }
    
    func joinGroupSession(name:String){
        let functionName:String  = "JavaCallJoinGroupSession";
        self.delegate?.genericFunctionCall(functionName, params: "\(name)")
    }
    
    func recalculateSyncAlgo(){
        let functionName:String = "JavaCallRecalculateSyncAlgo"
        self.delegate?.genericFunctionCall(functionName, params: "null")
    }
    
    // called on when in broadcasting state, (3)
    func updateLobeLists(var lobeActiveList:[TrackInfo] , lobeSuggestionList:[TrackInfo]  ){
        
        lobeActiveList.removeAll()
        for songItem:SongItem in  (AUDIO_OUT_MANAGER.lobeListSongItems as NSArray as! [SongItem])  {
            lobeActiveList.append(songItem.trackInfo)
        }
        
        let suggestionListJSON = AUDIO_OUT_MANAGER.musicListToJSONObjectData(lobeSuggestionList)
        let lobeListJSON = AUDIO_OUT_MANAGER.musicListToJSONArrayData(lobeActiveList)
        
        if let string1 = lobeListJSON.rawString() {
            //Do something you want
            if let string2 = suggestionListJSON.rawString() {
                //Do something you want
                let functionName:String = "JavaCallUpdateLobeLists"
                self.delegate?.genericFunctionCall(functionName, params: "'\(replaceBadChar(string1))',''")
            }
        }else{
            print("FAILURE PUSHING LOBE LISTS TO WEB")
        }
    }
    func replaceBadChar(str:String) -> String  {
        return str.replace("\'", withString: "\\\'").replace("\"", withString: "\\\"").replace("\n", withString: "\\n").replace("\r", withString: "")
        
        //        return str.replace(/\n/g, "\\\\n").replace(/\/g, "\\\\r").replace(/ t/g, "\\\\t");
    }
    // (2)
    func stopListeningStream()
    {
        let functionName:String  = "JavaCallStopListeningStream";
        self.delegate?.genericFunctionCall(functionName,params:"null" );
    }
    
    // (2,3)
    func newUploadChunkAvailable(track:TrackInfo, start_time_stamp:UInt64 ,music_delay:Int )
    {
        let functionName:String  = "JavaCallNewUploadChunkAvailable";
        self.delegate?.genericFunctionCall(functionName, params: "\(track)")
    }
    
    //DJ (3)
    func resumeEvent()
    {
        let functionName:String  = "JavaCallResumeEvent";
        self.delegate?.genericFunctionCall(functionName, params:"null");
    }
    
    //DJ (3)
    func pauseEvent()
    {
        let functionName:String  = "JavaCallPauseEvent";
        self.delegate?.genericFunctionCall(functionName, params: "null");
    }
    
    // Debug 
    func debugPing(pingType:String, value:Int )
    {
        let functionName:String  = "JavaCallDebugPing";
        self.delegate?.genericFunctionCall(functionName,  params: "\(pingType),\(value)");
    }
    
    func viewMyProfile()
    {
        let functionName:String  = "JavaCallViewMyProfile";
        self.delegate?.genericFunctionCall(functionName, params: "null")
    }
    func setTrackToUpload(track:TrackInfo)
    {
        let letTrackJson = AUDIO_OUT_MANAGER.trackInfoToJSON(track)
        if let string1 = letTrackJson.rawString() {
            
            let functionName:String  = "JavaCallSetTrackToUpload";
            self.delegate?.genericFunctionCall(functionName, params: "'\( replaceBadChar(string1))'")
        }else{
            print("FAILURE SEND UPLOAD TO WEB")
        }
    }
    
    func triggerDirectPlayTrack(track:TrackInfo)
    {
        let letTrackJson = AUDIO_OUT_MANAGER.trackInfoToJSON(track)
        if let string1 = letTrackJson.rawString() {
            
            let functionName:String  = "JavaCallTriggerDirectPlayTrack";
            self.delegate?.genericFunctionCall(functionName, params: "'\( replaceBadChar(string1))'")
        }else{
            print("FAILURE SEND UPLOAD TO WEB")
        }
    }
    
}

//Social song management & Control Calls (for DJ and Streaming mode)
extension NativeToWebHelper {
    
    // MARK: - Social playback management
    // new
    func triggerPreviousTrackEvent()
    {
        let functionName = "JavaCallTriggerPreviousTrackEvent";
        self.delegate?.genericFunctionCall(functionName, params: "null")
    }
    
    // new
    func triggerNextTrackEvent()
    {
        let functionName = "JavaCallTriggerNextTrackEvent";
        self.delegate?.genericFunctionCall(functionName, params: "null")
    }
    
    // new
    func setIsPlayingMediaContentAllowed(isAllowed:Bool)
    {
        let functionName = "JavaCallSetIsPlayingMediaContentAllowed";
        self.delegate?.genericFunctionCall(functionName, params: "\(isAllowed)")
    }
    
    
    //MARK: - Social song voting system
    // new
    func promoteTrackInLobeList(trackPosIndex:Int, trackUid:String)
    {
        let functionName = "JavaCallPromoteTrackInLobeList";
        self.delegate?.genericFunctionCall(functionName, params: "\(trackPosIndex),'\(trackUid)'")
    }
    
    // new
    func dislikeTrackInLobeList(trackPosIndex:Int, trackUid:String)
    {
        let functionName = "JavaCallDislikeTrackInLobeList";
        self.delegate?.genericFunctionCall(functionName, params: "\(trackPosIndex),'\(trackUid)'")
    }
    
    // new
    func voteChangeCurrentTrack()
    {
        let functionName = "JavaCallVoteChangeCurrentTrack";
        self.delegate?.genericFunctionCall(functionName, params: "null")

    }
    
    // MARK: - LobeList song list management
    // new
    func deleteTrackFromLobeList(trackUid:String )
    {
        let functionName = "JavaCallDeleteTrackFromLobeList";
        self.delegate?.genericFunctionCall(functionName, params: "'\(trackUid)'")
    }
    
    func refreshMediaContentPlaybackState(){
        let functionName = "refreshMediaContentPlaybackState";
        self.delegate?.genericFunctionCall(functionName, params: "null")
    }

}

extension String
{
    func replace(target: String, withString: String) -> String
    {
        return self.stringByReplacingOccurrencesOfString(target, withString: withString, options: NSStringCompareOptions.LiteralSearch, range: nil)
    }
    
    func replaceRegex(targetString: String) ->String {
        if let regex = try? NSRegularExpression(pattern: "[^a-zA-Z0-9_-]", options: .CaseInsensitive) {
            let modString = regex.stringByReplacingMatchesInString(targetString, options: .WithTransparentBounds, range: NSMakeRange(0, targetString.characters.count), withTemplate: "")
            return modString
        }
        return ""
    }
    
}
