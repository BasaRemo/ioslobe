//
//  WebToNativeHelper.swift
//  Lobe
//
//  Created by Professional on 2016-01-05.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import Foundation
import SwiftyJSON
import MediaPlayer
import AVFoundation
import AudioToolbox
import JLToast
import Parse
import SVProgressHUD

let WebToNativeCalls = WebToNative()
let DEBUG = false
struct WebToNative {
    
    var callList = ["testCall","showToastMessage","setTimeDiffWithFirebase",
        "toJavaCallSetAudioSourceMode","playNotifSound","setIsSignedInState",
        "setMyPersonalInfo","setLobeList","setSuggestionList",
        "backToNormal","doSynchoFutureEventTest","startStreamingFromUrl",
        "resumeMusic","pauseMusic","toggleDrawerMenuClick","groupUploadRequestReceived","addTrackUniqueToLobeList","createAllLocalFileChunks","uploadFileToServer","logFromSocial"
    ]
    
    let testCall = "testCall"
    let showToastMessage = "showToastMessage"
    let setTimeDiffWithFirebase = "setTimeDiffWithFirebase"
    let toJavaCallSetAudioSourceMode = "toJavaCallSetAudioSourceMode"
    let playNotifSound = "playNotifSound"
    let setIsSignedInState = "setIsSignedInState"
    let setMyPersonalInfo = "setMyPersonalInfo"
    let setLobeList = "setLobeList"
    let setSuggestionList = "setSuggestionList"
    let backToNormal = "backToNormal"
    let doSynchoFutureEventTest = "doSynchoFutureEventTest"
    let startStreamingFromUrl = "startStreamingFromUrl"
    let resumeMusic = "resumeMusic"
    let pauseMusic = "pauseMusic"
    let toggleDrawerMenuClick = "toggleDrawerMenuClick"
    let groupUploadRequestReceived = "groupUploadRequestReceived"
    let addTrackUniqueToLobeList = "addTrackUniqueToLobeList"
    let createAllLocalFileChunks = "createAllLocalFileChunks"
    let uploadFileToServer = "uploadFileToServer"
    
    let logFromSocial = "logFromSocial"
    
}

class WebToNativeHelper: NSObject {
    
    static let sharedInstance = WebToNativeHelper()
    
//    let streamingManager_sharedInstance = StreamingManager.sharedInstance()
//    let uploadManager_sharedInstance = UploadManager.sharedInstance()
    
    private override init() {
        super.init()
    }
    
    func goToViewFromBackAction(backToViewString:String){
        
        var magicState:MagicButtonViewState!
        switch (backToViewString) {
            case "localMusicView":
                magicState = .localMusicView
            case "groupView":
                magicState = .groupView
            case "lobeLiveView":
                magicState = .lobeLiveView
            case "myProfileView":
                magicState = .myProfileView
            case "findLobersView":
                magicState = .findLobersView
            default:
                break
        }
        LobeStateHelper.sharedInstance.magicButtonState.goToState(nextState: magicState)
    }
    
    // TODO: -  Nabil : Fonction a utiliser
    func createAllLocalFileChunks(trackInfo:JSON, nbSecondsPerChunk:Int){
        //createAllLocalFileChunks:(NSString*)persistentID nbSecondsPerChunk:(double)nbSecondsPerChunk
        let trackInfo = TrackInfo(trackInfoJson: trackInfo)

        let localPath = trackInfo.mCurrChunkLocalPath
        print("localPath: \(localPath)")
        let localPathArr = localPath.componentsSeparatedByString("id=")
        let persistantID = localPathArr[1] // must be 3839944631263754183 for exemple
        
        print("SongID: \(persistantID)")
        
        uploadManager_sharedInstance.createAllLocalFileChunks(persistantID, nbSecondsPerChunk: UInt32(nbSecondsPerChunk))
        // TODO: Make your calls here. I already create the trackInfo. You can just use it as needed
    }
    
    // TODO: -  Nabil : Fonction a utiliser
    func uploadFileToServer(chunkIndex:UInt32, localFilePath:String,correlationID:String, chunkStartTimeServer:Int64){
//        print("LOCAL PATH: \(localFilePath)")
        uploadManager_sharedInstance.startUploadingWithURL(chunkIndex, url: localFilePath, correlationID: correlationID, chunkStartTimeServer:chunkStartTimeServer);
    }
    
    func toggleDrawerMenuClick(){
        AUDIO_OUT_MANAGER.showMenu()
    }
    //Unused for now
    func groupUploadRequestReceived(trackObjStr: JSON){
        
        let trackInfo = TrackInfo(trackInfoJson: trackObjStr)
        //        let localPath = trackInfo.mCurrChunkLocalPath
        //        let localPathArr = localPath.componentsSeparatedByString("id=")
        //        let persistantID = localPathArr[1] // must be 3839944631263754183 for exemple
        //TODO: - NABIL This needs to be a trackInfo with and we do the upload using the local path
        //"track_chunk_local_path": "ipod-library://item/item.mp3?id=3839944631263754183"
        
        let persistantID = trackInfo.mID
        print("persistant ID: \(persistantID)")
        let musicIdentifier = NSNumberFormatter().numberFromString(persistantID);
        let query = MPMediaQuery(filterPredicates: NSSet(object:MPMediaPropertyPredicate(value: musicIdentifier, forProperty: MPMediaItemPropertyPersistentID)) as! Set<MPMediaPredicate>)
        let itemToUpload = (query.items?.first)! as MPMediaItem
//        AUDIO_OUT_MANAGER.uploadCurrentSong(itemToUpload)
        
    }
    
    func addTrackUniqueToLobeList(trackJsonObjString:JSON){
        
        let lockQueue = dispatch_queue_create("lobeList.LockQueue", nil)
        dispatch_sync(lockQueue) {
            print("//=================== Add unique track to Lobelist=====================//")
            var songItem = SongItem(mTrackInfo: TrackInfo(trackInfoJson: trackJsonObjString))
            // TODO: - MAKE SURE THE ITEM IS UNIQUE BEFORE Add song item to lobeList
            if (!AUDIO_OUT_MANAGER.lobeListContains(songItem)){
                 AUDIO_OUT_MANAGER.lobeListSongItems.addObject(songItem)
                AUDIO_OUT_MANAGER.lobeListObservableList.replaceRange(0...0, with: [SongItem()])
                print("Lobelist count: \(AUDIO_OUT_MANAGER.lobeListSongItems.count)")
                print("Lobelist observable count: \(AUDIO_OUT_MANAGER.lobeListObservableList.count)")
                print("//=================== END Adding item to LobeList =====================//")
            }
        }
            
    }
    
    
    func setLobeList(jsonArrayString:JSON){
        
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {

        let lockQueue = dispatch_queue_create("lobeList.LockQueue", nil)
        dispatch_sync(lockQueue) {
            print("//=================== Updating LobeList in background =====================//")
            let lobeList:NSMutableArray = NSMutableArray()
            //        print("func setLobeList jsonArrayString: \(jsonArrayString)")
            for (index,subJson):(String, JSON) in jsonArrayString[0] {
                //            print("subJson: \(subJson)")
                let trackInfo = TrackInfo(trackInfoJson: subJson)
                let songItem = SongItem(mTrackInfo: trackInfo)
                lobeList.addObject(songItem)
            }
            
            AUDIO_OUT_MANAGER.lobeListSongItems.removeAllObjects()
            AUDIO_OUT_MANAGER.lobeListSongItems.addObjectsFromArray(lobeList as [AnyObject])
            AUDIO_OUT_MANAGER.lobeListObservableList.replaceRange(0...0, with: [SongItem()])
            
            print("lobeListCount SERVER: \(lobeList.count)")
            print("lobeListCount: \(AUDIO_OUT_MANAGER.lobeListSongItems.count)")
            print("Lobelist observable count: \(AUDIO_OUT_MANAGER.lobeListObservableList.count)")
            print("//=================== END Updating LobeList =====================//")
        }
//        }
    }
    
    //TODO:- Clean unused
    func setSuggestionList(jsonObjListString:JSON){
        
        var suggestList = [TrackInfo]()
        for (key,subJson):(String, JSON) in jsonObjListString[0] {
//            print("subJson id \(key): \(subJson)")
            let track = TrackInfo(trackInfoJson: subJson)
            suggestList.append(track)
        }
//        AUDIO_OUT_MANAGER.suggestionList.removeAll()
//        AUDIO_OUT_MANAGER.suggestionList = suggestList

    }
    
    func setUnreadMsgsCount(count:Int){
        //        mUnreadMsgsCount = count;
        //        LobeStateHelper.sharedInstance.notifyUserStateChange()
        
    }
    
    func testCall(testString:String)
    {
        dispatch_async(dispatch_get_main_queue()) {
            JLToast.makeText(testString).show()
        }
    }
    
    
    func showToastMessage(msg:String)
    {
        dispatch_async(dispatch_get_main_queue()) {
            JLToast.makeText(msg).show()
        }
    }
    
    
    func setTimeDiffWithFirebase(jsCurrUnixTime:Int64, deltaF_JS:Int64) {
        
        let iosCurrentUnixTime:Int64 = streamingManager_sharedInstance.getCurrentUnixTimeInMs()
        let delta_JS_IOS = jsCurrUnixTime - iosCurrentUnixTime
        AUDIO_OUT_MANAGER.mDeltaTimeFirebaseIOS = deltaF_JS; //Used for sync
        
        print("jsCurrUnixTime: \(jsCurrUnixTime)")
        print("iosCurrentUnixTime: \(iosCurrentUnixTime)")
        print("delta_JS_IOS: \(delta_JS_IOS)")
        print("AUDIO_OUT_MANAGER.mDeltaTimeFirebaseIOS: \(AUDIO_OUT_MANAGER.mDeltaTimeFirebaseIOS)")
    }
    
    
    func toJavaCallSetAudioSourceMode(mode:String)
    {
        dispatch_async(dispatch_get_main_queue()) {
            switch (mode) {
            case "broadcasting":
                LobeStateHelper.sharedInstance.state = LobeState.DJ
                break
            case "listening":
                LobeStateHelper.sharedInstance.state = LobeState.Participant
                break
            case "idle":
                self.backToNormal();
            case "none","local": break
            default:
                self.backToNormal();
                break;
            }
        }
    }
    
    
    func playNotifSound(notifType:String)
    {
        switch (notifType) {
        case "close":
            let systemSoundID: SystemSoundID = 1114
            AudioServicesPlaySystemSound (systemSoundID)
            break;
        case "msg":
            let systemSoundID: SystemSoundID = 1003
            AudioServicesPlaySystemSound (systemSoundID)
            break;
        case "tock":
            let systemSoundID: SystemSoundID = 1104
            AudioServicesPlaySystemSound (systemSoundID)
            break;
        default:
            let systemSoundID: SystemSoundID = 1016
            AudioServicesPlaySystemSound (systemSoundID)
            
        }
    }
    
    
    func setIsSignedInState(isSignedIn: Bool)
    {
        LobeStateHelper.currentUser.isSignedIn = true
        LobeStateHelper.notifyUserStateChange()
    }
    
    
    func setMyPersonalInfo(id:String, name:String, picUrl:String)
    {
        LobeStateHelper.currentUser = User(mName: name, mParseId: id, mPicUrl: picUrl)
        
        //Set user for push notifications
        let query = PFUser.query()
        query!.getObjectInBackgroundWithId(id) {
            (user: PFObject?, error: NSError?) -> Void in
            if error == nil {
                print("I query User \(user)")
                //Subscribe user to push notifs
                let installation = PFInstallation.currentInstallation()
                installation["user"] = user
                installation["user_id"] = user?.objectId
                installation.saveInBackground()
            } else {
                print(error)
            }
        }
        
        if let _ = LobeStateHelper.sharedInstance.pushNotif {
            print("sending push notif to web")
            NativeToWebHelper.sharedInstance.JavaCallPushNotifActionReceived()
        }
        LobeStateHelper.notifyUserStateChange()
        
    }
    
    func backToNormal(){
        LobeStateHelper.sharedInstance.state = LobeState.Local
    }
    
    
    func doSynchoFutureEventTest( time_stamp:UInt64, music_start_time_delay:UInt64)
    {
        //        ((MainActivity)getActivity()).getAudioManager().SynchoFutureEventTest(time_stamp, music_start_time_delay);
    }
    
    
    func startStreamingFromUrl(trackObjStr:JSON, server_time_stamp:Int64, music_start_time_delay:Int64){
        print("Start streaming song")
//            dispatch_async(dispatch_get_main_queue()) {
//                SVProgressHUD.dismiss()
//            }
            //TODO: - Nabil appel la fonction de stream
            let trackInfo = TrackInfo(trackInfoJson: trackObjStr)
            AUDIO_OUT_MANAGER.currentSong = SongItem(mTrackInfo: trackInfo)
            
            var musicStartTimeInUnixTIme:Int64 = server_time_stamp -  AUDIO_OUT_MANAGER.mDeltaTimeFirebaseIOS + music_start_time_delay
            let persistantID = trackInfo.mID
            let localPath = trackInfo.mCurrChunkLocalPath
            #if DEBUG
                print("server_time_stamp: \(server_time_stamp)")
                print("music_start_time_delay: \(music_start_time_delay)")
                print("mDeltaTimeFirebaseIOS: \(AUDIO_OUT_MANAGER.mDeltaTimeFirebaseIOS)")
                
                print("musicStartTimeInUnixTIme: \(musicStartTimeInUnixTIme)")
                print("persistant ID: \(persistantID)")
                print("LOCAL DEVICE ID: \(unique_device_id)")
                print("SONG DEVICE ID: \(trackInfo.mUniqueDeviceId)")
            #endif
    
            //=============================================================================
            // TODO: - IMPORTANT: Ne pas utiliser un Index pour joueur la chanson courante , voir la fonction de download du streaming_manager
            //=============================================================================
            if trackInfo.mUniqueDeviceId == unique_device_id {
                print("MY LOCAL SONG")
                
                streamingManager_sharedInstance.downloadChuncksFromUrl(trackInfo.mCurrChunkUrl, localPath:localPath , correlationID:trackInfo.genCorrolationID(), musicStartTimeInUnixTime:musicStartTimeInUnixTIme,isLocalSong:true)
                
            }else{
                if !trackInfo.mIsYoutubeVideo {
                    print("Other user SONG")
                    print("song url: \(trackInfo.mCurrChunkUrl)")
                    AUDIO_OUT_MANAGER.resumeStreamingAudio()
                    streamingManager_sharedInstance.downloadChuncksFromUrl(trackInfo.mCurrChunkUrl, localPath:localPath, correlationID:trackInfo.genCorrolationID(), musicStartTimeInUnixTime:musicStartTimeInUnixTIme,isLocalSong:false)
                }else{
                    print("YOUTUBE SONG")
                    AUDIO_OUT_MANAGER.stopStreamingAudio()
                }
            }
        
            AUDIO_OUT_MANAGER.isGroupMusicPlaying = true
    }
    
    
    func resumeMusic(){
        //Resume stream Music
//        AUDIO_OUT_MANAGER.playMusic()
    }
    
    
    func pauseMusic(){
        AUDIO_OUT_MANAGER.pauseMusic()
    }
    
}
