
//
//  DownloadHelper.swift
//  Lobe
//
//  Created by Professional on 2016-01-17.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit
import Haneke
import AlamofireImage
import Toucan

typealias DownloadHelperCallback = (UIImage?, NSError?) -> Void

class DownloadHelper: NSObject {

    static let sharedInstance = DownloadHelper()
    
    private override init() {
    }
    static func cropImage(image:UIImage, withWidth width:Int, andHeigth heigth:Int) -> UIImage{
        
        return Toucan(image: image).resize(CGSize(width: width, height: heigth), fitMode: Toucan.Resize.FitMode.Crop).image
    }
    static func downloadImage(){

    }
    static func downloadImage(imgUrl:String, callback: DownloadHelperCallback){
        
        let cache = Shared.imageCache
        
        let iconFormat = Format<UIImage>(name: "icons", diskCapacity: 1 * 1024 * 1024) { image in
            return image
        }
        cache.addFormat(iconFormat)
        
        let URL = NSURL(string: imgUrl)!
        cache.fetch(URL: URL, formatName: "icons").onSuccess { image in

            return callback(image,nil)
        }
    }
    
    static func downloadImage(fromUrl imgUrl:String, withSize size:Int, completion callback: DownloadHelperCallback){
        
        let downloader = ImageDownloader()
        let URLRequest = NSURLRequest(URL: NSURL(string: imgUrl)!)
        let filter = AspectScaledToFillSizeCircleFilter(size: CGSize(width: size, height: size))
        
        downloader.downloadImage(URLRequest: URLRequest, filter: filter) { response in
            
            print(response.request)
            print(response.response)
            debugPrint(response.result)
            
            if let image = response.result.value {
                return callback(image,nil)
            }
        }
    }
}
