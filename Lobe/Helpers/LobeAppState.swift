//
//  LobeAppState.swift
//  Lobe
//
//  Created by Professional on 2016-02-20.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit

let streamingManager_sharedInstance = StreamingManager.sharedInstance()
let uploadManager_sharedInstance = UploadManager.sharedInstance()

class LobeAppState: NSObject {

    //MARK:- audio playback
    final func pauseTrack(){
        
        //We do the pause everywhere to be safe
        // Local Pause
        if AUDIO_OUT_MANAGER.isPlaying {
            AUDIO_OUT_MANAGER.player.pause()
            AUDIO_OUT_MANAGER.isPlaying = false
            AUDIO_OUT_MANAGER.notifyMusicChange(false)
        }
        //Group playing state
        AUDIO_OUT_MANAGER.isGroupMusicPlaying = false
        //TODO:- AudioQueue Pause : trigger the pause for AudioQueue : Make sure sync doesn't stop
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            streamingManager_sharedInstance.pauseAudioQueue()
        }
        
        //Notify real audio playing state to Social (Javascript)
        NativeToWebHelper.sharedInstance.setIsPlayingMediaContentAllowed(false)
        AUDIO_OUT_MANAGER.notifyMusicChange(false)
        
    }

    func stopSocialAudio(){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            social_slider_value.value = 0.0
            AUDIO_OUT_MANAGER.lobeListSongItems.removeAllObjects()
            AUDIO_OUT_MANAGER.lobeListObservableList.replaceRange(0...0, with: [SongItem()])
            streamingManager_sharedInstance.pauseAudioQueue()
            AUDIO_OUT_MANAGER.isGroupMusicPlaying = false
        }
    }
    
    func stopStreamingAudio(){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            social_slider_value.value = 0.0
            streamingManager_sharedInstance.pauseAudioQueue()
        }
    }
    
    func resumeStreamingAudio(){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            streamingManager_sharedInstance.resumeAudioQueue()
        }
    }
    
    //Ovverrided functions
    func playTrack(){
        preconditionFailure("LobeAppState playTrack() : This method must be overridden")
    }
    func resumeTrack(){
        preconditionFailure("LobeAppState resumeTrack(): This method must be overridden")
    }
    func isTrackPlaying() -> Bool {
        preconditionFailure("LobeAppState isTrackPLaying() : This method must be overridden")
    }
    func directPlayTrack(){
        preconditionFailure("LobeAppState directPlayTrack() : This method must be overridden")
    }
    func skipToNextMusic(){
        preconditionFailure("LobeAppState directPlayTrack() : This method must be overridden")
    }
    func skipToPreviousMusic(){
        preconditionFailure("LobeAppState directPlayTrack() : This method must be overridden")
    }
    func skipToBeginning(){
        preconditionFailure("LobeAppState directPlayTrack() : This method must be overridden")
    }
}
