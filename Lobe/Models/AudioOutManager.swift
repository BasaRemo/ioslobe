//
//  AudioOutManager.swift
//  Lobe
//
//  Created by Professional on 2015-12-12.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import Foundation
import MediaPlayer
import SwiftyJSON
import Alamofire
import WebKit
import Chronos
import Dollar
import Bond

enum MusicPlayerState :String {
    case Library = "Library"
    case LobeList = "LobeList"
    //    case Streaming = "Streaming"
    
    private init () {
        self = .Library
    }
}

let AUDIO_OUT_MANAGER = AudioOutManager()

class AudioOutManager:NSObject {
    
    let player = MPMusicPlayerController.applicationMusicPlayer()
   var isAudioPlaying: Observable<Bool> = Observable<Bool>(false)
    var isPlaying = false {
        didSet{
            if !isGroupMusicPlaying && !isPlaying{
                isAudioPlaying.value = true
            }else{
                isAudioPlaying.value = false
            }
        }
    }
    
    //TODO: - Social playback variables
    var isGroupMusicPlaying = false {
        didSet{
            if !isGroupMusicPlaying && !isPlaying{
                isAudioPlaying.value = true
            }else{
                isAudioPlaying.value = false
            }
            
        }
    }

    var socialVotingMapListChangeObserver: Observable<Bool> = Observable<Bool>(false)
    var socialVotingMapList:JSON?{
        didSet{
            socialVotingMapListChangeObserver.value = !socialVotingMapListChangeObserver.value
//            AUDIO_OUT_MANAGER.lobeListObservableList.replaceRange(0...0, with: [SongItem()])
        }
    }
    
    var initialLoading = true
    var musicListsHaveChanged = false
    var currentPlayingSongTimer = NSTimer()
    var mainVC:MainVC!
    var mDeltaTimeFirebaseIOS:Int64 = 0
    
    var lobeAppState:LobeAppState = LocalState.getSharedInstance()
    
    var musicPlayerState = MusicPlayerState() {
        didSet{
            print("player Owner moved from \(oldValue) to \(musicPlayerState)")
            notifyMusicChange(false)
        }
    }
    
    var currentTrackObservable: Observable<SongItem> = Observable<SongItem>(SongItem())
    var currentSong = SongItem() {
        didSet {
//            guard LobeStateHelper.sharedInstance.state == .Local else{
//                return
//            }
            currentTrackObservable.value = currentSong
        }
    }
    
//    var currentSocialSong:SongItem? {
//        didSet {
//            notifyMusicChange(true)
//        }
//    }
    var savedCurrentLocalSong = SongItem()
    var lobeListSongItems:NSMutableArray = NSMutableArray(){
        didSet {
            notifyMusicChange(false)
            musicListsHaveChanged = true
        }
    }
    var lobeListObservableList: ObservableArray<SongItem> = ObservableArray([SongItem()])
    var localObservableTrackList: ObservableArray<SongItem> = ObservableArray([])
    var localSongItemList:NSMutableArray = NSMutableArray()
    var currentLocalSongIndex = 0
    var currentLobeListSongIndex = 0
    
    func lobeListContains(songItem:SongItem) -> Bool{
        
        if AUDIO_OUT_MANAGER.lobeListSongItems.count > 0 {
            
            return AUDIO_OUT_MANAGER.lobeListSongItems.filter({ $0.trackInfo.mID == songItem.trackInfo.mID }).count > 0
        }
        return false
    }
    
    func localListContains(songItem:SongItem) -> Bool{
        
        guard AUDIO_OUT_MANAGER.localObservableTrackList.count > 0 else {
            return false
        }
        return AUDIO_OUT_MANAGER.localObservableTrackList.filter({ $0.trackInfo.mID == songItem.trackInfo.mID }).count > 0
    }
    
    func findLocalSong(songItem:SongItem) -> [SongItem]{
        
        guard localObservableTrackList.count > 0 else {
            return []
        }
        let resultArr = localObservableTrackList.filter({ $0.trackInfo.mID == songItem.trackInfo.mID })
        return resultArr as! [SongItem]
    }
    
    func getIndexOfLocalSongItem(songItem:SongItem) -> Int{
        
        let result = $.findIndex(localSongItemList as! [SongItem]) { $0.trackInfo.mID == songItem.trackInfo.mID }
        //        print("current Song Index : \(result!)")
        return result!
    }
    
    func getIndexOfLobeListSongItem(songItem:SongItem) -> Int{

        let result = $.findIndex(lobeListSongItems as! [SongItem]) { $0.trackInfo.mID == songItem.trackInfo.mID }
//        print("current Song Index : \(result!)")
        return result!
    }

    
    func getIndexOfCurrentPlayingLocalTrack() -> Int {
        let currentSong = AUDIO_OUT_MANAGER.currentSong
        return getIndexOfLocalSongItem(currentSong)
    }
    
    func getIndexOfCurrentPlayingLobeListTrack() -> Int {
        let currentSong = AUDIO_OUT_MANAGER.currentSong
        return getIndexOfLobeListSongItem(currentSong)
    }
    
    func userDidLeaveGroup(){
        
        AUDIO_OUT_MANAGER.player.pause()
        AUDIO_OUT_MANAGER.stopSocialAudio()
        currentLobeListSongIndex = 0
//        currentLocalSongIndex = 0
    }
    func saveLocalMusicState(){
        savedCurrentLocalSong = currentSong
    }
    func retrieveLocalMusicState(){
        
        currentSong = savedCurrentLocalSong
    }
    var timer = DispatchTimer(interval: 0.25, closure: {
        (timer: RepeatingTimer, count: Int) in
        print("Execute repeating task here")
    })
    
    override init() {
        super.init()
        
        player.beginGeneratingPlaybackNotifications()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("playBackStateDidChange"), name: MPMusicPlayerControllerPlaybackStateDidChangeNotification, object: player)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("nowPlayingMusicChanged"), name: MPMusicPlayerControllerNowPlayingItemDidChangeNotification, object: player)
        
        /** Starting the timer */
//        timer.start(true)
    }
    
    func nowPlayingMusicChanged(){
//        print("nowPlayingMusicChanged")
        //FIXME: - This call crashed 
        currentSong = SongItem(mMediaItem: player.nowPlayingItem!)
        notifyMusicChange(true)
    }
    func playerDidFinish(){
        notifyMusicChange(true)
    }
    func playBackStateDidChange(){
        
    }
    func addMainViewInstance(mainview:MainVC){
        self.mainVC = mainview
    }
    
    func showMenu(){
        print("Show Menu")
        dispatch_async(dispatch_get_main_queue()) {
            // update some UI
            self.mainVC.menuButtonPressed(self)
        }
    }
    
}

//MARK: - Music Player Playback
extension AudioOutManager{
    
    func playMusic(){
        lobeAppState.playTrack()
    }
    func directPlayMusic(){
        lobeAppState.directPlayTrack()
    }
    func pauseMusic(){
        lobeAppState.pauseTrack()
    }
    func skipToNextMusic(){
        lobeAppState.skipToNextMusic()
    }
    func skipToPreviousMusic(){
        lobeAppState.skipToPreviousMusic()
    }
    func skipToBeginning(){
        lobeAppState.skipToBeginning()
    }
    func isMusicPlaying() -> Bool{
        return lobeAppState.isTrackPlaying()
    }
    func stopSocialAudio(){
        lobeAppState.stopSocialAudio()
    }
    func stopStreamingAudio(){
        lobeAppState.stopStreamingAudio()
    }
    
    func resumeStreamingAudio(){
        lobeAppState.resumeStreamingAudio()
    }
}

//MARK: - App State Management
extension AudioOutManager{
    
    func musicListsDidChange(){
        musicListsHaveChanged = true
        notifyMusicChange(false)
    }
    
    func lobeStateChanged(){
        
//        switch LobeStateHelper.sharedInstance.state {
//        case .Local, .Participant:
//            self.cancelTimer()
//        case .DJ:
//            self.startTimer()
//        }
    }
}

//MARK: - Notification Observer Management
extension AudioOutManager{
    
    func observeMusicChange(viewController: AnyObject){
        NSNotificationCenter.defaultCenter().addObserver(viewController, selector: Selector("actOnMusicChange:"), name: musicNotificationKey, object: nil)
    }
    
    func notifyMusicChange(songChanged:Bool) {
        NSNotificationCenter.defaultCenter().postNotificationName(musicNotificationKey, object: nil,userInfo:["songChanged":songChanged])
    }
    
    func observeLobeListChange(viewController: AnyObject){
        NSNotificationCenter.defaultCenter().addObserver(viewController, selector: Selector("actOnLobeListChange"), name: lobeListNotificationKey, object: nil)
    }
    
    func notifyLobeListChange() {
        NSNotificationCenter.defaultCenter().postNotificationName(lobeListNotificationKey, object: nil)
    }
}

//MARK: - JSON Encoding
extension AudioOutManager{
    
    func trackInfoToJSON(trackInfo:TrackInfo) -> JSON {
        return JSON(trackInfo.toDictionary())
    }
    
    func musicListToJSONArrayData(musicList: [TrackInfo]) -> JSON {
        
        var dictPTracks:[JSON] = []
        var dictionary = [[String:AnyObject]]()
        
        for track in musicList{
            dictPTracks.append(JSON(track.toDictionary()))
            dictionary.append(track.toDictionary())
//            print("Track: \(track.toDictionary())")
        }
//        print("valid JSON: \(dictPTracks)")
        let dataDict  = NSMutableDictionary()
        dataDict.setValue(dictionary, forKey: "data")
        
        return JSON(dataDict)
    }

    func musicListToJSONObjectData(musicList: [TrackInfo]) -> JSON{
        
        let trackJsonDict :NSMutableDictionary = [:]
        for track in musicList{
            trackJsonDict.setValue(track.toDictionary(), forKey: "\(track.mUnixTimeStampAtUserPush)")
        }
        
        let dataDict:NSMutableDictionary = [:]
        dataDict.setValue(trackJsonDict, forKey: "data")
        return JSON(dataDict)
    }
    
    func chunkInfoListToJSONArrayData(musicList: [ChunkInfo]) -> JSON {
        
//        var dictPTracks:[JSON] = []
        var dictionary = [[String:AnyObject]]()
        
        for track in musicList{
//            dictPTracks.append(JSON(track.toDictionary()))
            dictionary.append(track.toDictionary())
//            print("Track: \(track.toDictionary())")
        }
        
//        print("valid JSON: \(dictPTracks)")
        let dataDict  = NSMutableDictionary()
        dataDict.setValue(dictionary, forKey: "data")
        
        return JSON(dataDict)
    }
    
//    func musicListToJSONArray(musicList: [TrackInfo]) -> [JSON] {
//
//        var dictPTracks:[JSON] = []
//        for track in musicList{
//            dictPTracks.append(JSON(track.toDictionary()))
//            print("Track: \(track.toDictionary())")
//        }
//        print("valid JSON: \(dictPTracks)")
//
//        return dictPTracks
//    }

//    func musicListToJSONObject(musicList: [TrackInfo]) -> JSON{
//
//        let trackJsonDict :NSMutableDictionary = [:]
//        for track in musicList{
//            trackJsonDict.setValue(track.toDictionary(), forKey: "\(track.mUnixTimeStampAtUserPush)")
//        }
//        //        print("valid DICT: \(trackJsonDict))")
//        //        print("valid JSON: \(JSON(trackJsonDict)))")
//        
//        return JSON(trackJsonDict)
//    }
}

extension AudioOutManager {
    
    func createUploadDirectory(){
        let fileManager = NSFileManager.defaultManager()
        
        let dirPaths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,
            .UserDomainMask, true)
        let docsDir = dirPaths[0]
        let newDir = docsDir + "/uploadFolder"
        var isDir : ObjCBool = false
        
        guard !fileManager.fileExistsAtPath(newDir, isDirectory:&isDir) else{
            print("Folder Already exist")
            return
        }
//        guard !isDir else {
//            print("Path is not a directory")
//            return
//        }
        
        do {
            try fileManager.createDirectoryAtPath(newDir, withIntermediateDirectories: true, attributes: nil)
            print("Success creating folder")
        } catch {
            print("Could not create upload folder: \(error)")
        }
    }
    
    func deleteUploadDirectory(){
        
        let fileManager = NSFileManager.defaultManager()
        let dirPaths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,
            .UserDomainMask, true)
        let docsDir = dirPaths[0]
        let newDir = docsDir + "/uploadFolder"
        var isDir : ObjCBool = false
        
        guard fileManager.fileExistsAtPath(newDir, isDirectory:&isDir) else{
            print("Folder dont exist")
            return
        }
        guard isDir else {
            print("Path is not a directory")
            return
        }
        
        do {
            try fileManager.removeItemAtPath(newDir)
            print("Success Delete upload folder")
        } catch {
            print("Could not delete upload folder: \(error)")
        }
    }
    
    
    func resetUploadDirectory(){
        
        let fileManager = NSFileManager.defaultManager()
        let docsDir = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,
            .UserDomainMask, true)[0]
        
        let newDir = docsDir + "/uploadFolder"
        var isDir : ObjCBool = false
        
        guard fileManager.fileExistsAtPath(newDir, isDirectory:&isDir) else{
            print("Folder dont exist")
            return
        }
        guard isDir else {
            print("Path is not a directory")
            return
        }
        
        do {
            try fileManager.removeItemAtPath(newDir)
            print("Success Delete upload folder")
            createUploadDirectory()
        } catch {
            print("Could not delete upload folder: \(error)")
        }
    }
}
