//
//  User.swift
//  Lobe
//
//  Created by Professional on 2016-01-10.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit
import Bond
import Alamofire
import SwiftyJSON
import Haneke

class User: NSObject {
    
    var name:String = "unavailable"
    var email:String = "email"
    var parseID: String = "unavailable"
    var picUrl:String = "unavailable" {
        
        didSet{
            self.downloadImage(self.picUrl)
        }
    }
    
    var userImage:UIImage?
    var isSignedIn:Bool = false
    var userPic : Observable<UIImage?> = Observable<UIImage?>(nil)
    var userName: Observable<String?> = Observable<String?>("unavailable")
    var userPicUrl:Observable<String?> = Observable<String?>("unknown")
    
    override init() {
        
    }
    
    func toDictionary() -> [String: String] {
        return userToDictionary(self)
    }
    func userToDictionary(user: User) -> [String: String] {
        return [
            "name": userName.value!,
            "email": email,
            "picUrl": picUrl,
            "parseID": parseID,
        ]
        
    }
    
    init(mName:String, mParseId:String, mPicUrl:String){
        userName.value = mName
        parseID = mParseId
        userPicUrl.value = mPicUrl
        picUrl  = mPicUrl
         name = mName
    }
    
    func setPersonnalInfo(mName:String, mParseId:String, mPicUrl:String){
        userName.value = mName
        parseID = mParseId
        userPicUrl.value = mPicUrl
        picUrl  = mPicUrl
        name = mName
    }
    
    func downloadImage(){
        if let _  = userImage {
        }else{
            downloadImage(picUrl)
        }
    }
    
    func downloadCurrentUserImage(){
        if let _  = userImage {
        }else{
            downloadCurrentUserImage(picUrl)
        }
    }
    
    func downloadCurrentUserImage(imgUrl:String){
        
        //        DownloadHelper.downloadImage(fromUrl: imgUrl, withSize: 30) { (image:UIImage?, error: NSError?) -> Void in
        //            self.userPic.value = image
        //            self.userImage = image
        //            LobeStateHelper.notifyUserStateChange()
        //            AUDIO_OUT_MANAGER.notifyMusicChange(false)
        //        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            
            let cache = Shared.imageCache
            let iconFormat = Format<UIImage>(name: "icons", diskCapacity: 1 * 1024 * 1024) { image in
                return image
            }
            cache.addFormat(iconFormat)
            
            let URL = NSURL(string: imgUrl)!
            cache.fetch(URL: URL, formatName: "icons").onSuccess { image in
                
                let circularImage = image.af_imageRoundedIntoCircle()
                self.userPic.value = circularImage
                self.userImage = circularImage
                dispatch_async(dispatch_get_main_queue()) {
                    LobeStateHelper.notifyUserStateChange()
                }
            }
        }
    }
    
    func downloadImage(imgUrl:String){
        
//        DownloadHelper.downloadImage(fromUrl: imgUrl, withSize: 30) { (image:UIImage?, error: NSError?) -> Void in
//            self.userPic.value = image
//            self.userImage = image
//            LobeStateHelper.notifyUserStateChange()
//            AUDIO_OUT_MANAGER.notifyMusicChange(false)
//        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            
            let cache = Shared.imageCache
            let iconFormat = Format<UIImage>(name: "icons", diskCapacity: 1 * 1024 * 1024) { image in
                return image
            }
            cache.addFormat(iconFormat)
            
            let URL = NSURL(string: imgUrl)!
            cache.fetch(URL: URL, formatName: "icons").onSuccess { image in
                
                let circularImage = image.af_imageRoundedIntoCircle()
                self.userPic.value = circularImage
                self.userImage = circularImage
            }
        }
    }
}
