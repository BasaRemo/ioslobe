//
//  SongItem.swift
//  Lobe
//
//  Created by Professional on 2015-12-14.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import Foundation
import MediaPlayer
import Bond

class SongItem: MPMediaItem {
    
    var music:MPMediaItem?{
        didSet{
            
            if let _ = self.music {
                
                let trackID = music!.valueForProperty(MPMediaItemPropertyPodcastPersistentID) as? NSNumber
                let trackAlbumID = music!.valueForProperty(MPMediaItemPropertyAlbumPersistentID) as? NSNumber
                let trackLength = music!.valueForProperty(MPMediaItemPropertyPlaybackDuration) as? NSNumber
                let itemArtwork = music!.valueForProperty(MPMediaItemPropertyArtwork) as? MPMediaItemArtwork
                
                //Formating
                let tID = String(format:"%f", trackID!.doubleValue)
                let tAlbumID = String(format:"%f", trackAlbumID!.doubleValue)
                print("trackInfo track lenght double: \(trackLength!.doubleValue)")
                let tLength = Int(trackLength!)
                
                let tArtist = music!.valueForProperty(MPMediaItemPropertyArtist) as? String
                let tName = music!.valueForProperty(MPMediaItemPropertyTitle) as? String
                let tDisplayName = music!.valueForProperty(MPMediaItemPropertyTitle) as? String
                
                print("trackInfo track lenght: \(tLength)")
                
                trackInfo = TrackInfo(trackID: tID ?? "" , artist: tArtist ?? "" , name: tName ?? "", dataPath: "", displayName: tDisplayName ?? "", length: tLength ?? 0 , albumID: tAlbumID ?? "",user:LobeStateHelper.currentUser)
                
                if let _ = itemArtwork {
                    let artWorkImage = itemArtwork!.imageWithSize(CGSize(width: 44.0, height: 44.0))
                    trackInfo.songAlbumArt.value = artWorkImage ?? UIImage(named: placeholderImageName)
                }else{
                    trackInfo.songAlbumArt.value = UIImage(named: placeholderImageName)
                }

            }
        }
    }
    var trackInfo = TrackInfo()
    
    override init() {
        super.init()
    }
    init(mMediaItem:MPMediaItem,mTrackInfo:TrackInfo) {
        super.init()
        music = mMediaItem
        trackInfo = mTrackInfo
    }
    init(mTrackInfo:TrackInfo) {
        super.init()
        trackInfo = mTrackInfo
        
        if trackInfo.mUniqueDeviceId == unique_device_id {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                let resultArr = AUDIO_OUT_MANAGER.localSongItemList.filter({ $0.trackInfo.mID == self.trackInfo.mID })
                if resultArr.count > 0 {
                    let localSongItem = resultArr[0]
                    self.trackInfo.albulArtImage = localSongItem.trackInfo.albulArtImage
                    self.trackInfo.songAlbumArt.value = localSongItem.trackInfo.albulArtImage
                }
            }
        }
        
    }
    init(mMediaItem:MPMediaItem) {
        super.init()
        music = mMediaItem
        trackInfo = TrackInfo(music: mMediaItem)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
