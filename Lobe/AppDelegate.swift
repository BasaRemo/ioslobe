//
//  AppDelegate.swift
//  Lobe
//
//  Created by Professional on 2015-11-16.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import Parse
import Bolts
import SwiftyJSON
import AVFoundation

let THEME_COLOR:UIColor = UIColor(red: 29/255 , green: 83/255, blue: 126/255, alpha: 1.0)
let LABEL_COLOR:UIColor = UIColor.whiteColor()

let DJ_COLOR:UIColor = UIColor(red: 5/255 , green: 38/255, blue: 127/255, alpha: 0.25)
let PARTICIPANT_COLOR:UIColor = UIColor(red: 86/255 , green: 0/255, blue: 0/255, alpha: 0.25)
let CURRENT_PLAYING_COLOR:UIColor = UIColor.orangeColor()
let SELECT_FOCUS_COLOR:UIColor = UIColor(red: 255/255 , green: 255/255, blue: 255/255, alpha: 0.1)

let SEGMENT_MENU_COLOR:UIColor = UIColor(red: 0/255 , green: 0/255, blue: 0/255, alpha: 0.5)
let SEGMENT_RED_MENU_COLOR:UIColor = UIColor(red: 150/255 , green: 20/255, blue: 010/255, alpha: 0.5)
let unique_device_id = UIDevice.currentDevice().identifierForVendor!.UUIDString
let placeholderImageName = "lobe_background_3"

func backgroundThread(delay: Double = 0.0, background: (() -> Void)? = nil, completion: (() -> Void)? = nil) {
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)) {
        if(background != nil){ background!(); }
        
        let popTime = dispatch_time(DISPATCH_TIME_NOW, Int64(delay * Double(NSEC_PER_SEC)))
        dispatch_after(popTime, dispatch_get_main_queue()) {
            if(completion != nil){ completion!(); }
        }
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
//        application.statusBarHidden = true
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
//        SideMenuController.menuButtonImage = UIImage(named: "menu_icon")
        SideMenuController.presentationStyle = .UnderCenterPanelLeft
        SideMenuController.animationStyle = .FadeAnimation
        
         LocationHelper.sharedInstance.manager.requestWhenInUseAuthorization()
         LocationHelper.sharedInstance.manager.requestAlwaysAuthorization()
//        UINavigationBar.appearance().translucent = false
//        UINavigationBar.appearance().barTintColor = UIColor.clearColor()
        
        
        // Initialize Parse.
        Parse.setApplicationId("DlIbcE0Xiy5upPCDpZXneHfAD76lDBiGvwcT51ee",
            clientKey: "5BV9uurEqcEelh6cHEkR4TOrh8wB8IHfzCxPkHkc")
        
        // [Optional] Track statistics around application opens.
        PFAnalytics.trackAppOpenedWithLaunchOptions(launchOptions)
        LobeStateHelper.sharedInstance.notifyPushNotifReceived()
        let settings = UIUserNotificationSettings(forTypes: [.Alert, .Sound, .Badge], categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        UIApplication.sharedApplication().registerForRemoteNotifications()
        
        if let launchOptions = launchOptions as? [String : AnyObject] {
            if let notificationDictionary = launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey] as? [NSObject : AnyObject] {
                self.application(application, didReceiveRemoteNotification: notificationDictionary)
            }
        }
        
        //Set background audio capabilities
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            do{
                let session = AVAudioSession.sharedInstance()
                try session.setCategory(AVAudioSessionCategoryPlayback)
                try session.setActive(true)
            }
            catch{
                 print("\(error)")
            }
        }
        
        
        return true
    }

    func clearBadges() {
        
        let installation = PFInstallation.currentInstallation()
        installation.badge = 0
        installation.saveInBackgroundWithBlock { (success, error) -> Void in
            if success {
                print("cleared badges")
                UIApplication.sharedApplication().applicationIconBadgeNumber = 0
            }
            else {
                print("failed to clear badges")
            }
        }
    }
    
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        
        let installation = PFInstallation.currentInstallation()
//        installation["user"] = PFUser.currentUser()
        installation.setDeviceTokenFromData(deviceToken)
        installation.saveInBackground()
        
//        PFPush.subscribeToChannelInBackground("") { (succeeded, error) in
//            if succeeded {
//                print("ParseStarterProject successfully subscribed to push notifications on the broadcast channel.");
//            } else {
//                print("ParseStarterProject failed to subscribe to push notifications on the broadcast channel with error = %@.", error)
//            }
//        }
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        
        PFPush.handlePush(userInfo)
        let json = JSON(userInfo)
        print("json Object: \(json)")
        LobeStateHelper.sharedInstance.pushNotif = json
        
        dispatch_async(dispatch_get_main_queue()) {
            LobeStateHelper.sharedInstance.notifyPushNotifReceived()
        }
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        clearBadges()
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

