//
//  PcmAudioManager.m
//  Lobe
//
//  Created by Professional on 2016-02-17.
//  Copyright Â© 2016 Ntambwa. All rights reserved.
//

#import "StreamingManager.h"
#include <sys/time.h>
#include <pthread.h>

#define NUM_CHANNELS 2
#define NUM_BUFFERS 4
#define BUFFER_SIZE 8192 //2048 //8192 //512 //65536 //32768//4194304
#define SAMPLE_TYPE short
#define MAX_NUMBER 32767
#define SAMPLE_RATE 44100
#define FORMAT_ID 778924083

#pragma mark static object shared between C and Objective-C
static OSStatus result;

//Buffers used by the audioqueue playback
static AudioQueueBufferRef buffers[NUM_BUFFERS];

//Data buffer containing the decoded pcm data
static NSMutableData *pcmDataBuffer;

//Data buffer used by the input stream
static NSMutableData *data;

//The audioqueue playback
static AudioQueueRef queue;

//Audio Format
static AudioStreamBasicDescription format;

// Current position in the buffer where when need to start reading data
static int64_t mPlayBufferPosition;

// Positioned to the end of the buffer. For our infinite buffer, this should be the position of the last added byte in the buffer
static int64_t mPlayBufferEndPosition;

// Correlation ID used to know if song changed
static NSString* localCorrelationID;
// Used with the correlation ID to manage changing of songs
static bool currentSongChanged;

static NSTimer *syncTimer;

int64_t numSamples = 0;

static NSInputStream *inputStreamer;

static long music_start_timeInUnixTime = 0;

//Used to know whick data to get from the chunks arraylist
static int current_chunk_index;

pthread_mutex_t audioQ_mutex;
pthread_mutex_t stream_mutex;

@interface StreamingManager ()

@end

@implementation StreamingManager
// Private instance variables
{
}


#pragma mark Initalisation
//returns the Singleton object
+ (StreamingManager *)sharedInstance {
    static StreamingManager *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}

-(id)init
{
    if (self = [super init]) {
        //        NSLog(@"init timer");
        //        [syncTimer invalidate];
        //        syncTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(syncTimerTicked) userInfo:nil repeats:YES];
    }
    return self;
}

/*
 The initialize method is the class-level equivalent of init. It gives you a chance to set up the class before anyone uses it
 */
+ (void)initialize {
    if (self == [StreamingManager class]) {
        // Makes sure this isn't executed more than once
        //Initialize the buffers
        pcmDataBuffer = [[NSMutableData alloc] init];
        data = [NSMutableData data];
        
        //Setup the pcm Format
        memset(&format, 0, sizeof(AudioStreamBasicDescription));
        format.mSampleRate       = SAMPLE_RATE;
        format.mFormatID         = kAudioFormatLinearPCM;
        format.mFormatFlags      = kLinearPCMFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked;
        format.mBitsPerChannel   = 8 * sizeof(SAMPLE_TYPE); //8* 2 bytes = 16 bytes
        format.mChannelsPerFrame = NUM_CHANNELS;
        format.mBytesPerFrame    = sizeof(SAMPLE_TYPE) * NUM_CHANNELS; // 2 bytes * 2 Channels = 4 bytes
        format.mFramesPerPacket  = 1;
        format.mBytesPerPacket   = format.mBytesPerFrame * format.mFramesPerPacket; // 4 bytes* 1  = 4 bytes
        format.mReserved         = 0;
        
        mPlayBufferPosition = 0;
        mPlayBufferEndPosition = 0;
        music_start_timeInUnixTime = 0;
        current_chunk_index = -1;
        
        localCorrelationID = @"-1";
        //        localCorrelationID = [[NSString alloc]init];
        
        currentSongChanged = false;
        
        NSLog(@"Init Static Sync Timer");
        //        [syncTimer invalidate];
        //        syncTimer = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(syncTimerTicked:) userInfo:nil repeats:YES];
        //        [self activeAVAudioSessionForBackgroundAndScreenLockPlay];
        
    }
}
+(void) activeAVAudioSessionForBackgroundAndScreenLockPlay{
    
    // implicitly initializes your audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    
    NSError *activationError = nil;
    NSError *setCategoryErr = nil;
    BOOL success = [[AVAudioSession sharedInstance] setActive: YES error: &activationError];
    [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error:&setCategoryErr];
    if (!success) { /* handle the error in activationError */
        NSLog(@"ERROR Setting background audioPlayback");
    }
    
}
+(void)syncTimerTicked:(NSTimer *)timer {
    doSyncMusic();
    //    NSLog(@"sync timer ticked");
}

//Callback function used by AudioQue
//void callback(void *custom_data, AudioQueueRef queue, AudioQueueBufferRef buffer);

#pragma mark AudioQueue Setup & CallBack
int resetAudioQueue()
{
    
    mPlayBufferEndPosition = (int)pcmDataBuffer.length; //(int)pcmDataBuffer.length;//(int)pcmDataBuffer.length; //1073741824; // Arbitrary position for now
    
    NSLog(@" mPlayBufferEndPosition %u \n ",(unsigned int)mPlayBufferEndPosition);
    NSLog(@" mPlayBufferPosition %u \n ",(unsigned int)mPlayBufferPosition);
    //    NSLog(@"pcmDataBuffer length   %lu ", pcmDataBuffer.length);
    
    //Get the decoded pcm buffer (Not used in the callback)
    //    const char *decodedPcmByte = [pcmDataBuffer bytes];
    
    //Create an audioqueue output
    //        AudioQueueNewOutput(&format, callback, (void *)(decodedPcmByte), NULL, NULL, 0, &queue);
    AudioQueueNewOutput(&format, callback, NULL, NULL, NULL, 0, &queue);
    
    // Loop throught the available buffers allocate them and enqueue Data in it so that the audio playback (queue) can start reading it
    for (unsigned int i = 0; i < NUM_BUFFERS; i++)
    {
        AudioQueueAllocateBuffer(queue, BUFFER_SIZE, &buffers[i]);
        buffers[i]->mAudioDataByteSize = BUFFER_SIZE;
        AudioQueueEnqueueBuffer(queue, buffers[i], 0, NULL);
        
    }
    
    OSStatus errorMsg = AudioQueueSetParameter(queue, kAudioQueueParam_Volume, 1.0);
    
    if (errorMsg) {
        NSLog(@"AudioQueueSetParameter returned %d when setting the volume.", errorMsg);
    }
    // Start the audio queue playback (queue)
    // - NULL indicates that the player should start consuming data in the queue buffers as soon as possible
    AudioQueueStart(queue, NULL);
    //    CFRunLoopRun();
    
    return 0;
}

// Callback called by the audio queue plaback (queue) when he has an empty buffer
void callback(void * inUserData, AudioQueueRef inAQ,AudioQueueBufferRef  inCompleteAQBuffer)
{
    
    pthread_mutex_lock(&audioQ_mutex);
    mPlayBufferEndPosition = (int)pcmDataBuffer.length;
    //We use the pcm data buffer here instead of the inUserData to have a better control on the data
    //    char *byteData = [pcmDataBuffer bytes];
    //    void *mUserData = (void *)byteData;
    const void *mUserData = [pcmDataBuffer bytes];
    pthread_mutex_unlock(&audioQ_mutex);
    
    
    if (mPlayBufferPosition <= mPlayBufferEndPosition) {
        
        UInt64 mBufferByteSize = inCompleteAQBuffer->mAudioDataByteSize;
        UInt64 numRemainingBytes = mPlayBufferEndPosition - mPlayBufferPosition;
        UInt64 numBytesToCopy =  numRemainingBytes < mBufferByteSize ? numRemainingBytes : mBufferByteSize;
        
        
        // Do system copy of a chunk of data and enqueue it in the empty buffer so that the audioqueue playback (queue) can read it
        inCompleteAQBuffer->mAudioDataByteSize = numBytesToCopy;
        memcpy(inCompleteAQBuffer->mAudioData, (const void *) (mUserData  + (mPlayBufferPosition / sizeof(Byte))) , numBytesToCopy); //Copy data with offset
        
        AudioTimeStamp timeStamp;
        
        AudioQueueEnqueueBufferWithParameters(queue,
                                              inCompleteAQBuffer,
                                              0,
                                              NULL,
                                              0, // inTrimFramesAtStart: The number of priming frames to skip at the start of the buffer. We have 256 frames/buffer
                                              0, // inTrimFramesAtEnd : The number of frames to skip at the end of the buffer.
                                              0,
                                              NULL,
                                              NULL, //inStartTime : The desired start time for playing the buffer
                                              &timeStamp); //outActualStartTime : the time when the buffer will actually start playing.
        
        
        //        NSLog(@"timeInterval == %f", timeStamp.mSampleTime / SAMPLE_RATE);
        //        NSLog(@" mPlayBufferPosition %u \n ",(unsigned int)mPlayBufferPosition);
        
        //Enqueue call without params
        //        AudioQueueEnqueueBuffer(queue, inCompleteAQBuffer, 0, NULL);
        
        mPlayBufferPosition += numBytesToCopy;
        
    }
    else{
        AudioQueueStop(queue, NULL);
    }
}


#pragma mark PCM Decoder
//Decode an MP3 file in a PCM Bufferlist (myConvertedData)
- (NSData*) decodeMp3ToPCMFromAudioFile:(NSData *)mData  {
    @autoreleasepool {
        
//        pthread_mutex_lock(&stream_mutex);
        
        AudioFileID refAudioFileID;
        ExtAudioFileRef inputFileID;
        
        OSStatus result = AudioFileOpenWithCallbacks((__bridge void * _Nonnull)(mData), readProc, 0, getSizeProc, 0, kAudioFileMP3Type, &refAudioFileID);
        if (result != noErr)  {
            NSLog(@"problem in theAudioFileReaderWithData function: result code %i \n", result);
            //        return pcmData;
        }
        
        result = ExtAudioFileWrapAudioFileID(refAudioFileID, false, &inputFileID);
        if (result != noErr) {
            NSLog(@"problem in theAudioFileReaderWithData function Wraping the audio FileID: result code %i \n", result);
            //        return pcmData;
        }
        
        int size = sizeof(format);
        result = 0;
        result = ExtAudioFileSetProperty(inputFileID, kExtAudioFileProperty_ClientDataFormat, size, &format);
        if(result != noErr) {
            NSLog(@"error on ExtAudioFileSetProperty for input File with result code %i \n", result);
        }
        
        
        //How many samples to read in at a time Default 1024 (change this value to read a larger chunk of data, you will hear the sound a little longer)
        numSamples = [mData length]*32; //(UInt32)[data length]*4;//> mPlayBufferEndPosition ? [data length] : mPlayBufferEndPosition; //[pcmData length];//32768;
        //        NSLog(@"numSamples ==> %i", numSamples);
        UInt32 sizePerPacket = format.mBytesPerPacket; // = sizeof(Float32) = 32bytes
        UInt32 packetsPerBuffer = numSamples;
        UInt32 outputBufferSize = packetsPerBuffer;// * sizePerPacket ;
        
        // So the lvalue of outputBuffer is the memory location where we have reserved space
        UInt8 *outputBuffer = (UInt8 *)malloc(sizeof(UInt8 *) * outputBufferSize);
        
        AudioBufferList convertedData ;//= *(AudioBufferList *)malloc(sizeof(AudioBufferList) + sizeof(AudioBuffer) * (1));//;= malloc(sizeof(convertedData));
        
        convertedData.mNumberBuffers = 1;    // Set this to 1 for mono
        convertedData.mBuffers[0].mNumberChannels = format.mChannelsPerFrame;  //also = 1
        convertedData.mBuffers[0].mDataByteSize = outputBufferSize;
        convertedData.mBuffers[0].mData = outputBuffer; //
        
        UInt32 frameCount = numSamples; // todo .. change this logic.... we need to read all anyways ...
        //    while (frameCount > 0) {
        
        result = ExtAudioFileRead(inputFileID,&frameCount,&convertedData);
        
        if (result != noErr) {
            NSLog(@"Error on ExtAudioFileRead with result code %i \n", result);
            //            break;
        }
        //        NSLog(@" Decoding: Before pcm Appending (frameCount = %i )", convertedData.mBuffers[0].mDataByteSize );
        //        [pcmData appendData:tempData];
        // if the number of frame that was read is > 0
        
        if (frameCount > 0)  {
            //            NSLog(@" FRAMES LU: (frameCount = %u )", frameCount );
            
            //SOLUTION 1 : **** WORKS!!!...but crashed because run out of memory limit ***//
            //                [pcmDataBuffer appendBytes:(const void *)convertedData.mBuffers[0].mData length:convertedData.mBuffers[0].mDataByteSize];
            //            [pcmDataBuffer initWithBytes:convertedData.mBuffers[0].mData length:convertedData.mBuffers[0].mDataByteSize];
            //            if(!currentSongChanged ){
            //                currentSongChanged = true;
            ////                [data setLength:0];
            //            }
            
            // End Solution 1
            
            //*** SOLUTION 2 :Works.. but still with a gap ***//
            if(!currentSongChanged ){
                //                [pcmDataBuffer appendBytes:(const void *)convertedData.mBuffers[0].mData length:convertedData.mBuffers[0].mDataByteSize];
                pthread_mutex_lock(&audioQ_mutex);
                [pcmDataBuffer initWithBytes:convertedData.mBuffers[0].mData length:convertedData.mBuffers[0].mDataByteSize];
                currentSongChanged = true;
                [data setLength:0];
                pthread_mutex_unlock(&audioQ_mutex);
                
                //                }
                
            }else{
                
                pthread_mutex_lock(&audioQ_mutex);
                NSLog(@" old mPlayBufferEndPosition %u \n ",(unsigned int)mPlayBufferEndPosition);
                //                [pcmDataBuffer appendBytes:(const void *)convertedData.mBuffers[0].mData length:convertedData.mBuffers[0].mDataByteSize];
                [pcmDataBuffer appendData:[NSData dataWithBytes:convertedData.mBuffers[0].mData length:convertedData.mBuffers[0].mDataByteSize]];
                mPlayBufferEndPosition = (int)pcmDataBuffer.length;
                NSLog(@" new mPlayBufferEndPosition %u \n ",(unsigned int)mPlayBufferEndPosition);
                [data setLength:0];
                pthread_mutex_unlock(&audioQ_mutex);
                
            }
            //** END SOLUTION2 **//
            
        }
        
        //Clean up
        free(outputBuffer);
        ExtAudioFileDispose(inputFileID);
        AudioFileClose(refAudioFileID);
        
//        pthread_mutex_unlock(&stream_mutex);
        
        return pcmDataBuffer;
    }
}

// PCM Decoder callbacks
static OSStatus readProc(void* clientData,
                         SInt64 position,
                         UInt32 requestCount,
                         void* buffer,
                         UInt32* actualCount)
{
    
    NSData *inAudioData = (__bridge NSData *) clientData;
    
    size_t dataSize = inAudioData.length;
    size_t bytesToRead = 0;
    //    NSLog(@"position %lld === dataSize %zu ", position, dataSize);
    if(position <= dataSize) {
        size_t bytesAvailable = dataSize - position;
        bytesToRead = requestCount <= bytesAvailable ? requestCount : bytesAvailable;
        
        [inAudioData getBytes: buffer range:NSMakeRange(position, bytesToRead)];
    } else {
        NSLog(@"data was not read \n");
        bytesToRead = 0;
    }
    
    if(actualCount) {
        *actualCount = bytesToRead;
    }
    
    return noErr;
}

static SInt64 getSizeProc(void* clientData) {
    NSData *inAudioData = (__bridge NSData *) clientData;
    size_t dataSize = inAudioData.length;
    return dataSize;
}

#pragma mark Input Stream
// Init the InputStream
- (void)initNetworkCommunication:(NSString *)strUrl {
    //Set the request
    //    @autoreleasepool {
    CFReadStreamRef readStream;
    CFHTTPMessageRef request;
    NSURL *url;
    
    url = [NSURL URLWithString:strUrl];
    
    request = CFHTTPMessageCreateRequest(NULL, (CFStringRef)@"GET", (__bridge CFURLRef)(url), kCFHTTPVersion1_1);
    
    if (!request) NSLog(@"Error setting request");
    
    //Set the readStream
    readStream = CFReadStreamCreateForHTTPRequest(kCFAllocatorDefault, request);
    if (!readStream) NSLog(@"Error in readStream");
    /* add persistent property */
    CFReadStreamSetProperty(readStream, kCFStreamPropertyHTTPAttemptPersistentConnection, kCFBooleanTrue);
    /* make the read stream unique */
    CFReadStreamSetProperty(readStream, CFSTR("UniqueProperty"), @"UniqueProperty");
    
    
    //Assign the read stream to our inputStream
    inputStreamer = (__bridge_transfer NSInputStream *)readStream;
    CFReadStreamClose(readStream);
    
    //Set delegate and schedule and start loop
    
    [inputStreamer setDelegate:self];
    [inputStreamer scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [inputStreamer open];
    
    //            CFRelease(request);
    //            CFRelease(readStream);
    //    }
    
}

-(void) manageReceivedData :(NSData*)mData{
    
    if (!currentSongChanged) {
        //        NSLog(@"MY FIRST TIME");
        [self decodeMp3ToPCMFromAudioFile:data];
        resetAudioQueue();
    }else{
        //        NSLog(@"SECOND TIME");
        [self decodeMp3ToPCMFromAudioFile:data];
    }
}
//Delegate that manage the inputstream events
- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent {
    //    NSMutableData * tempData = [[NSMutableData alloc] init];
    int count= 0;
    switch (streamEvent) {
            
        case NSStreamEventOpenCompleted:
            NSLog(@"------------------------- STREAM Open -------------------------");
            break;
            
        case NSStreamEventHasSpaceAvailable:
        {
            //            NSLog(@"Request");
            break;
        }
        case NSStreamEventHasBytesAvailable:
            
            if (theStream == inputStreamer) {
                
//                pthread_mutex_lock(&stream_mutex);
                UInt32 len;
                uint8_t buffer[8192];
                len = (UInt32)[inputStreamer read:buffer maxLength:sizeof(buffer)];
                if (len > 0) {
                    
                    //                    [data appendBytes:(const void *)buffer length:len];
                    
                    // TODO: - TEST Trying to decode as soon as I have a certain amount (Seems Like Now its gapping at the chunk reception so this seems to work)
                    
                    [data appendBytes:(const void *)buffer length:len];
                    
                    
                    if (data.length > 524288) {
                        
                        [self manageReceivedData:data];//[NSData dataWithBytes:(__bridge const void * _Nullable)(data) length:data.length]];
                        //                                [data setLength:0];
//                        pthread_mutex_unlock(&stream_mutex);
                    }
                }
            }
            
            break;
            
        case NSStreamEventErrorOccurred:
            NSLog(@"Can not connect to the host!");
            break;
            
        case NSStreamEventEndEncountered:
        {
            NSLog(@"------------------------- STREAM End -------------------------");
            [self manageReceivedData:data];//[NSData dataWithBytes:(__bridge const void * _Nullable)(data) length:data.length]];
            [data setLength:0];
            [theStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
            [theStream close];
            theStream = nil;
            break;
        }
        default:
            NSLog(@"Unknown event");
    }
}

#pragma mark Song Download
-(void)downloadChuncksFromUrl:(NSString*)url localPath:(NSString*)localPath correlationID:(NSString*)correlationID musicStartTimeInUnixTime:(int64_t)musicStartTimeInUnixTime isLocalSong:(BOOL)isLocalSong{
    
    //    NSLog(@"old correlationID: %@", localCorrelationID);
    //    NSLog(@"new correlationID: %@", correlationID);
    
    if (![localCorrelationID isEqualToString:correlationID]) {
        
        //        NSLog(@"Cleaning buffer oldID: %@, newID: %@", localCorrelationID,correlationID);
        [self cleanBuffer];
        localCorrelationID = correlationID;
        
        music_start_timeInUnixTime = musicStartTimeInUnixTime;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // do work here
            [syncTimer invalidate];
            syncTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(syncTimerTickedLocal:) userInfo:nil repeats:YES];
        });
        
    }
    
    if (isLocalSong) {

        NSData *localChunkData = [NSData dataWithContentsOfFile: localPath];
//        ++current_chunk_index;
//        UploadManager *uploadManager_sharedInstance = [UploadManager sharedInstance];
//        NSData * localChunkData = [uploadManager_sharedInstance chunkDataAtIndex:current_chunk_index];
        
        [data appendBytes:[localChunkData bytes] length:localChunkData.length];
        [self manageReceivedData:data];
        
    }else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self initNetworkCommunication:url];
        });
    }
    
}

-(void)syncTimerTickedLocal:(NSTimer *)timer {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        doSyncMusic();
    });
    
}

#pragma mark Synchronisation
void doSyncMusic(){
    
    NSLog(@"\n");
    NSLog(@"------------------------- SYNC CALLED -------------------------");
    
    int64_t newCalculatedBufferPosition = thereoticalNbOfByteConsumed(music_start_timeInUnixTime);
    
    if (newCalculatedBufferPosition % 2 != 0){
        newCalculatedBufferPosition++;
    }
    
    int64_t nbBytesPadding = mPlayBufferEndPosition - newCalculatedBufferPosition;
    
    NSLog(@"current mPlayBufferPosition : %lld,", mPlayBufferPosition);
    NSLog(@"newCalculatedBufferPosition : %lld,", newCalculatedBufferPosition);
    NSLog(@"mPlayBufferEndPosition : %lld,", mPlayBufferEndPosition);
    NSLog(@"(mPlayBufferEndPosition - newCalculatedBufferPosition) : %lld,", nbBytesPadding);
    
    int64_t deltaBytesCorrection = mPlayBufferPosition - newCalculatedBufferPosition;
    int64_t deltaMsCorrection = msPerByte(deltaBytesCorrection);
    
    NSLog(@"deltaBytesCorrection : %lld,", deltaBytesCorrection);
    NSLog(@"deltaMsCorrection : %lld,", deltaMsCorrection);
    //
    if ( nbBytesPadding >  881998) {
        NSLog(@"Doing actual sync cursor move");
        mPlayBufferPosition = newCalculatedBufferPosition;
    } else {
        NSLog(@"Not anough padding, skipping actual sync");
    }
}
int64_t thereoticalNbOfByteConsumed(int64_t musicUnixStartTime){
    struct timeval tv;
    gettimeofday(&tv, NULL);
    unsigned long long millisecondsSinceEpoch =
    (unsigned long long)(tv.tv_sec) * 1000 +
    (unsigned long long)(tv.tv_usec) / 1000;
    
    int64_t currentTime = millisecondsSinceEpoch;
    NSLog(@"currentTime : %lld,", currentTime);
    int64_t timeElapsedSinceStart = currentTime - musicUnixStartTime;
    NSLog(@"timeElapsedSinceStart : %lld,", timeElapsedSinceStart);
    return bytePerMS(labs(timeElapsedSinceStart));
    //    return byteForElapsedTime(labs(timeElapsedSinceStart));
    
}


-(int64_t) getCurrentUnixTimeInMs{
    
    struct timeval tv;
    gettimeofday(&tv, NULL);
    unsigned long long millisecondsSinceEpoch =
    (unsigned long long)(tv.tv_sec) * 1000 +
    (unsigned long long)(tv.tv_usec) / 1000;
    return millisecondsSinceEpoch;
}

int64_t byteForElapsedTime(int64_t elapsedTimeInMs){
    
    //    int64_t elapsedTimeInSec = elapsedTimeInMs/1000;
    //    return elapsedTimeInSec*SAMPLE_RATE*NUM_CHANNELS*2;
    return ((double)(((double)elapsedTimeInMs)*SAMPLE_RATE*NUM_CHANNELS*2))/1000.0;
    
    //Experimental solution
    //    float nbOfSecToReadBuffer = 0.005805; // Got the number by printing the time it takes to read 1 buffer of size :BUFFER_SIZE
    //    long numOfByteConsummed = elapsedTimeInSec * BUFFER_SIZE /nbOfSecToReadBuffer;
    //    if (numOfByteConsummed%2 != 0){
    //        numOfByteConsummed += 1;
    //    }
    //    return numOfByteConsummed;
}
int64_t bytePerMS(int64_t timeInMs){
    
    return ((double)(((double)timeInMs)*SAMPLE_RATE*NUM_CHANNELS*2))/1000.0;
}
int64_t msPerByte(int64_t nbOfByte){
    return ((1000.0 * ((double)nbOfByte)) / ((double) SAMPLE_RATE*NUM_CHANNELS*2));
    // return ((double)(((double)timeInMs)*SAMPLE_RATE*NUM_CHANNELS*2))/1000.0;
}

#pragma mark local Song play
-(void)playLocalSong:(NSData*)songData correlationID:(NSString*)correlationID musicStartTimeInUnixTime:(int64_t)musicStartTimeInUnixTime{
}

#pragma mark AUDIOQUEUE PLAYBACK
-(void) startAudioQueue{
    AudioQueueStart (queue,NULL);
}
-(void) pauseAudioQueue{
    AudioQueuePause (queue);
}
-(void) resumeAudioQueue{
    AudioQueueStart(queue, NULL);
}
-(void) resetAudioQueue{
    AudioQueueReset(queue);
}
-(void) stopAudioQueue:(bool) immediately{
    //    [self pauseAudioQueue];
    AudioQueueStop(queue,immediately);
    [self cleanBuffer];
}

-(void) cleanBuffer {
    
    NSLog(@"Clean buffer");
    
    [pcmDataBuffer setLength:0];
    [data setLength:0];
    mPlayBufferPosition = 0;
    current_chunk_index = -1;
    //        mPlayBufferEndPosition = 0;
    currentSongChanged = false;
    
    //        [inputStreamer removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    //        [inputStreamer close];
    //        inputStreamer = nil;
    
    //[pcmDataBuffer resetBytesInRange:NSMakeRange(0, [pcmDataBuffer length])];
}

@end

