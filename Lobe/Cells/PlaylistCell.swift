//
//  PlaylistCell.swift
//  Lobe
//
//  Created by Professional on 2015-11-17.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import Bond
import Haneke
import Foundation
import MediaPlayer
import Haneke
import MGSwipeTableCell
import SwiftyJSON

class PlaylistCell: MGSwipeTableCell {
    
    @IBOutlet var cellImgLeadingConstraint: NSLayoutConstraint!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var artistLabel: UILabel!
    @IBOutlet var coverImage: UIImageView!
    @IBOutlet var optionListButton: UIButton!
    
    @IBOutlet var ownerLabel: UILabel!
    @IBOutlet var ownerPic: UIImageView!
    
    //Social
    @IBOutlet var thumbsUpButton:UIButton!
    @IBOutlet var thumbsDownButton:UIButton!
    @IBOutlet var socialView:UIView!
    @IBOutlet var thumbsUpCountLabel:UILabel!
    @IBOutlet var thumbsDownCountLabel:UILabel!
    
    var trackInfo:TrackInfo?
    var index = 0
    var onRemoveTrackToLobeList : ((PlaylistCell) -> Void)? = nil
    var songItem = SongItem(){
        didSet{
            
            changeUI()
            
            self.titleLabel?.text = songItem.trackInfo.mName ?? "Unknown"
            self.artistLabel?.text = songItem.trackInfo.mArtist ?? "Unknown"
            self.ownerLabel.text = songItem.trackInfo.mUserOwnerName
            
            //If Local Song find it in Library List
            if songItem.trackInfo.mUniqueDeviceId == unique_device_id {
                LobeStateHelper.currentUser.userName.observe{ username in
                    self.ownerLabel.text = username
                }
                LobeStateHelper.currentUser.userPic.observe{ image in
                    self.ownerPic.image = image
                }
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                    let resultArr = AUDIO_OUT_MANAGER.localSongItemList.filter({ $0.trackInfo.mID == self.songItem.trackInfo.mID })
                    
                        if resultArr.count > 0 {
                            let localSongItem = resultArr[0]
                            let width = Int(CGRectGetWidth(self.coverImage.bounds))
                            let heigth = Int(CGRectGetHeight(self.coverImage.bounds))
                            let croppedImage = DownloadHelper.cropImage( (localSongItem.trackInfo.albulArtImage), withWidth: width , andHeigth:heigth)
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.coverImage.image = croppedImage
                            })
                        }
                }
                
            }else{
                songItem.trackInfo.songAlbumArt.observe{ image in
                    self.coverImage.image = image
                }
                
                songItem.trackInfo.mUser!.downloadImage()
//            self.ownerPic.image = songItem?.trackInfo.mUser!.userImage
                songItem.trackInfo.mUser!.userPic.observe{ image in
                    self.ownerPic.image = image
                }
            }

            
            switch LobeStateHelper.sharedInstance.state {
            case .Local:
                socialView.hidden = true
            case .Participant,.DJ:
                socialView.hidden = false
                thumbsDownButton.setImage(UIImage(named: "like_btn_selected"), forState: UIControlState.Normal)
                thumbsDownButton.setImage(UIImage(named: "like_btn_unselected"), forState: UIControlState.Selected)
                
                thumbsUpButton.setImage(UIImage(named: "dislike_btn_selected"), forState: UIControlState.Normal)
                thumbsUpButton.setImage(UIImage(named: "dislike_btn_unselected"), forState: UIControlState.Selected)
                
                guard let socialVotingMap = AUDIO_OUT_MANAGER.socialVotingMapList else {
                    print("voting map is not set yet")
                    return
                }
//                guard let songItemJson = socialVotingMap["\(songItem.trackInfo.mID)"] else {
//                    print("didn't find ID: \(songItem.trackInfo.mID) in social voting map list")
//                    return
//                }
                
//                guard let promoting_user_ids_array = songItemJson["promoting_user_ids_array"] else {
//                    print("didn't find promoting_user_ids_array in social voting map list")
//                }
//                guard let disliking_user_ids_array = songItemJson["disliking_user_ids_array"] else {
//                    print("didn't find disliking_user_ids_array in social voting map list")
//                }

                let songItemJson = socialVotingMap[0]["\(songItem.trackInfo.mID)"]
                let promoting_user_ids_array = songItemJson["promoting_user_ids_array"].arrayValue
                let disliking_user_ids_array = songItemJson["disliking_user_ids_array"].arrayValue
                
//                print("songItem.trackInfo.mID: \(songItem.trackInfo.mID)")
//                print("songItemJson: \(songItemJson)")
//                print("like count: \(promoting_user_ids_array.count - 1)")
//                print("dislike count: \(disliking_user_ids_array.count - 1)")
                
                thumbsUpCountLabel.text = "\(promoting_user_ids_array.count - 1)"
                thumbsDownCountLabel.text = "\(disliking_user_ids_array.count - 1)"
                break
                
            }
        }
    }
    
    func changeUI(){
        
//        if self.nowPlayingSongRow == indexPath.section {
//            cell.cellImgLeadingConstraint.constant = 120
//            cell.titleLabel.textColor = CURRENT_PLAYING_COLOR
//        }else{
//            cell.cellImgLeadingConstraint.constant = 40
//            cell.titleLabel.textColor = UIColor.whiteColor()
//        }
        
        //configure swipe buttons
        let padding = 5
        let button1 =  MGSwipeButton(title: "DISLIKE",backgroundColor: UIColor.redColor(),padding: padding,
            callback: {
                (sender: MGSwipeTableCell!) -> Bool in
                self.handleRemoveTrack(self.songItem)
                return true
        })
        
        let button2 =  MGSwipeButton(title: "PROMOTE", backgroundColor: UIColor.greenPositiveColor(),padding: padding,
            callback: {
                (sender: MGSwipeTableCell!) -> Bool in
                NativeToWebHelper.sharedInstance.promoteTrackInLobeList(-1, trackUid: self.songItem.trackInfo.mID)
                return true
        })
        let font:UIFont = UIFont(name: "HelveticaNeue-Light", size: 11)!
        button1.titleLabel?.font = font
        button2.titleLabel?.font = font
        
        
        self.rightSwipeSettings.transition = MGSwipeTransition.Border
        self.rightExpansion.fillOnTrigger = true
        self.rightExpansion.threshold = 1.0
        self.rightExpansion.buttonIndex = 0
        
        self.leftSwipeSettings.transition = MGSwipeTransition.Border
        self.leftExpansion.fillOnTrigger = true
        self.leftExpansion.threshold = 1.0
        self.leftExpansion.buttonIndex = 0
        
        switch LobeStateHelper.sharedInstance.state {
            case .Local:
                //TODO: - Make a check if in editing mode or not. This code is duplicated for .Local and Participant case
                //Right now the local can only reorder songs
                
                let button =  MGSwipeButton(title: "REMOVE",backgroundColor: UIColor.redColor(),padding: padding,
                    callback: {
                        (sender: MGSwipeTableCell!) -> Bool in
                        self.handleRemoveTrack(self.songItem)
                        return true
                })
                
                //Inactive
                self.rightButtons = [button]
                self.rightExpansion.expansionColor = UIColor.redColor()
                
            case .DJ,.Participant:
                //Remove Song
                self.rightButtons = [button1]
                self.rightExpansion.expansionColor = UIColor.dislikeTrackColor()
                
                //Promote Song
                self.leftButtons = [button2]
                self.leftExpansion.expansionColor = UIColor.promoteTrackColor()
            }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let selectionView = UIView()
        selectionView.backgroundColor = SELECT_FOCUS_COLOR
        self.selectedBackgroundView = selectionView
        
//        self.backgroundColor = UIColor.playlistCellColor()
        
        if  LobeStateHelper.sharedInstance.state == .Local {
            
            LobeStateHelper.currentUser.userName.observe{ username in
                self.ownerLabel.text = username
            }
            
            LobeStateHelper.currentUser.userPic.observe{ image in
                self.ownerPic.image = image
            }
        }

    }
    
    func handleRemoveTrack(songItem:SongItem){
        
        let lockQueue = dispatch_queue_create("lobeList.LockQueue", nil)
        dispatch_sync(lockQueue) {
            switch LobeStateHelper.sharedInstance.state {
                
            case .Local:
                if (AUDIO_OUT_MANAGER.lobeListContains(songItem)){
                    let trackIndex = AUDIO_OUT_MANAGER.getIndexOfLobeListSongItem(songItem)
                     AUDIO_OUT_MANAGER.lobeListSongItems.removeObjectAtIndex(trackIndex)
                    AUDIO_OUT_MANAGER.lobeListObservableList.replaceRange(0...0, with: [SongItem()])

                   
                }
            case .DJ:
                NativeToWebHelper.sharedInstance.deleteTrackFromLobeList(songItem.trackInfo.mID)
            case .Participant:
                NativeToWebHelper.sharedInstance.dislikeTrackInLobeList(-1, trackUid: songItem.trackInfo.mID)
                
            }
            dispatch_async(dispatch_get_main_queue()) {
                AUDIO_OUT_MANAGER.musicListsDidChange()
                //                    self.tableView.reloadData()
            }
        }
    }
    
    func handleRemoveTrack(){
        
        if let onRemoveTapped = self.onRemoveTrackToLobeList {
            onRemoveTapped(self)
        }
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func setEditing(editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
                
        // Change the cell editing button image
//        if (editing) {
//            for view in subviews as [UIView] {
//                if view.dynamicType.description().rangeOfString("Reorder") != nil {
//                    for subview in view.subviews as! [UIImageView] {
//                        if subview.isKindOfClass(UIImageView) {
//                            
//                            var frame = subview.frame
//                            frame.size.height = 22
//                            frame.size.width = 5
//                            subview.frame = frame
//                            subview.backgroundColor = UIColor.clearColor()
//                            
//                            subview.image =  UIImage(named: "drag_2_col_right_morepoints_no_trans")!
//
//                        }
//                    }
//                }
//            }
//        }
    }
    
}

extension PlaylistCell {
    
    //TODO: - Need to change it Mixed
    @IBAction func downVoteSong(sender: AnyObject) {
//       NativeToWebHelper.sharedInstance.dislikeTrackInLobeList(-1, trackUid: (self.songItem.trackInfo.mID))
    }
    @IBAction func upVoteSong(sender: AnyObject) {
//         NativeToWebHelper.sharedInstance.promoteTrackInLobeList(-1, trackUid: (self.songItem.trackInfo.mID))
    }
}

