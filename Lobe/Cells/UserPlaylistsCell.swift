//
//  UserPlaylistsCell.swift
//  Lobe
//
//  Created by Professional on 2015-11-17.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit

class UserPlaylistsCell: UITableViewCell {
    
    @IBOutlet var playlistNameLabel: UILabel!
    @IBOutlet var songsNumberLabel : UILabel!
    @IBOutlet var coverImage: UIImageView!
    @IBOutlet var deletePlistButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
