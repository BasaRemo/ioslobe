//
//  CustonUploadAudioPlayerVC.m
//  Lobe
//
//  Created by Nabil Tahri on 16-01-17.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

#import "CustonUploadAudioPlayerVC.h"
#import "Lobe-Swift.h"
#import <AFNetworking/AFNetworking.h>

#define TICK   NSDate *startTime = [NSDate date]
#define TOCK   NSLog(@"Decode Duration: %f \n \n", -[startTime timeIntervalSinceNow])

//@import MediaPlayer;

//MPMediaQuery * everything;
//MHWDirectoryWatcher *_dirWatcher;
/*
 3839944631263754042
 3839944631263754043
 3839944631263754044
 3839944631263754045
 3839944631263754046
 3839944631263754047
 3839944631263754228
 
 var mTempStoragePath:String = "-1"// the full path (in cache) where the chuck (mp3) is saved
 var mNbOfBytes:Int = -1  // the exact nb of bytes in this chunk
 var mEstimatedLengthInSeconds:Int  = -1 // length of the chunk in seconds
 var mFirstFrameTimePositionMillis:Int  = -1 // At time
 
 */

@implementation CustonUploadAudioPlayerVC{
    
    NSMutableArray *songsList;
    //    ChunkInfo *chunkInfo;
}
@synthesize  _dirWatcher, everything,chunksInfoList,chunksDataList;

-(instancetype)init{
    self = [super init];
    return self;
}
ChunkInfo *chunkInfo;
int codeStatus = 0;
//NSMutableArray *chunksInfoList;
//NSMutableArray *chunksDataList;
int ret;
int nbChunks;
int nombreChunks;
//-(void)viewDidLoad{
//    [super viewDidLoad];
//    
//    [self createAllLocalFileChunks:@"3839944631263754043" nbSecondsPerChunk:30.0];
//    
//    
//}
//-(int)createAllLocalFileChunks:(NSString*)persistentID nbSecondsPerChunk:(double)nbSecondsPerChunk {
//    ret = 0;
//    nombreChunks = 0;
//    nbChunks = 0;
//    chunksInfoList = [[NSMutableArray alloc] init];
//    MPMediaItem *song;
//    
//    MPMediaPropertyPredicate *predicate = [MPMediaPropertyPredicate predicateWithValue: persistentID forProperty:MPMediaItemPropertyPersistentID];
//    MPMediaQuery *songQuery = [[MPMediaQuery alloc] init];
//    [songQuery addFilterPredicate: predicate];
//    if (songQuery.items.count > 0)
//    {
//        //song exists
//        NSLog(@"Found Song");
//        song = [songQuery.items objectAtIndex:0];
//        
//        [self cleanUpAudioFile:@"mp3"];
//        [self cleanUpAudioFile:@"m4a"];
//        NSNumber *duration= [song valueForProperty:MPMediaItemPropertyPlaybackDuration];
//        int nbS = (int)[duration doubleValue] / nbSecondsPerChunk;
//        nombreChunks = nbS;
//        
//        if ([duration doubleValue] < nbSecondsPerChunk) {
//            ret= -1;
//        }else{
//            
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                chunkInfo = [[ChunkInfo alloc] init];
//                [self mediaItemToData:song nbSplit:nbS vocalStartMarker:0 vocalEndMarker:0 index:0 mEstimatedLengthInSeconds:nbSecondsPerChunk mFirstFrameTimePositionMillis:0];
//            });
////            for (int i =0 ; i < nbS; i++) {
////                
////                
////                float start = i * nbSecondsPerChunk;
////                float end = ((i + 2) * nbSecondsPerChunk <= [duration doubleValue]) ? (i + 1) * nbSecondsPerChunk : [duration doubleValue];
////                //TODO - specifie the ret var when there is probleme with saving or encoding audios
////                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
////                    chunkInfo = [[ChunkInfo alloc] init];
////                    [self mediaItemToData:song nbSplit:nbS vocalStartMarker:start vocalEndMarker:end index:i mEstimatedLengthInSeconds:(end - start) mFirstFrameTimePositionMillis:start * 1000];
////                });
////            }
//            
//        }
//        
//        
//    }else{
//        NSLog(@"NOO!! Found Song");
//    }
//    
//    
//    return ret;
//}
-(NSData*) chunkDataAtIndex:(int) chunkIndex {
    return [NSData dataWithData:[chunksDataList objectAtIndex:chunkIndex]];
}
            
-(int)createAllLocalFileChunks:(NSString*)persistentID nbSecondsPerChunk:(UInt32)nbSecondsPerChunk {


    MPMediaItem *song;
    MPMediaPropertyPredicate *predicate = [MPMediaPropertyPredicate predicateWithValue: persistentID forProperty:MPMediaItemPropertyPersistentID];
    MPMediaQuery *songQuery = [[MPMediaQuery alloc] init];
    [songQuery addFilterPredicate: predicate];
    
    if (songQuery.items.count > 0)
    {
        //song exists
        NSLog(@"Found Song");
        song = [songQuery.items objectAtIndex:0];
        [self dataFromMediaItem:song mNbSecondsPerChunk:nbSecondsPerChunk];
        
    }else{
        [self invokeChunkCreationCallbackFailed:0];
        NSLog(@"Error Can't Find Song");
    }
    return 0;
}

-(void)dataFromMediaItem :(MPMediaItem *)curItem  mNbSecondsPerChunk:(int)nbSecondsPerChunk
{
    //     TICK;
    NSLog(@"Media Item To Data");
    NSNumber *songDuration= [curItem valueForProperty:MPMediaItemPropertyPlaybackDuration];
    NSURL *url = [curItem valueForProperty: MPMediaItemPropertyAssetURL];
    NSError *assetError;
    AVURLAsset *avasset = [[AVURLAsset alloc] initWithURL:url options:nil];
    
    self.assetReader = [AVAssetReader assetReaderWithAsset:avasset error:&assetError];
    self.assetOutput = [AVAssetReaderTrackOutput assetReaderTrackOutputWithTrack:avasset.tracks[0] outputSettings:nil];
    
    if ([self.assetReader canAddOutput:self.assetOutput]) {
        NSLog(@"Start Reading");
        [self.assetReader addOutput:self.assetOutput];
        [self.assetReader startReading];
        [self SaveDataToUrlLocalPath:songDuration.doubleValue mNbSecondsPerChunk:nbSecondsPerChunk];
    }else{
        [self invokeChunkCreationCallbackFailed:0];
    }
    
    
}

-(ChunkInfo*) SaveDataToUrlLocalPath :(double)songDuration mNbSecondsPerChunk:(int)nbSecondsPerChunk
{
    
    TICK;
    // Write the entire mediaItem to mp3
    chunksInfoList = [[NSMutableArray alloc] init];
    chunksDataList = [[NSMutableArray alloc] init];
    BOOL firstTime = YES;
    NSMutableData *dataBuffer;
    CMSampleBufferRef sample;
    CMBlockBufferRef blockBufferRef = NULL;
//    [self.assetReader ];
    while(self.assetReader.status == AVAssetReaderStatusReading) { //while (1){

        sample = [self.assetOutput copyNextSampleBuffer];
//        CMTime duration = CMSampleBufferGetDuration(sample);
//        NSLog(@"Duration: %lld seconds",duration.value/duration.timescale);
        
        blockBufferRef = CMSampleBufferGetDataBuffer(sample);
        size_t length = CMBlockBufferGetDataLength(blockBufferRef);
        UInt8 buffer[length];
        CMBlockBufferCopyDataBytes(blockBufferRef, 0, length, buffer);
//            NSLog(@"buffer size: %zu",length);
        if (firstTime) {
            firstTime = NO;
            dataBuffer = [[NSMutableData alloc] initWithBytes:buffer length:length];
        }else{
            [dataBuffer appendBytes:buffer length:length];
        }
    }
    
    UInt64 fileByteSize = dataBuffer.length;
    
    if (fileByteSize == 0 || songDuration < nbSecondsPerChunk) {
        [self invokeChunkCreationCallbackFailed:0];
    }else{
        
        UInt64 numByteIn30Secs = (fileByteSize/songDuration)*nbSecondsPerChunk;
        NSLog(@"song byte size: %llu",fileByteSize);
        NSLog(@"song duration: %f",songDuration);
        NSLog(@"numByteIn30Secs: %llu",numByteIn30Secs);
        
        UInt64 mBufferCurrentPosition = 0;
        NSData *chunkFileData;
        
        int chunkNumber = 0;
        [chunksInfoList removeAllObjects];
        
        while (1) {
            chunkNumber++;
            UInt64 numRemainingBytes = fileByteSize - mBufferCurrentPosition;
            if (numRemainingBytes >= 2*numByteIn30Secs) {
               chunkFileData = [dataBuffer subdataWithRange:NSMakeRange(mBufferCurrentPosition, numByteIn30Secs)];
                
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"LobeCachedAudioFile%i.mp3", chunkNumber]];
                [chunkFileData writeToFile:dataPath atomically:YES];
 
                ChunkInfo *mChunkInfo = [[ChunkInfo alloc] init];
                mChunkInfo.mNbOfBytes = numByteIn30Secs;
                mChunkInfo.mEstimatedLengthInSeconds = 30;
                mChunkInfo.mFirstFrameTimePositionMillis = mBufferCurrentPosition*(songDuration*1000)/fileByteSize;
                mChunkInfo.mTempStoragePath = dataPath;
                mBufferCurrentPosition += numByteIn30Secs;
                [chunksInfoList addObject:mChunkInfo];
                [chunksDataList addObject:chunkFileData];
                
                
                
                continue;
            }else{
                chunkFileData = [dataBuffer subdataWithRange:NSMakeRange(mBufferCurrentPosition, numRemainingBytes)];
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"CachedAudioFile%i", chunkNumber]];
                [chunkFileData writeToFile:dataPath atomically:YES];
                
                ChunkInfo *mChunkInfo = [[ChunkInfo alloc] init];
                mChunkInfo.mNbOfBytes = numRemainingBytes;
                mChunkInfo.mEstimatedLengthInSeconds = numRemainingBytes*songDuration/fileByteSize;
                mChunkInfo.mFirstFrameTimePositionMillis = mBufferCurrentPosition*(songDuration*1000)/fileByteSize;
                mChunkInfo.mTempStoragePath = dataPath;
                
                [chunksInfoList addObject:mChunkInfo];
                [chunksDataList addObject:chunkFileData];
                
                break;
            }
        }
        NSLog(@" Number of CHUNKS: %lu ",(unsigned long)chunksInfoList.count);
        
        for (int i=0; i<chunksInfoList.count; i++) {
            
            ChunkInfo *chunkInfoObj = chunksInfoList[i];
            NSLog(@"\n ");
            NSLog(@" ChunkNumber: %d ",i);
            NSLog(@"mTempStoragePath %@", chunkInfoObj.mTempStoragePath);
            NSLog(@"mNbOfBytes %lu", chunkInfoObj.mNbOfBytes);
            NSLog(@"mEstimatedLengthInSeconds %lu", chunkInfoObj.mEstimatedLengthInSeconds);
            NSLog(@"mFirstFrameTimePositionMillis %lu", chunkInfoObj.mFirstFrameTimePositionMillis);
        }
        [self invokeChunkCreationCallbackSucceded:chunksInfoList];
    }
    
    TOCK;
    return chunkInfo;
}

-(void) invokeChunkCreationCallbackFailed:(int) status{
    
    NativeToWebHelper *nativeToWeb = [NativeToWebHelper sharedInstance];
    [nativeToWeb chunksLocalCreationCallback:NULL status:status];
    NSLog(@"Chunk Creation Failed");
}
-(void) invokeChunkCreationCallbackSucceded:(NSMutableArray*) chunkInfoList{
    
    NativeToWebHelper *nativeToWeb = [NativeToWebHelper sharedInstance];
    [nativeToWeb chunksLocalCreationCallback:chunksInfoList status:1];
}

-(void) startUploading:(MPMediaItem*)song nbreOfSplite:(int) nbreOfSplite{
    
    
    //    everything = [MPMediaQuery songsQuery];
    //    NSArray *itemsFromGenericQuery = [[NSArray alloc] initWithArray:[everything items]];
    //    songsList = [NSMutableArray arrayWithArray:itemsFromGenericQuery];
    //    song = [songsList objectAtIndex:0];
    //    [self cleanUpAudioFile:@"m4a"];
    [self cleanUpAudioFile:@"mp3"];
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        [self mediaItemToData:song nbSplit:4 vocalStartMarker:0 vocalEndMarker:0 index:0 mEstimatedLengthInSeconds:0 mFirstFrameTimePositionMillis:0];
//        
//    });
    
//    NSNumber *duration= [song valueForProperty:MPMediaItemPropertyPlaybackDuration];
//    NSString *docsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
//    [NSThread detachNewThreadSelector:@selector(checkAndSend) toTarget:self withObject:nil];
//    
//    _dirWatcher = [MHWDirectoryWatcher directoryWatcherAtPath:docsDirectory callback:^{
//        [self checkAndSend];
//    }];
    
//    
//    int nbS = nbreOfSplite;
//    for (int i =0 ; i < nbS; i++) {
//        float start = i * [duration floatValue] / (float)nbS;
//        float end = (i + 1) * [duration floatValue] / (float)nbS;
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            [self mediaItemToData:song nbSplit:nbS vocalStartMarker:start vocalEndMarker:end index:i mEstimatedLengthInSeconds:0 mFirstFrameTimePositionMillis:0];
//            
//        });
//    }
    
}

-(void) startUploadingWithURL:(UInt32)chunckIndex url:(NSString*)urlName correlationID:(NSString*)correlationID chunkStartTimeServer:(UInt64) chunkStartTimeServer{
    NativeToWebHelper *nativeToWeb = [NativeToWebHelper sharedInstance];
    [self uploadAudioFile:chunckIndex fileName:urlName correlationID:correlationID chunkStartServerTime:chunkStartTimeServer];
}

-(void)checkAndSend{
    //    NSMutableArray *fileNames = [[NSMutableArray alloc] init];
    NSString *fileName = [self checkMp3Exist];
    if (fileName != nil) {
        //        if (![fileNames containsObject:fileName]) {
        [self sendChuncks:fileName];
        //            [fileNames addObject:fileNames];
        //        }
        
    }
}
//allow as the send the file the server
-(NSString*) sendChuncks:(NSString*)fileName{
    //    NSFileManager  *manager = [NSFileManager defaultManager];
    NSString *docsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *tmpStr = [NSString stringWithFormat:@"%@/%@",docsDirectory, fileName];
    NSData* data = [[NSData alloc] initWithContentsOfFile:tmpStr];
    NSLog(@"%@", [fileName stringByReplacingOccurrencesOfString:@" " withString:@""]);
//    return [self uploadAudioFile:data fileName:[fileName stringByReplacingOccurrencesOfString:@" " withString:@""]];
    return @"";
    
}
//Check if the file exist and returnthe name
-(NSString*)checkMp3Exist{
    NSError *error;
    NSString * name = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSArray *contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:&error];
    
    
    NSEnumerator *e = [contents objectEnumerator];
    NSString *filename;
    while ((filename = [e nextObject])) {
        if ([[filename pathExtension] isEqualToString:@"mp3"]) {
            name = filename;
            
            return name;
        }
    }
    return name;
}
-(NSString*)cleanString:(NSString *)sampleString{
    NSRegularExpression *expression = [NSRegularExpression regularExpressionWithPattern:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
                                                                                options:0
                                                                                  error:NULL];
    
    NSString *cleanedString = [expression stringByReplacingMatchesInString:sampleString
                                                                   options:0
                                                                     range:NSMakeRange(0, sampleString.length)
                                                              withTemplate:@""];
    return cleanedString;
    
}

//bring mp3 file from itunes library and convert it to m4a file
//-(ChunkInfo *)mediaItemToData : (MPMediaItem * ) curItem nbSplit:(int)nbSplit vocalStartMarker:(float) vocalStartMarker vocalEndMarker:(float) vocalEndMarker index:(int)index mEstimatedLengthInSeconds:(int)mEstimatedLength mFirstFrameTimePositionMillis:(int)mFirstFrameTime
//{
//    //     TICK;
//    NSLog(@"Media Item To Data");
//    
//    nbChunks = 0;
//    //    ChunkInfo *__block chunkInfo = [[ChunkInfo alloc] init];
//    NSString * name = [curItem valueForKey:MPMediaItemPropertyTitle];
//    
//    //    NSLog(@"%@", duration);
//    NSString *songName = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
//    NSURL *url = [curItem valueForProperty: MPMediaItemPropertyAssetURL];
//    NSError *assetError;
//    
//    AVURLAsset *avasset = [[AVURLAsset alloc] initWithURL:url options:nil];
//    
//    self.assetReader = [AVAssetReader assetReaderWithAsset:avasset error:&assetError];
//    self.assetOutput = [AVAssetReaderTrackOutput assetReaderTrackOutputWithTrack:avasset.tracks[0] outputSettings:nil];
//    
//    //    self.assetReader = [AVAssetReader assetReaderWithAsset:avasset error:&assetError];
//    //    self.assetOutput = [AVAssetReaderTrackOutput assetReaderTrackOutputWithTrack:avasset.tracks[0] outputSettings:nil];
//    //    if (![self.assetReader canAddOutput:self.assetOutput]) return;
//    NSLog(@"Before Reading");
//    if ([self.assetReader canAddOutput:self.assetOutput]) {
//        NSLog(@"Start Reading");
//        [self.assetReader addOutput:self.assetOutput];
//        [self.assetReader startReading];
////        [self SaveDataToUrlLocalPath];
//    }
//    
//    
//    //    AVURLAsset *songAsset = [AVURLAsset URLAssetWithURL: url options:nil];
//    //    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset: songAsset presetName:AVAssetExportPresetAppleM4A];
//    //    NSString *fileType = [[[[songAsset.URL absoluteString] componentsSeparatedByString:@"?"] objectAtIndex:0] pathExtension];//@"mp3"
//    //
//    //    exporter.shouldOptimizeForNetworkUse = YES;
//    //
//    //    exporter.outputFileType = AVFileTypeAppleM4A;   //AVFileTypeAppleM4A;//@"com.apple.m4a-audio";
//    //
//    //    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    //    NSString * myDocumentsDirectory = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
//    //
//    //
//    //    NSString * fileName = [NSString stringWithFormat:@"%@%i.m4a",songName, index];
//    //    CMTime startTime = CMTimeMake((int)(floor(vocalStartMarker * 100)), 100);
//    //    CMTime stopTime = CMTimeMake((int)(ceil(vocalEndMarker * 100)), 100);
//    //    CMTimeRange exportTimeRange = CMTimeRangeFromTimeToTime(startTime, stopTime);
//    //
//    //    NSString *exportFile = [myDocumentsDirectory stringByAppendingPathComponent:fileName];
//    //
//    //    NSURL *exportURL = [NSURL fileURLWithPath:exportFile];
//    //    exporter.outputURL = exportURL;
//    //    exporter.timeRange = exportTimeRange;
//    //
//    //
//    //
//    //
//    //    // do the export
//    //    // (completion handler block omitted)
//    //    [exporter exportAsynchronouslyWithCompletionHandler:
//    //     ^{
//    //         NativeToWebHelper *nativeToWeb;
//    //         int exportStatus = exporter.status;
//    //         if (exportStatus == AVAssetExportSessionStatusCompleted) {
//    //             nativeToWeb = [[NativeToWebHelper alloc] init];
//    //             NSLog (@"AVAssetExportSessionStatusCompleted");
//    //             NSString* tmp = [myDocumentsDirectory stringByAppendingString:[NSString stringWithFormat:@"/%@", fileName]];
//    //             NSData *data = [NSData dataWithContentsOfFile: tmp];
//    //             //             chunkInfo.mNbOfBytes = [data length];
//    //             //             chunkInfo.mTempStoragePath = tmp;
//    //             //             chunkInfo.mEstimatedLengthInSeconds = mEstimatedLength;
//    //             //             chunkInfo.mFirstFrameTimePositionMillis = mFirstFrameTime;
//    //
//    //
//    //             chunkInfo = [self audioFileReaderWithData:data fileName:[songName stringByAppendingString:[NSString stringWithFormat:@"%i", index]] mEstimatedLengthInSeconds:mEstimatedLength mFirstFrameTimePositionMillis:mFirstFrameTime];
//    //             [chunksInfoList addObject:chunkInfo];
//    //             nbChunks++;
//    //             if (nbChunks == nombreChunks) {
//    //                 ret = 0;
//    //                 [nativeToWeb chunksLocalCreationCallback:chunksInfoList status:ret];
//    //             }
//    //             //                 NSLog(@"mTempStoragePath %@", chunkInfo.mTempStoragePath);
//    //             //                 NSLog(@"mNbOfBytes %lu", chunkInfo.mNbOfBytes);
//    //             //                 NSLog(@"mEstimatedLengthInSeconds %lu", chunkInfo.mEstimatedLengthInSeconds);
//    //             //                 NSLog(@"mFirstFrameTimePositionMillis %lu", chunkInfo.mFirstFrameTimePositionMillis);
//    //             NSLog(@"nbChunks ========> %d   nombreChunks ====> %d  ret ===> %d", nbChunks, nombreChunks, ret);
//    //             data = nil;
//    //         }else{
//    //             NSLog (@"AVAssetExportSessionStatusNotCompleted !!!!!!!");
//    //         }
//    //     }];
//    return chunkInfo;
//}

//Upload file to the server and remove it
-(NSString*)uploadAudioFile:(UInt32)chunkIndex fileName:(NSString*) mFileName correlationID:(NSString*) mCorrelationID chunkStartServerTime:(UInt64) mChunkStartServerTime {
    
    NSString *urlString = @"http://lobemusic.com/upload";
    NSString* theFileName = [[mFileName lastPathComponent] stringByDeletingPathExtension];
    NSLog(@"FILE NAME: %@",theFileName);
    
        NSMutableURLRequest* request= [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"POST"];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        NSMutableData *postbody = [NSMutableData data];
        [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n", mFileName] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[NSData dataWithData:[chunksDataList objectAtIndex:chunkIndex]]];
        [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
        [request setHTTPBody:postbody];
    
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
//                      NSLog(@"Progress: %f",uploadProgress.fractionCompleted);
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if (error) {
                          NSLog(@"Error: %@", error);
                      } else {
//                          NSLog(@"%@ %@", response, responseObject);
                          
                          NSDictionary *JSONDic=[[NSDictionary alloc] init];
                          NSError *error;
                          NSData *jsonData = [NSJSONSerialization dataWithJSONObject:responseObject
                                                                             options:NSJSONWritingPrettyPrinted
                                                                               error:&error];
                          NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                           NSLog(@"jsonString %@", jsonString);
      
                          jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                          jsonString = [NSString stringWithFormat:@"%s",[jsonString UTF8String]];
                                        
                              NativeToWebHelper *nativeToWeb = [NativeToWebHelper sharedInstance];
                          [nativeToWeb fileUploadResponseFromServer:200 resultStr:jsonString correlationID:mCorrelationID chunkStartServerTime:mChunkStartServerTime];
                      }
                  }];
    
    [uploadTask resume];
    return @"";
//    NSString *urlString = @"http://lobemusic.com/upload";
////    NSString *filename = fileName;
////    NSLog(@"FILE NAME BEFORE : %@",mFileName);
////    NSString* filename = [mFileName stringByDeletingPathExtension];
////    NSLog(@"FILE NAME: %@",filename);
//    
//    NSMutableURLRequest* request= [[NSMutableURLRequest alloc] init];
//    [request setURL:[NSURL URLWithString:urlString]];
//    [request setHTTPMethod:@"POST"];
//    NSString *boundary = @"---------------------------14737809831466499882746641449";
//    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
//    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
//    NSMutableData *postbody = [NSMutableData data];
//    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n", mFileName] dataUsingEncoding:NSUTF8StringEncoding]];
//    [postbody appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    [postbody appendData:[NSData dataWithData:data]];
//    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [request setHTTPBody:postbody];
//    NSHTTPURLResponse *response = nil;
//    
//    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
//    NSString*returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
//    //Server response
//    codeStatus = (int)[response statusCode];
//    NSLog(@"Server Code Response :%ld === JSON Response:%@", (long)[response statusCode], returnString);
//    NSError *error = nil;
//    NSString *dataPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/MusicSent"];
//    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath]){
//        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
//    }
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *newDirectory = [documentsDirectory stringByAppendingPathComponent:@"MusicSent"];
//    [[NSFileManager defaultManager] moveItemAtPath:[documentsDirectory stringByAppendingPathComponent:mFileName]
//                                            toPath:[newDirectory stringByAppendingPathComponent:mFileName]
//                                             error:&error];
//    if (error) {
//        ret = -1;
//    }
//    //mp3
//    //        [[NSFileManager defaultManager] removeItemAtPath:[documentsDirectory stringByAppendingPathComponent:fileName] error:NULL];
//    //m4a
//    [[NSFileManager defaultManager] removeItemAtPath:[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.m4a",[[mFileName lastPathComponent] stringByDeletingPathExtension]]] error:NULL];
//    
//    return returnString;
}

//Make a cleanup and remove all files which content the extension in parameter
-(void) cleanUpAudioFile:(NSString*) fileExtension{
    NSFileManager  *manager = [NSFileManager defaultManager];
    
    // the preferred way to get the apps documents directory
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory;
    //    if ([fileExtension isEqualToString:@"mp3"]) {
    //        documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/MusicSent"];
    //    }else{
    documentsDirectory = [paths objectAtIndex:0];
    //    }
    //
    
    // grab all the files in the documents dir
    NSArray *allFiles = [manager contentsOfDirectoryAtPath:documentsDirectory error:nil];
    
    // filter the array for only sqlite files
    NSString* fExtension = [NSString stringWithFormat:@"self ENDSWITH '.%@'", fileExtension];
    //    NSPredicate *fltr = [NSPredicate predicateWithFormat:@"self ENDSWITH '.m4a'"];
    NSPredicate *fltr = [NSPredicate predicateWithFormat:fExtension];
    NSArray *audioFiles = [allFiles filteredArrayUsingPredicate:fltr];
    
    // use fast enumeration to iterate the array and delete the files
    for (NSString *audioFile in audioFiles)
    {
        NSError *error = nil;
        
        [manager removeItemAtPath:[documentsDirectory stringByAppendingPathComponent:audioFile] error:&error];
        NSAssert(!error, @"Assertion: Audio file deletion shall never throw an error.");
    }
}

static OSStatus readProc(void* clientData,
                         SInt64 position,
                         UInt32 requestCount,
                         void* buffer,
                         UInt32* actualCount)
{
    
    NSData *inAudioData = (__bridge NSData *) clientData;
    
    size_t dataSize = inAudioData.length;
    size_t bytesToRead = 0;
    
    if(position < dataSize) {
        size_t bytesAvailable = dataSize - position;
        bytesToRead = requestCount <= bytesAvailable ? requestCount : bytesAvailable;
        
        [inAudioData getBytes: buffer range:NSMakeRange(position, bytesToRead)];
    } else {
        NSLog(@"data was not read \n");
        bytesToRead = 0;
        ret = -1;
    }
    
    if(actualCount)
        *actualCount = bytesToRead;
    
    return noErr;
}

static SInt64 getSizeProc(void* clientData) {
    NSData *inAudioData = (__bridge NSData *) clientData;
    size_t dataSize = inAudioData.length;
    return dataSize;
}



//Convert to mp3 and save file in var/.../documents/ folder
-(ChunkInfo*) audioFileReaderWithData: (NSData *) audioData fileName:(NSString*) fileName mEstimatedLengthInSeconds:(int)mEstimatedLengthInSeconds mFirstFrameTimePositionMillis:(int)mFirstFrameTimePositionMillis{
    
    
    ChunkInfo *chunkInf = [[ChunkInfo alloc] init];
    chunkInf.mFirstFrameTimePositionMillis = mFirstFrameTimePositionMillis;
    chunkInf.mEstimatedLengthInSeconds = mEstimatedLengthInSeconds;
    
    AudioFileID         refAudioFileID;
    ExtAudioFileRef     inputFileID;
    ExtAudioFileRef     outputFileID;
    
    OSStatus result = AudioFileOpenWithCallbacks((__bridge void * _Nonnull)(audioData), readProc, 0, getSizeProc, 0, kAudioFileMP3Type, &refAudioFileID);
    if(result != noErr){
        NSLog(@"problem in theAudioFileReaderWithData function: result code %i \n", result);
        ret = -1;
    }
    
    result = ExtAudioFileWrapAudioFileID(refAudioFileID, false, &inputFileID);
    if (result != noErr){
        NSLog(@"problem in theAudioFileReaderWithData function Wraping the audio FileID: result code %i \n", result);
        ret = -1;
    }
    
    // Client Audio Format Description
    AudioStreamBasicDescription clientFormat;
    memset(&clientFormat, 0, sizeof(clientFormat));
    clientFormat.mFormatID          = kAudioFormatLinearPCM;
    clientFormat.mFramesPerPacket   = 1;
    clientFormat.mChannelsPerFrame  = 2;
    clientFormat.mBitsPerChannel    = 32;
    clientFormat.mBytesPerPacket    = clientFormat.mBytesPerFrame = 4 *   clientFormat.mChannelsPerFrame;
    clientFormat.mFormatFlags       = kAudioFormatFlagsNativeFloatPacked;
    clientFormat.mSampleRate        = 44100;
    
    //Output Audio Format Description
    AudioStreamBasicDescription outputFormat;
    memset(&outputFormat, 0, sizeof(outputFormat));
    outputFormat.mChannelsPerFrame  = 2;
    outputFormat.mSampleRate        = 44100;
    outputFormat.mFormatID          = kAudioFormatMPEG4AAC;
    outputFormat.mFormatFlags       = kMPEG4Object_AAC_Main;
    outputFormat.mBitsPerChannel    = 0;
    outputFormat.mBytesPerFrame     = 0;
    outputFormat.mBytesPerPacket    = 0;
    outputFormat.mFramesPerPacket   = 1024;
    
    // create the outputFile that we're writing to here....
    UInt32 outputFormatSize = sizeof(outputFormat);
    result = 0;
    result = AudioFormatGetProperty(kAudioFormatProperty_FormatInfo, 0, NULL, &outputFormatSize, &outputFormat);
    if(result != noErr) {
        NSLog(@"could not set the output format with status code %i \n",result);
        ret = -1;
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString * name = [NSString stringWithFormat:@"%@.mp3", fileName];
    NSMutableString *outputFilePath = [documentsDirectory stringByAppendingPathComponent:name];
    chunkInf.mTempStoragePath = [NSString stringWithFormat:@"%@", outputFilePath];
    
    //    NSMutableString *outputFilePath = [NSMutableString stringWithCapacity: 100];
    //    [outputFilePath setString:@"/Users/You/Desktop/testAudio.m4a"];
    NSURL *sourceURL = [NSURL fileURLWithPath:outputFilePath];
    //    NSLog(@"chunkInf.mTempStoragePath : %@", chunkInf.mTempStoragePath);
    
    result      =  0;
    result      =  ExtAudioFileCreateWithURL((__bridge CFURLRef)sourceURL, kAudioFileM4AType, &outputFormat, NULL, kAudioFileFlags_EraseFile, &outputFileID);
    if(result != noErr){
        NSLog(@"ExtAudioFileCreateWithURL failed for outputFileID with status %i \n", result);
        ret = -1;
    }
    
    int size = sizeof(clientFormat);
    result = 0;
    result = ExtAudioFileSetProperty(inputFileID, kExtAudioFileProperty_ClientDataFormat, size, &clientFormat);
    
    if(result != noErr) {
        NSLog(@"error on ExtAudioFileSetProperty for input File with result code %i \n", result);
        ret = -1;
    }
    
    size = sizeof(clientFormat);
    result = 0;
    result = ExtAudioFileSetProperty(outputFileID, kExtAudioFileProperty_ClientDataFormat, size, &clientFormat);
    if(result != noErr){
        NSLog(@"error on ExtAudioFileSetProperty for output File with result code %i \n", result);
        ret = -1;
    }
    
    int totalFrames = 0;
    UInt32 outputFilePacketPosition = 0; //in bytes
    UInt32 encodedBytes = 0;
    
    while (1) {
        UInt32 bufferByteSize       = 22050 * 4 * 2;
        char srcBuffer[bufferByteSize];
        UInt32 numFrames            = (bufferByteSize/clientFormat.mBytesPerFrame);
        
        AudioBufferList fillBufList;
        fillBufList.mNumberBuffers  = 1;
        fillBufList.mBuffers[0].mNumberChannels     = clientFormat.mChannelsPerFrame;
        fillBufList.mBuffers[0].mDataByteSize       = bufferByteSize;
        fillBufList.mBuffers[0].mData               = srcBuffer;
        result = 0;
        result = ExtAudioFileRead(inputFileID, &numFrames, &fillBufList);
        
        if (result != noErr) {
            NSLog(@"Error on ExtAudioFileRead with result code %i \n", result);
            totalFrames = 0;
            ret = -1;
            break;
        }
        if (!numFrames)
            break;
        
        totalFrames = totalFrames + numFrames;
        
        result = 0;
        result = ExtAudioFileWrite(outputFileID,
                                   numFrames,
                                   &fillBufList);
        
        if(result!= noErr){
            NSLog(@"ExtAudioFileWrite failed with code %i \n", result);
            ret = -1;
        }
        
        encodedBytes += numFrames  * clientFormat.mBytesPerFrame;
        chunkInf.mNbOfBytes = encodedBytes;
    }
    
    
    //Clean up
    //    [self cleanUpAudioFile:@"m4a"];
    ExtAudioFileDispose(inputFileID);
    ExtAudioFileDispose(outputFileID);
    AudioFileClose(refAudioFileID);
    return chunkInf;
    
}

@end
