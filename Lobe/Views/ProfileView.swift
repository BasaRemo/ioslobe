//
//  ProfileView.swift
//  OfficeHours
//
//  Created by Eliel Gordon on 12/8/15.
//  Copyright © 2015 Saltar Group. All rights reserved.
//

import UIKit
import Bond

let ProfileViewCellIdentifier = "ProfileView"

//Create the delegate protocol to add a mediaItem to a lobeList
protocol ProfileViewDelegate {
    func openWebView()
    func expandMusicList()
    func createGroup()
}


class ProfileView: UICollectionReusableView {

    @IBOutlet var containerView:UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet var socialButton:UIButton!
    //
    @IBOutlet var segmentControl : ADVSegmentedControl!
    
    @IBOutlet var timeSlider: UISlider!
    var delegate:ProfileViewDelegate?
    var albumArtImage:UIImage?
    
    @IBAction func profilBtnPressed(sender: UIButton) {
        sender.selected = !sender.selected
        self.delegate?.openWebView()
    }
    
    @IBAction func expandBtnPressed(sender: UIButton) {
        sender.selected = !sender.selected
        self.delegate?.expandMusicList()
    }
    
    @IBAction func createGroup(){
        self.delegate?.createGroup()
    }
    
    func updateState(){
//        print("ProfilView update")
        
        switch LobeStateHelper.sharedInstance.state {
            
        case .Local:
            stateLabel.text = "Local Libray"
            self.containerView.backgroundColor = UIColor(patternImage: UIImage(named: "success-baby")!)
            socialButton.setImage(UIImage(named: "user_male_circle_filled")!, forState: UIControlState.Normal)
            
        case .DJ:
            stateLabel.text = "DJ Broadcast"
            self.containerView.backgroundColor = UIColor.blueColor()
//            socialButton.setImage(<#T##image: UIImage?##UIImage?#>, forState: UIControlState.Normal)
            
        case .Participant:
            stateLabel.text = "Streaming"
            self.containerView.backgroundColor = UIColor.redColor()
//            socialButton.setImage(<#T##image: UIImage?##UIImage?#>, forState: UIControlState.Normal)
        
        }
    }
    
    func actOnMusicChange(notification:NSNotification){
        // TODO: Get currrent song meta data
        let songItem = AUDIO_OUT_MANAGER.currentSong
        if let song = songItem.music {
            songNameLabel.text = song.title ?? "unknown"
            artistLabel.text = song.artist ?? "unknown"
            if let artwork = song.artwork{
                
                let artWorkImg = artwork.imageWithSize(CGSizeMake(150, 150)) ?? UIImage(named: "lobe_logo_circular_no_gredient")
                profileImage.image = artWorkImg
                self.containerView.backgroundColor = UIColor(patternImage: artWorkImg!)
                
            }
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        LobeStateHelper.sharedInstance.addLobeStateObserver(self)
        profileImage.roundCorners(.AllCorners, radius: profileImage.frame.size.width / 2)
        profileImage.layer.borderColor = UIColor.lightGrayColor().CGColor
        profileImage.layer.borderWidth = 2
//        updateState()
//        timeSlider.setThumbImage(UIImage(named: "triangle", inBundle: .None, compatibleWithTraitCollection: .None), forState: .Normal)
        AUDIO_OUT_MANAGER.observeMusicChange(self)
        self.containerView.backgroundColor = UIColor.grayBackgroundColor()
    }
    
    static func nib() -> UINib {
        return UINib(nibName: ProfileViewCellIdentifier, bundle: nil)
    }
}

//Unused for now
extension ProfileView {
    
    func blurImage() {
        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        let blurView = UIVisualEffectView(effect: darkBlur)
        blurView.frame = profileImage.bounds
        profileImage.addSubview(blurView)
    }
    //UNUSED
    func setupSegmentedControl(){
        
        segmentControl.items = ["1", "2", "3"]
        segmentControl.font = UIFont(name: "Avenir-Black", size: 12)
        segmentControl.borderColor = UIColor.purpleColor()
        segmentControl.selectedIndex = 0
        segmentControl.backgroundColor  = UIColor.purpleColor()
        segmentControl.addTarget(self, action: "segmentValueChanged:", forControlEvents: .ValueChanged)
    }
    func segmentValueChanged(sender: AnyObject?){
        
        if segmentControl.selectedIndex == 0 {
        }else if segmentControl.selectedIndex == 1{
        }else{
        }
    }
}
