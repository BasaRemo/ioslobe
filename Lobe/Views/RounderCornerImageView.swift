//
//  RounderCornerImageView.swift
//  Lobe
//
//  Created by Professional on 2016-01-12.
//  Copyright © 2016 Ntambwa. All rights reserved.
//


import UIKit

@IBDesignable
class RounderCornerImageView: UIImageView {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    
}

