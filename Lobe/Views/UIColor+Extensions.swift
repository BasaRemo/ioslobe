//
//  UIColor+Extensions.swift
//  OfficeHours
//
//  Created by Eliel Gordon on 12/7/15.
//  Copyright © 2015 Saltar Group. All rights reserved.
//

import UIKit

extension UIColor {
    static func primaryBlueColor() -> UIColor {
        return UIColor(red: 6/255, green: 111/255, blue: 218/255, alpha: 1.0)
    }
    
    static func lightBlueColor() -> UIColor {
        return UIColor(red: 29/255, green: 173/255, blue: 255/255, alpha: 1.0)
    }
    
    static func lighterGrayColor() -> UIColor {
        return UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1.0)
    }
    static func bluePositiveColor() -> UIColor {
        return UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1)
    }
    static func greenPositiveColor() -> UIColor {
        return UIColor(red: 33/255, green: 175/255, blue: 67/255, alpha: 1)
    }
    static func orangePositiveColor() -> UIColor {
        return UIColor(red: 230/255, green: 90/255, blue: 0, alpha: 1)
    }
    static func primaryGrayLightColor() -> UIColor {
        return UIColor(red: 19/255, green: 19/255, blue: 20/255, alpha: 0.5)
    }
    
    static func intenseOrangeColor() -> UIColor {
         return hexColor("E56C01") //UIColor(red: 230/255, green: 110/255, blue: 1, alpha: 1.0)
    }
    
    static func promoteTrackColor() -> UIColor {
        return UIColor(red: 60/255, green: 139/255, blue: 65/255, alpha: 0.5)
    }
    
    static func dislikeTrackColor() -> UIColor {
        return UIColor(red: 140/255, green: 59/255, blue: 65/255, alpha: 0.5)
    }
    
    static func grayBackgroundColor() -> UIColor {
        return UIColor(red: 26/255, green: 26/255, blue: 28/255, alpha: 0.5)
    }
    
    static func ligthBlackColor() -> UIColor {
        return UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.90)
    }
    static func lighterBlackColor() -> UIColor {
        return UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.25)
    }
    static func playlistCellColor() -> UIColor {
        return UIColor(red: 15/255, green: 15/255, blue: 15/255, alpha: 0.8)
    }
    static func backgroundStrongColor() -> UIColor {
        return UIColor(red: 45/255, green: 44/255, blue: 55/255, alpha: 1)
    }
    static func playbackBackgroundColor() -> UIColor {
        return UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.80)
    }
    
    
    static func hexColor(hexString: String) -> UIColor {
        let hex = hexString.stringByTrimmingCharactersInSet(NSCharacterSet.alphanumericCharacterSet().invertedSet)
        var int = UInt32()
        NSScanner(string: hex).scanHexInt(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (1, 1, 1, 0)
        }
        
        return UIColor(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

extension UIColor {
    
    convenience init(hexString: String) {
        let hex = hexString.stringByTrimmingCharactersInSet(NSCharacterSet.alphanumericCharacterSet().invertedSet)
        var int = UInt32()
        NSScanner(string: hex).scanHexInt(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (1, 1, 1, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
    
    func generateImageWithColor(color: UIColor) -> UIImage {
        let rect = CGRectMake(0, 0, 1, 1)
        
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        CGContextSetFillColorWithColor(context, color.CGColor)
        CGContextFillRect(context, rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
}